<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Client;
use App\AnswerOption;
use App\Poll;
use App\Question;
use App\Option;
use App\Http\Controllers\Main;
use App\SubscriptionEvent;
use App\MySQLClientRepository;
use DB;
use Request;
use Validator;
use Auth;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;

class AnswersController extends Main {

	/**
	 * index
	 * Obtiene todas las respuestas con unos filtros aplicados.
	 * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
	 * 
	 * @Illuminate\Foundation\Http\FormRequest
	 * 
	 * @return json|mixed   Array de App\Answers | Error
	 */
	public function index() {

		try {

			$answers = DB::table('answers');

			foreach(Request::query() as $name => $value) {

                $answers = $answers->where($name, $value);

            }

            $answers = $answers->get();

            foreach ($answers as &$answer) {

            	$answer = $this->resolveRelations($answer);

            }

			return Main::response(true, 'OK', $answers);

		} catch(\Exception $e) {

			return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

		}

	}

	/**
	 * answer
	 * Guarda las respuestas de una encuesta, siempre y cuando exista y las preguntas correspondan a la encuesta.
	 * @param  integer $id    Id de la encuesta.
	 * @return json|mixed     Ok|NOT FOUND|BAD REQUEST(error de validacion)|Forbidden(la respuesta no es de la encuesta)
	 */
	public function answer($id) {

    	$poll = Poll::find($id);

    	if ($poll) {

            try {

	    		$input = Request::all();

		    	$rules = [
	                'answers' => 'required|array'
	            ];

	            if(isset($input['answers']) &&
	            		 $input['answers']) {

			        foreach(range(0, count($input['answers']) - 1) as $i) {

			        	$rules["answers.$i.id_questions"]  	= 'required|integer|exists:questions,id_questions';
			        	$rules["answers.$i.options"]    	= 'required|array';

			        	if(isset($input['answers'][$i]['options']) &&
			        			 $input['answers'][$i]['options']) {

			        		foreach(range(0, count($input['answers'][$i]['options']) - 1) as $k) {

				                $rules["answers.$i.options.$k"] = 'required|integer|exists:options,id_options';

				            }

			        	}

		            }

	            }

	            $validator = Validator::make($input, $rules);

	            if($validator->fails()) {

	                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

	            }

	            DB::beginTransaction();

	            $id_clients = Client::where('id_users', '=', Auth::user()->id)->first()->id_clients;

	            if(Answer::where('id_clients', '=', $id_clients)
	            		 ->where('poll', '=', $poll->name)->first()) {

	            	return Main::response(true, 'Forbidden', $poll->name . 'The poll has been answered', 403);

	            }

				foreach($input['answers'] as $a) {

					$question = Question::find($a['id_questions']);

					if($question->id_polls != $poll->id_polls) {

						return Main::response(false, 'Bad Request', ['errors' => ['questions' => 'question does not belong to the poll']], 400);

					}

					$answer = new Answer;
					$answer->id_clients = $id_clients;
					$answer->poll = $poll->name;
					$answer->question = $question->description;
					$answer->save();

					foreach($a['options'] as $id_options) {

						$option = Option::find($id_options);

						if($option->id_questions != $question->id_questions) {

							return Main::response(false, 'Bad Request', ['errors' => ['options' => 'option does not belong to the poll']], 400);

						}

						$answerOption = new AnswerOption;
						$answerOption->answer = $option->description;
						$answerOption->id_answers = $answer->id_answers;
						$answerOption->save();

					}

				}

				DB::commit();

				return Main::response(true, 'OK', $this->resolveRelations($answer));

	    	} catch(\Exception $e) {

	    		DB::rollback();

	    		return Main::response(false, $e->getMessage(), null, 500);

	    	}

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

	}

	/**
	 * resolveRelations
	 * Devuelve todas las opciones (App\AnswerOption) disponibles de una pregunta.
	 * @param  \App\Answer $answer
	 * @return \App\Answer         Objeto Eloquent de \App\Answer
	 */
	private function resolveRelations($answer) {

		$answer->answers = AnswerOption::where('id_answers', '=', $answer->id_answers)->get();

		return $answer;

	}

}

