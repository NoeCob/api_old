<?php

namespace App\Http\Controllers;

use App\Vendor;
use App\Card;
use App\CardsVendors;
use App\Debt;
use App\Contract;
use App\Group;
use App\Correction;
use App\ErrorCode;
use App\Role;
use App\Permission;
use App\User;
use App\Client;
use App\Coupon;
use App\Category;
use App\Element;
use App\Extras;
use App\Charge;
use App\MySQLClientRepository;
use App\LevelService;
use App\Http\Controllers\Main;
use App\SubscriptionEvent;
use App\Ticket;
use App\Traits\NotifyTrait;
use Log;
use DB;
use Auth;
use Request;
use Validator;
use Mail;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Carbon\Carbon;

class ContractsController extends Main
{

    use NotifyTrait;

    /**
     * __construct
     * Se le indica que la funcion "create" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct()
    {

        parent:: __construct(['create']);
    }

    /**
     * index
     * Devuelve todas los contrato después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     *
     * @return response NULL|Error(400)
     */
    public function index()
    {

        try {

            $contracts = DB::table('contracts');

            foreach (Request::query() as $name => $value) {

                switch ($name) {
                    case 'referred_by': 

                        $contracts = $contracts->whereIn(
                            'id_clients',
                            array_map(
                                function ($element) {
                                    return $element->id_clients;
                                },
                                DB:: table('clients')
                                    ->where($name, $value)
                                    ->select('id_clients')
                                    ->get()
                            )
                        );

                        break;

                    default: 
                        $contracts = $contracts->where($name, $value);
                        break;
                }
            }

            $contracts = $contracts->get();
        } catch (\Exception $e) {

            return Main:: response(false, null, null, 400);
        }

        foreach ($contracts as &$contract) {

            $contract = $this->resolveRelations($contract);
        }

        return Main:: response(true, null, $contracts);
    }

    /**
     * passwordNotification
     * Se le envía su password al cliente(\App\Client), identificandolo por medio del ID ($idclient)
     *
     * @Mail
     * @App\Client
     *
     * @param int $idclient ID del Cliente
     * @param string $password Password
     * @return void
     */
    public function passwordNotification($idclient, $password)
    {

        $client = Client::find($idclient);
        if (!is_null($client)) {
            Mail::send(
                'email.passwordNotification',
                array(
                    'name'     => $client->name,
                    'username' => $client->email,
                    'password' => $password
                ),
                function ($message) use ($client) {
                    $message->to($client->email, $client->name)->subject('Bienvenido a Aguagente');
                }
            );
        }
    }

    /**
     * create
     * Crea un cliente con todas su dependencias: 
     *  - Cliente
     *      - Dentro de base de datos
     *      - Dentro de CONKECTA
     *  - Usuario
     *  - Rol de usuario
     *  - Contrato
     *      - Categoria
     *      - Extras
     *
     * @todo NOTE: 
     *     creat-contract has to be enabled just for sellers.
     *     validate max chars
     *     drop name and email from clients model
     *     validate sent elements
     *
     * @Auth
     * @Illuminate\Foundation\Http\FormRequest
     * @Conekta
     * @Conekta_Customer
     * @App\Client
     * @App\Group
     * @App\User
     * @App\Contract
     * @App\Element
     * @App\Extras
     * @App\Category
     *
     * @return response null|Bad Request|Frobbiden|null(500)
     */
    public function create()
    {
        $conektaID = '';
        try {

            $user = Auth::user();

            $input = Request::all();

            $rules = [
                'client.name'            => 'required',
                'client.email'           => 'required|email',
                'client.address'         => 'required',
                'client.phone'           => 'required|string|min:8|max:20',
                'client.between_streets' => 'required',
                'client.colony'          => 'required',
                'client.postal_code'     => 'required|digits:5',
                'client.state'           => 'required|string',
                'client.county'          => 'required|string',
                'elements'               => 'array',
                'photos'                 => 'array',
                'proof_of_address'       => 'image|max:10240',
                'id'                     => 'image|max:10240',
                'id_reverse'             => 'image|max:10240',
                'profile'                => 'image|max:10240',
                'monthly_installments'   => 'in:1,3,6,9,12',
                'credit_card'            => 'string'
            ];

            if (is_null($user)) {

                $rules['seller'] = 'required|exists:clients,id_clients';
            }

            if (isset($input['elements'])) {

                foreach (range(0, count($input['elements']) - 1) as $index) {

                    $rules["elements.$index"] = 'integer';
                }
            }

            if (isset($input['photos'])) {

                foreach (range(0, count($input['photos']) - 1) as $index) {

                    $rules["photos.$index"] = 'image|max:10240';
                }
            }
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {

                return Main:: response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
            }

            DB:: beginTransaction();

            $referrer = isset($input['seller']) ?
                Client:: find($input['seller']): Client:: where('id_users', Auth::user()->id)->first();

            $cuponUsed = null;
            if (isset($input['cupon'])) {
                $cupon = Coupon::where('token', $input['cupon'])->first();
                if ($cupon) {
                    $cuponUsed = $cupon->id_coupon;
                    $cuponDe   = Client::where('email', $cupon->email)->first();

                    if ($cuponDe) {
                        $referrer = $cuponDe;
                    }
                }
            }

            if (!$referrer) {
                return Main:: response(false, 'Forbidden', ['errors' => ['referrer' => ['Is not a client']]], 403);
            }

            $referrerGroup = Group::find($referrer->id_groups);
            $signIntoGroup = Group::find($referrerGroup->sign_into);

            //$password = str_random(10);
            $password = rand(100000, 999999);

            if ($user = User::where('email', '=', $input['client']['email'])->first()) {

                if ($user->hasRole('Client')) {
                    return Main:: response(false, 'Forbidden', 'User is already a client', 503);
                } else {
                    $user->attachRole(Role::where('name', '=', 'Client')->first());
                }
            } else {

                $user           = new User();
                $user->name     = $input['client']['name'];
                $user->email    = $input['client']['email'];
                $user->password = bcrypt($password);
                $user->save();
                $user->attachRole(Role::where('name', '=', 'Client')->first());
            }

            $client                   = new Client;
            $client->id_users         = $user->id;
            $client->id_groups        = $referrerGroup->id_groups;
            $client->name             = $input['client']['name'];
            $client->email            = $input['client']['email'];
            $client->address          = $input['client']['address'];
            $client->phone            = $input['client']['phone'];
            $client->between_streets  = $input['client']['between_streets'];
            $client->colony           = $input['client']['colony'];
            $client->postal_code      = $input['client']['postal_code'];
            $client->state            = $input['client']['state'];
            $client->county           = $input['client']['county'];
            $client->signed_in_id     = $referrerGroup->id_groups;
            $client->signed_in        = $referrerGroup->name;
            $client->deposit          = $referrerGroup->deposit;
            $client->installation_fee = $referrerGroup->installation_fee;
            //$client->monthly_fee        = $signIntoGroup->monthly_fee;
            $client->monthly_fee           = $referrerGroup->monthly_fee;
            $client->social_responsability = (boolean)@$input['responsabilidad'];
            $client->referred_by           = $referrer->id_clients;
            $client->status                = 'standby';
            $client->monthly_installments  = isset($input['monthly_installments']) ? $input['monthly_installments']: 1;
            $client->signed_in_group       = $referrerGroup;

            if (isset($input['cupon'])) {
                $client->coupon = $cuponUsed;
            }

            $client->collected_at = null;
            $client->pay_day      = null;
            $client->next_payday  = null;
            $client->save();

            if ($client->conekta_token === null) {

                $customer = null;

                if (isset($input['credit_card'])) {
                    try {
                        
                        $vendors = Vendor::where('status', '1')->get();

                        $card                 = new Card;
                        $card->id_clients     = $client->id_clients;
                        $card->name           = $client->name;
                        $card->address        = $client->address;
                        $card->colony         = $client->colony;
                        $card->postal_code    = $client->postal_code;
                        $card->county         = $client->county;
                        $card->state          = $client->state;
                        $card->country        = 'Mexico';
                        $card->phone          = $client->phone;
                        $card->email          = $client->email;
                        $card->save();

                        foreach ($vendors as $vendor) {
                            $nCard = false;
                            switch(strtoupper($vendor->name)) {
                                case "CONEKTA":
                                    Conekta:: setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                                    Conekta:: setLocale('es');
                    
                                    $customer = Conekta_Customer::create(array(
                                        'name'  => $client->name,
                                        'email' => $client->email,
                                        'phone' => $client->phone
                                    ));

                                    $nCard = $customer->createCard(array('token' => $input['credit_card']));
                                    break;
                            }
                
                            if ($nCard) {
                                $cardVendor              = new CardsVendors;
                                $cardVendor->id_cards    = $card->id_cards;
                                $cardVendor->id_vendor   = $vendor->id_vendor;
                                $cardVendor->card_token  = $nCard->id;
                                $cardVendor->save();

                                $card->card_number = $nCard->last4;
                                $card->exp_month   = $nCard->exp_month;
                                $card->exp_year    = $nCard->exp_year;
                                $card->save();
                            }
                        }
                    } catch (\Exception $e) {
                        return Main:: response(false, $e->getMessage(), null, 500);
                    }
                } else {
                    try {
                        
                        $vendors = Vendor::where('status', '1')->get();

                        foreach ($vendors as $vendor) {
                            $nCard = false;
                            switch(strtoupper($vendor->name)) {
                                case "CONEKTA":
                                    Conekta:: setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                                    Conekta:: setLocale('es');
                    
                                    $customer = Conekta_Customer::create(array(
                                        'name'  => $client->name,
                                        'email' => $client->email,
                                        'phone' => $client->phone
                                    ));

                                    break;
                            }
                        }
                    } catch (\Exception $e) {
                        return Main:: response(false, $e->getMessage(), null, 500);
                    }
                }

                if ($customer) {
                    $conektaID             = $customer->id;
                    $client->conekta_token = $customer->id;
                    $client->save();
                }
            }

            $contract             = new Contract;
            $contract->id_clients = $client->id_clients;
            $contract->save();

            foreach (isset($input['elements']) ? $input['elements'] : array() as $id_elements) {

                $element = Element::find($id_elements);

                if ($element) {

                    $category = Category::find($element->id_categories);

                    $extras                = new Extras();
                    $extras->id_contracts  = $contract->id_contracts;
                    $extras->category_name = $category->name;
                    $extras->element_name  = $element->name;
                    $extras->price         = $element->price;
                    $extras->save();
                }
            }

            $location = realpath('documents/contracts') . "/$referrer->id_clients/$contract->id_contracts";

            $i = 1;

            foreach (isset($input['photos']) ? $input['photos'] : array() as $name => $file) {

                $file->move(
                    "$location/photos",
                    $file->getClientOriginalName()
                );
            }

            if (isset($input['id'])) $input['id']->move($location, 'id' . '.' . $input['id']->getClientOriginalExtension());
            if (isset($input['id_reverse'])) $input['id_reverse']->move($location, 'id_reverse' . '.' . $input['id_reverse']->getClientOriginalExtension());
            if (isset($input['proof_of_address'])) $input['proof_of_address']->move($location, 'proof_of_address' . '.' . $input['proof_of_address']->getClientOriginalExtension());
            if (isset($input['profile'])) $input['profile']->move($location, 'profile' . '.' . $input['profile']->getClientOriginalExtension());

            $this->notifyPassword([
                'client'   => $client,
                'password' => $password
            ]);

            DB:: commit();

            return Main:: response(true, null, $this->resolveRelations($contract), 201);
        } catch (\Exception $e) {
            if ($customer) {
                $customer = Conekta_Customer::find($conektaID);
                $customer->delete();
            }
            DB:: rollback();

            return Main:: response(false, $e->getMessage(), null, 500);
        }
    }

    /**
     * show
     * Muestra la información de un contrato(|App\Contract) por medio de su ID($id)
     *
     * @App\Contract
     *
     * @param int $id ID del contrato
     * @return response         null|null(404)
     */
    public function show($id)
    {

        if ($contract = Contract::find($id)) {

            return Main:: response(true, null, $this->resolveRelations($contract));
        } else {

            return Main:: response(false, null, null, 404);
        }
    }

    /**
     * update
     * Guarda las modificaciones realizadas a un contrato y todas sus dependencias (\App\Contract) por medio del ID.
     * Además genera un registro de corrección(\App\Correction) para poder comparar con información anterior (versiones de información)
     *
     * @Auth
     * @Illuminate\Foundation\Http\FormRequest
     * @Conekta
     * @Conekta_Customer
     * @App\Correction
     * @App\Client
     * @App\Group
     * @App\User
     * @App\Contract
     * @App\Element
     * @App\Extras
     * @App\Category
     *
     * @return response null|Bad Request|null(404)|null(500)
     */
    public function update($id)
    {
        if ($contract = Contract::find($id)) {
            try {

                $client = Client::find($contract->id_clients);
                if ($input = Request::all()) {
                    $rules = [
                        'client.name'                 => '',
                        'client.address'              => '',
                        'client.phone'                => 'string|min:8|max:20',
                        'client.between_streets'      => '',
                        'client.colony'               => '',
                        'client.postal_code'          => 'digits:5',
                        'client.state'                => '',
                        'client.county'               => '',
                        'client.monthly_installments' => 'in:1,3,6,9,12',
                        'elements'                    => 'array',
                        'photos'                      => 'array',
                        'proof_of_address'            => 'image|max:10240',
                        'id'                          => 'image|max:10240',
                        'id_reverse'                  => 'image|max:10240',
                        'profile'                     => 'image|max:10240'
                    ];

                    if (isset($input['elements'])) {

                        foreach (range(0, count($input['elements']) - 1) as $index) {

                            $rules["elements.$index"] = 'exists:categories_elements,id_categories_elements';
                        }
                    }

                    $customMessage = [
                        'exists' => 'Debe seleccionar al menos 1 material para su instalación'
                    ];

                    $validator = Validator::make($input, $rules, $customMessage);

                    if ($validator->fails()) {
                        return Main:: response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                    }

                    $location = realpath('documents/contracts') . "/$client->referred_by/$contract->id_contracts";

                    if (isset($input['client']['name'])) $client->name                                   = $input['client']['name'];
                    if (isset($input['client']['address'])) $client->address                             = $input['client']['address'];
                    if (isset($input['client']['phone'])) $client->phone                                 = $input['client']['phone'];
                    if (isset($input['client']['between_streets'])) $client->between_streets             = $input['client']['between_streets'];
                    if (isset($input['client']['colony'])) $client->colony                               = $input['client']['colony'];
                    if (isset($input['client']['postal_code'])) $client->postal_code                     = $input['client']['postal_code'];
                    if (isset($input['client']['state'])) $client->state                                 = $input['client']['state'];
                    if (isset($input['client']['county'])) $client->county                               = $input['client']['county'];
                    if (isset($input['client']['monthly_installments'])) $client->monthly_installments   = $input['client']['monthly_installments'];
                    if (isset($input['client']['social_responsability'])) $client->social_responsability = $input['client']['social_responsability'] == 'true';
                    if (isset($input['client']['extra_data'])) $client->extra_data                       = $input['extra_data'];
                    if (isset($input['id']) && $input['id']) {
                        foreach (glob("$location/id.*") as $file)
                            unlink($file);
                        $input['id']->move($location, 'id' . '.' . $input['id']->getClientOriginalExtension());
                    }

                    if (isset($input['id_reverse']) && $input['id_reverse']) {
                        foreach (glob("$location/id_reverse.*") as $file)
                            unlink($file);
                        $input['id_reverse']->move($location, 'id_reverse' . '.' . $input['id_reverse']->getClientOriginalExtension());
                    }

                    if (isset($input['proof_of_address']) && $input['proof_of_address']) {
                        foreach (glob("$location/proof_of_address.*") as $file)
                            unlink($file);
                        $input['proof_of_address']->move($location, 'proof_of_address' . '.' . $input['proof_of_address']->getClientOriginalExtension());
                    }

                    if (isset($input['profile']) && $input['profile']) {
                        foreach (glob("$location/profile.*") as $file)
                            unlink($file);
                        $input['profile']->move($location, 'profile' . '.' . $input['profile']->getClientOriginalExtension());
                    }

                    $i = 1;

                    foreach (isset($input['photos']) ? $input['photos'] : array() as $name => $file) {

                        $file->move(
                            "$location/photos",
                            $file->getClientOriginalName()
                        );
                    }

                    if ($client->status == 'invalid' || $client->status == 'rejected' || $client->status == 'standby') {
                        $charged = Charge::where('customer_id', $client->conekta_token)
                            ->whereNotNull('paid_at')
                            ->get();

                        if (count($charged) < 1) {
                            $client->status       = 'standby';
                            $client->collected_at = null;
                            $client->pay_day      = null;
                            $client->next_payday  = null;
                        }
                    }

                    $client->save();

                    $correction             = new Correction;
                    $correction->id_clients = $client->id_clients;
                    $correction->save();

                    foreach (Extras::where('id_contracts', '=', $contract->id_contracts)->get() as $extra) {
                        $extra->delete();
                    }

                    if (isset($input['elements'])) {

                        foreach ($input['elements'] as $id_elements) {

                            $element = Element::find($id_elements);

                            if ($element) {

                                $category = Category::find($element->id_categories);

                                $extras                = new Extras();
                                $extras->id_contracts  = $contract->id_contracts;
                                $extras->category_name = $category->name;
                                $extras->element_name  = $element->name;
                                $extras->price         = $element->price;
                                $extras->save();
                            }
                        }
                    }
                    $this->notifyStaff($client);
                    return Main:: response(true, null, $this->resolveRelations($contract));
                } else {

                    return Main:: response(true, 'Ok', null, 200);
                }
            } catch (\Exception $e) {

                return Main:: response(false, $e->getMessage(), null, 400);
            }
        } else {

            return Main:: response(false, null, null, 404);
        }
    }


    /**
     * resolveRelations
     * Devuelve todos el contrato con todas sus dependencias cargadas: 
     *  - Cliente
     *  - Grupo
     *  - Extras
     *  - Documentos y fotos.
     *
     * @param  Contract $contract Objeto Eloquent Contract
     * @return Contract $contract Objeto Eloquent Contract
     */
    private function resolveRelations($contract)
    {

        $client = DB::table('clients')
            ->where('id_clients', $contract->id_clients)
            ->first();

        $group = DB::table('groups')
            ->where('id_groups', $client->id_groups)
            ->first();

        $group->sign_into = Group::find($group->sign_into);

        $elements = DB::table('contracts_extras')
            ->where('id_contracts', $contract->id_contracts)
            ->get();

        $location = "documents/contracts/$client->referred_by/$contract->id_contracts";
        $photos   = glob("$location/photos/*");
        foreach ($photos as &$photo)
            $photo = Request::root() . "/" . $photo;

        $documents = array_values(array_filter(glob("$location/*"), 'is_file'));
        foreach ($documents as &$document)
            $document = Request::root() . "/" . $document;

        if ($client->coupon != 0) {
            $coupon                     = Coupon::where('id_coupon', $client->coupon)->first();
            $contract->cupon            = $coupon;
            $contract->cupon->descuento = $coupon->rewards[$coupon->level];
        }

        $invalidation = DB::table('invalidations')
            ->where('id_clients', $contract->id_clients)
            ->orderBy('id_invalidations', 'desc')
            ->first();

        $client->group          = $group;
        $contract->client       = $client;
        $contract->elements     = $elements;
        $contract->photos       = $photos;
        $contract->documents    = $documents;
        $contract->invalidation = $invalidation;

        return $contract;
    }

    /**
     * on
     * IPN listener para los WebHooks de Conekta: 
     *  - charge.created
     *  - charge.paid
     *  - subscription.created
     *  - subscription.paid
     *  - subscription.paused
     *  - subscription.resumed
     *  - subscription.payment_failed
     *  - subscription.canceled
     *
     * @Coneckta
     * @Conekta_plan
     * @Conekta_Charge
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Client
     * @App\Charge
     * @App\SubscriptionEvent
     * @App\Debt
     * @App\LevelService
     * @App\MySQLClientRepository
     *
     * @return void
     */
    public function on()
    {

        $input = Request::all();

        if (isset($input['type'])) {

            switch ($input['type']) {

                case 'charge.created': 

                    if (! ($charge = Charge::find($input['data']['object']['id']))) {

                        Conekta:: setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                        Conekta:: setLocale('es');

                        $client    = Client::where('conekta_token', '=', $input['data']['object']['customer_id'])->first();
                        $newCharge = false;
                        if ($client) {

                            $charge                  = new Charge;
                            $charge->id_charges      = $input['data']['object']['id'];
                            $charge->customer_id     = $input['data']['object']['customer_id'];
                            $charge->description     = $input['data']['object']['description'];
                            $charge->amount          = $input['data']['object']['amount'];
                            $charge->fee             = $input['data']['object']['fee'];
                            $charge->id_clients      = $client->id_clients;
                            $charge->failure_message = '';//Conekta_Charge::find($input['data']['object']['id'])->failure_message;
                            $charge->save();
                            $newCharge = true;
                        }

                        if ($newCharge) {
                            $charge->failure_message = Conekta_Charge::find($input['data']['object']['id'])->failure_message;
                        }
                    }

                    break;

                case 'charge.paid': 

                    $client = Client::where('conekta_token', '=', $input['data']['object']['customer_id'])->first();
                    if ($charge = Charge::find($input['data']['object']['id'])) {

                        if (is_null($charge->paid_at)) {
                            $charge->paid_at = date('Y-m-d H:i:s', $input['data']['object']['paid_at']);
                            $charge->save();

                            $extradata = json_decode($charge->extra_data, true);
                            if ($extradata['type'] == 'oxxo' || $extradata['type'] == 'spei') {
                                $client = Client::where('id_clients', '=', $charge->id_clients)->first();
                                $params = [
                                    'client'  => $client,
                                    'charge'  => $charge,
                                    'data'    => $extradata,
                                    'request' => $input
                                ];
                                $this->offlinePaymentsSteps($params);
                            }
                        }
                    } else {

                        $charge              = new Charge;
                        $charge->id_charges  = $input['data']['object']['id'];
                        $charge->customer_id = $input['data']['object']['customer_id'];
                        $charge->description = $input['data']['object']['description'];
                        $charge->amount      = $input['data']['object']['amount'];
                        $charge->fee         = $input['data']['object']['fee'];
                        $charge->paid_at     = date('Y-m-d H:i:s', $input['data']['object']['paid_at']);
                        $charge->id_clients  = $client->id_clients;

                        if (!Charge::find($input['data']['object']['id'])) {
                            $charge->save();
                        }
                    }

                    break;

                case 'subscription.created'       : 
                case 'subscription.paid'          : 
                case 'subscription.paused'        : 
                case 'subscription.resumed'       : 
                case 'subscription.payment_failed': 
                case 'subscription.canceled'      : 
                    
                    /*
                    if (! ($event = SubscriptionEvent::find($input['id']))) {

                        $client = Client::where('conekta_token', '=', $input['data']['object']['customer_id'])->first();

                        if (($client->subscription_status == null) || (new \DateTime($client->subscription_status_date) <= new \DateTime(date('Y-m-d H:i:s', $input['created_at'])))) {

                            $client->subscription_status      = $input['data']['object']['status'];
                            $client->subscription_status_date = date('Y-m-d H:i:s', $input['created_at']);
                            $client->save();
                        }

                        $event                         = new SubscriptionEvent;
                        $event->id_subscription_events = $input['id'];
                        $event->customer_id            = $input['data']['object']['customer_id'];
                        $event->type                   = $input['type'];
                        $event->subscription_status    = $input['data']['object']['status'];
                        $event->date                   = date('Y-m-d H:i:s', $input['created_at']);
                        $event->id_clients             = $client->id_clients;
                        $event->save();

                        if ($input['type'] == 'subscription.canceled' && $client->status != 'canceled') {

                            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                            Conekta::setLocale('es');
                            $customerExceptions = \Config:: get('constants.clientsExceptionList');

                            if (!in_array($client->id_clients, $customerExceptions)) {
                                $plan = Conekta_Plan::find($input['data']['object']['plan_id']);

                                $accumulated = Debt::where('id_clients', '=', $client->id_clients)
                                    ->whereNull('id_charges')
                                    ->sum('amount');

                                $accumulated += $plan->amount;

                                $debt                     = new Debt();
                                $debt->id_clients         = $client->id_clients;
                                $debt->amount             = $plan->amount;
                                $debt->collection_fees    = 10000;
                                $debt->moratory_fees      = (integer)$accumulated * .04;
                                $debt->next_try_to_charge = date('Y-m-d', strtotime('+6 days'));
                                $debt->save();

                                $client->debt += $debt->amount + $debt->collection_fees + $debt->moratory_fees;
                            }
                        }

                        $levelService = new LevelService(new MySQLClientRepository());
                        $levelService->setLevels($client);
                    }
                    */

                    break;
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $params
     * @return void
     */
    private function offlinePaymentsSteps($params)
    {
        switch ($params['charge']->description) {
            case "Extras": 
                $this->offlineFirstPayment($params);
                break;

            case "Pago tardío": 
                $this->offlineLatePayment($params);
                break;

            default: 
                $this->offlineAheadPayment($params);
                break;
        }

        $this->notifyStaffOffilinePayment($params);
    }

    /**
     * offlineFirstPayment
     * Cuando se recibe la confirmación del PRIMER pago de CONEKTA, el sistema tiene que realizar las acciones como: 
     * - Asignar su dia de pago.
     * - Asignar su siguiente fecha de pago.
     * - Generar ticket de instalación.
     *
     * @param array $params Array con los datos obtenidos de CONEKTA
     * @return void
     */
    private function offlineFirstPayment($params)
    {
        $client   = Client::where('id_clients', '=', $params['charge']->id_clients)->first();
        $group    = Group::find($params['client']->id_groups);
        $dayToday = date('d');
        if ($dayToday > 27) {
            $dayToday = 27;
        }

        $today      = date('Y-m-') . $dayToday;
        $nextPayDay = date('Y-m-d', strtotime('+1 month', strtotime($today)));

        $client->subscription_status = 'active';
        $client->collected_at        = $today;
        $client->next_payday         = $nextPayDay;
        $client->pay_day             = str_pad($dayToday, 2, '0', STR_PAD_LEFT);

        if ($group->trial_days > 0) {
            $nextPayDay          = date('Y-m-d', strtotime('+' . $group->trial_days . ' days', strtotime($today)));
            $client->next_payday = $nextPayDay;
            $client->pay_day     = str_pad($dayToday, 2, '0', STR_PAD_LEFT);
        }

        if (isset($params['data']['months_ahead'])) {
            $nextPayDay          = date('Y-m-d', strtotime('+' . $params['data']['months_ahead'] . ' months', strtotime($client->next_payday)));
            $client->next_payday = $nextPayDay;
        }

        $client->save();

        if (!Ticket::where('id_clients', '=', $client->id_clients)->first()) {
            //if(1==1) {}
            $errorCode = ErrorCode::where('name', 'LIKE', '%Install%')->first();

            if ($errorCode) {
                $ticket                                = new Ticket;
                $ticket->id_clients                    = $client->id_clients;
                $ticket->id_error_codes                = $errorCode->id_error_codes;
                $ticket->description                   = 'Favor de instalar equipo.';
                $ticket->estimated_service_fee         = null;
                $ticket->estimated_service_fee_reasons = null;
                $ticket->status                        = 'opened';
                $ticket->type                          = 'installation';
                $ticket->save();

                $ticket->client = $client;

                $this->ticketCreation($ticket);
            }
        }
    }

    /**
     * offlineLatePayment
     * Cuando se recibe la confirmación de un pago con DEUDA de CONEKTA, el sistema tiene que realizar las acciones como: 
     * - Asignar el charge_id a la deuda.
     * - Eliminar la deuda del cliente.
     * - Asigunar siguiente fecha de pago.
     *
     * @param array $params Array con los datos obtenidos de CONEKTA
     * @return void
     */
    private function offlineLatePayment($params)
    {
        $client = Client::where('id_clients', '=', $params['charge']->id_clients)->first();
        $debts  = Debt::where('id_clients', '=', $client->id_clients)->whereNull('id_charges')->get();

        foreach ($debts as $debt) {
            $debt->id_charges = $params['request']['data']['object']['id'];
            $debt->save();
        }

        $today      = date('Y-m-') . $client->pay_day;
        $nextPayDay = date('Y-m-d', strtotime('+1 month', strtotime($today)));

        $client->next_payday         = $nextPayDay;
        $client->subscription_status = 'active';
        $client->debt                = 0;
        $client->save();
    }

    /**
     * offlineAheadPayment
     * Cuando se recibe la confirmación de un pago ADELANTADO de CONEKTA, el sistema tiene que realizar las acciones como: 
     * - Asigunar siguiente fecha de pago.
     *
     * @param array $params Array con los datos obtenidos de CONEKTA
     * @return void
     */
    private function offlineAheadPayment($params)
    {
        $client       = Client::where('id_clients', '=', $params['charge']->id_clients)->first();
        $months_ahead = $params['data']['months_ahead'];
        $nextPayDay   = date('Y-m-d', strtotime('+' . $months_ahead . ' months', strtotime($client->next_payday)));

        $client->next_payday = $nextPayDay;
        $client->save();
    }
}
