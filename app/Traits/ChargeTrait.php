<?php
namespace App\Traits;

use App\Debt;
use App\Contract;
use App\Client;
use App\Charge;
use App\Traits\NotifyTrait;
use Request;
use Mail;

trait ChargeTrait
{
    use NotifyTrait;
    /*
        $inputs = [
            'client' => \App\Client,
            'description' => '',
            'amount' => '',
            'line_items' => ''
            'type' => card|cash|bank
        ];
    */

    /**
     * createCharge
     * Genera el cargo con CONEKTA.
     * De momento, solo sirve para OXXO/SPEI y requiere de la nueva librería de CONEKTA.
     * 
     * @param array $inputs
     * @return array        Array de respuesta con la referencia generada.
     */
    public function createCharge($inputs)
    {
        $url = base_path('libs/conekta/Conekta.php');
        require_once($url);

        //\Conekta\Conekta::setApiKey('key_nSQs9v4851teqNGmKTyHjA');
        \Conekta\Conekta::setApiKey(env('CONEKTA_API_KEY'));
        \Conekta\Conekta::setLocale('es');
        $client = $inputs['client'];
        //if ($client = Client::find($inputs['id_clients'])) {
            $params = $this->createArrayForCharge($inputs);
            $total  = $this->calculateTotal($inputs['line_items']);

            try {
                $order = \Conekta\Order::create($params);
                
                if($charge = Charge::find($order->id)) {
                    $charge->extra_data  = json_encode($inputs);
                    $charge->save();
                } else {
                    $charge = new Charge;
                    $charge->id_charges  = $order->charges[0]->id;
                    $charge->id_clients  = $inputs['client']->id_clients;
                    $charge->customer_id = $inputs['client']->conekta_token;
                    $charge->description = $inputs['description'];
                    $charge->amount      = $order->charges[0]->amount;
                    $charge->fee         = $order->charges[0]->fee;
                    
                    $inputs['id_clients'] = $inputs['client']->id_clients;
                    unset($inputs['client']);

                    $charge->extra_data  = json_encode($inputs);
                    $charge->save();
                }

                $response = [
                    'error'     => 0,
                    'service'   => 'OXXO',
                    'reference' => $order->charges[0]->payment_method->reference
                ];

                if ($inputs['type'] == 'spei') {
                    $response = [
                        'error'     => 0,
                        'service'   => 'SPEI',
                        'reference' => $order->charges[0]->payment_method->clabe,
                        'bank'      => $order->charges[0]->payment_method->bank
                    ];
                }
                $this->notifyUserOffline($response['service'], $order, $client);

            } catch(\Exception $e) {
                $response = [
                    'error'     => 1,
                    'message' =>  $e->getMessage()
                ];
            }

            return $response;
        //}
    }

    /**
     * calculateTotal
     *
     * @param array $lineItems  Items a cobrar
     * @return float            Suma total de todos los items a cobrar
     */
    private function calculateTotal($lineItems)
    {
        $total = 0;
        foreach ($lineItems as $item) {
            $total+=$item['unit_price'];
        }

        return $total;
    }

    /**
     * createArrayForCharge
     *
     * @param array $params Array de todos los parametros, tipo de pago, items, etc.
     * @return array        Array con el formato determinado para CONEKTA
     */
    private function createArrayForCharge($params)
    {   
        
        $oxxoSpeiExtra = [
            'name'        => 'Gastos de administración',
            'description' => 'Gastos de administración',
            'unit_price'  => 10000,
            'quantity'    => 1
        ];

        foreach ($params['line_items'] as $key => &$line) {
            $line['unit_price'] = intval($line['unit_price']);
        }

        $data  = [
            'currency'   => 'MXN',
            'line_items' => $params[line_items],
            'shipping_lines' => [
                [
                    "amount" => 0,
                    "carrier" => "AGUAGENTE"
                ]
            ],
            'shipping_contact' => [
                'address' => [
                    'street1'     => $params[client]->address,
                    'street2'     => $params[client]->address,
                    'city'        => $params[client]->county,
                    'state'       => $params[client]->state,
                    'postal_code' => $params[client]->postal_code,
                    'country'     => 'Mexico'
                ]
            ],
            'customer_info' => [
                //TEST
                //'customer_id' => 'cus_2inMDXxvYVXZgnEg8'
                'customer_id' => $params[client]->conekta_token
            ],
            'charges' => [
                [
                    'payment_method' => [
                        'type' => 'oxxo_cash'
                    ]
                ]
            ]
        ];

        switch($params['type']) {
            case 'oxxo':
                $data['charges'][0]['payment_method']['type'] = 'oxxo_cash';
                $data['line_items'][]                         = $oxxoSpeiExtra;
                break;
            case 'spei':
                $data['charges'][0]['payment_method']['type'] = 'spei';
                $data['line_items'][]                         = $oxxoSpeiExtra;
                break;
            case 'card':
                $params = $this->getCustomerCard($params);
                break;
        }
        
        return $data;
    }

    /**
     * createArrayForChargeOld
     * DEPRECATED
     * 
     * @param array $params Array de todos los parametros, tipo de pago, items, etc.
     * @return array        Array con el formato determinado para CONEKTA
     */
    private function createArrayForChargeOld($params)
    {
        $oxxoSpeiExtra = [
            'name'        => 'Gastos de administración',
            'description' => 'Gastos de administración',
            'unit_price'  => 10000,
            'quantity'    => 1
        ];

        $total = $this->calculateTotal($params['line_items']);

        $data  = [
            'customer_id' => $params[client]->conekta_token,
            'description' => $params[description],
            'amount'      => $total,
            'currency'    => 'MXN',
            'details'     => [
                'name'       => $params[client]->name,
                'phone'      => str_limit($params[client]->phone, 20,''),
                'email'      => $params[client]->email,
                'line_items' => $params[line_items],
                "shipment" => [
                    "carrier" => "aguagente",
                    "service" => "next_day",
                    "price"   => 0,
                    "address" => [
                        "street1" => $params[client]->address,
                        "street2" => $params[client]->address,
                        "street3" => $params[client]->address,
                        "city"    => $params[client]->county,
                        "state"   => $params[client]->state,
                        "zip"     => $params[client]->postal_code,
                        "country" => "Mexico"
                    ]
                ]
            ]
        ];

        switch($params['type']) {
            case 'oxxo':
                $data['cash']['type'] = 'oxxo';
                $data[line_items][]   = $oxxoSpeiExtra;
                break;
            case 'spei':
                $data['bank']['type'] = 'spei';
                $data[line_items][]   = $oxxoSpeiExtra;
                break;
            case 'card':
                $params = $this->getCustomerCard($params);
                break;
        }
        
        return $data;
    }

    /**
     * getCustomerCard
     * Sin uso, se usará cuando se convierta los pagos automatizados.
     * 
     * @param [type] $params
     * @return void
     */
    private function getCustomerCard($params)
    {
        $customer       = Conekta_Customer::find($params[client]->conekta_token);
        $cards          = $this->_defaultCardFirst(json_decode($customer->cards->__toJSON()), $customer->default_card_id);
        $return['card'] = $cards[0]->id;

        return $return;
    }

    /**
     * _defaultCardFirst
     * Sin uso, se usará cuando se convierta los pagos automatizados.
     * 
     * @param array $cards
     * @param [type] $defaultCardId
     * @return void
     */
    private function _defaultCardFirst(array $cards, $defaultCardId)
    {  
        $result = [];
        foreach($cards as $card) {
            $card->id == $defaultCardId ? array_unshift($result, $card) : array_push($result, $card);
        }
      
        return $result;
    }
}