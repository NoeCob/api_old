<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Request;
use PDO;
use DateTime;
use LaravelAnalytics;
use App\DashboardChart;
use App\Completion;
use Carbon\Carbon;
use App\AppUsage;
use App\Client;

class Main extends Controller
{

	public function __construct(array $except = array())
	{
		$this->middleware('verify_app_version');

		$this->middleware(
			'auth',
			[
				'except' => array_merge($except, ['checkAppVersion', 'getToken', 'postLogin', 'postRegister', 'postEmail', 'postReset', 'on', 'unregistered', 'sendPassword', 'job', 'registerCommissions', 'canceledSubscriptions', 'loginFacebook', 'registerFacebook', 'getRegisterFacebook', 'template', 'invoice', 'appUsage', 'leadSignup', 'facebookWebhook'])
			]
		);

		$this->middleware(
			'csrf',
			[
				'except' => ['checkAppVersion', 'storeSharedClick', 'on', 'sendPassword', 'job', 'invoice', 'registerCommissions', 'canceledSubscriptions', 'registerFacebook', 'getRegisterFacebook', 'loginFacebook', 'template', 'generate', 'leadSignup', 'facebookWebhook']
			]
		);
	}

	public function response($success, $message, $object, $status = 200, $headers = array())
	{
		$request = request();
		$appVersion = $request->input('appVersion');
		$isMovil    = $request->input('isMovilApp');
		$user       = $request->user();

		if ($isMovil) {
            $appUsg = new AppUsage;
            $appUsg->id_clients = '0';
            $appUsg->version = $appVersion;
            
            $client = Client::where('id_users', 828)->first();
            if ($client) {
                $appUsageExists = AppUsage::where('id_clients', $client->id_clients)->first();

                if ($appUsageExists && $appUsageExists->id_clients !== 0) {
                    $appUsageExists->version = $appVersion;
                    // $appUsg->id_clients = $client->id_clients;
                    // echo 'si existe, atualizo su valoir';
                    $appUsageExists->save();
                } else {
                    $appUsg->id_clients = $client->id_clients;
                    // echo 'no existe, creo su valoir';
                    $appUsg->save();
                }
            } else {
                // echo 'no hay usuario';
                $appUsg->save();
            }
        }
		
		return response()->json([
			'success' => $success,
			'message' => $message,
			'response' => $object
		], $status, $headers);
	}

	public function clients() {
		$connection = new PDO('mysql:host=' . env('DB_HOST') . ';dbname=' . env('DB_DATABASE'), env('DB_USERNAME'), env('DB_PASSWORD'));
		$statement = $connection->prepare("SELECT count(*) as quantity, status, SUM(IF(serial_number IS NULL, 0, 1)) AS installed, SUM(IF(subscription_status IS NULL, 0, 1)) AS charged FROM clients GROUP BY status");
		$statement->execute();
		$clients = $statement->fetchAll(PDO::FETCH_OBJ);

		return ['clients' => $clients];
	}

	public function freeWater()
	{
		$clients   = Client::where('status', '=', 'accepted')->get()->toArray();
		$freeWater = [];

		foreach ($clients as $client) {
			$allClients = Client::getHierarchicalTree($client[id_clients])[0]['clients'];

			if (count($allClients) > 5) {
				$validClients = [];
				foreach ($allClients as $clientClient) {
					if ($clientClient[status] == 'accepted' && $clientClient[debt] == 0) {
						$validClients[] = $clientClient;
					}
				}

				if (count($validClients) > 5) {
					$client['tree']      = $allClients;
					$client['treeValid'] = $validClients;
					$freeWater[]         = $client;
				}
			}
		}
		return $freeWater;
	}

	public function subscriptions()
	{
		$params            = Request::query();
		$from              = date('Y-01-01');
		$to                = date('Y-m-d');
		$newSubscriptions  = [];
		$pastSubscriptions = [];

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d');
		}

		$from_past = Carbon::createFromFormat('Y-m-d', $from)->subYears('1')->format('Y-m-d');
		$to_past   = Carbon::createFromFormat('Y-m-d', $to)->subYears('1')->format('Y-m-d');

		$pastSubscriptions = Client::select(DB::raw('COUNT(*) AS quantity, date_format(collected_at, "%Y-%m") as month'))
									->whereBetween('collected_at', [$from_past, $to_past])
									->where('status', '=', 'accepted')
									->whereNotNull('pay_day')
									->orderBy('month')
									->groupBy(DB::raw('MONTH(collected_at)'))
									->get();

		$newSubscriptions = Client::select(DB::raw('COUNT(*) AS quantity, date_format(collected_at, "%Y-%m") as month'))
									->whereBetween('collected_at', [$from, $to])
									->where('status', '=', 'accepted')
									->whereNotNull('pay_day')
									->orderBy('month')
									->groupBy(DB::raw('MONTH(collected_at)'))
									->get();

		return [
			'new'  => $newSubscriptions,
			'past' => $pastSubscriptions
		];
	}

	public function shares()
	{
		$from         = date('Y-01-01');
		$to           = date('Y-m-d');
		$totalShares  = [];
		$sharesByType = [];

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d H:i');
		}
		
		$totalShares = DashboardChart::select(DB::raw('COUNT(*) AS quantity, date_format(created_at, "%Y-%m") as month'))
										->whereBetween('created_at', [$from, $to])
										->orderBy('month')
										->groupBy(DB::raw('MONTH(created_at)'))
										->get();
		
		$sharesByType = DashboardChart::select(DB::raw('COUNT(*) AS quantity, date_format(created_at, "%Y-%m") as month, type'))
										->whereBetween('created_at', [$from, $to])
										->orderBy('month')
										->groupBy(['month', 'type'])
										->get();
		
		return [
			'total'  => $totalShares,
			'byType' => $sharesByType
		];
	}

	public function ticketsCompletion()
	{
		$params            = Request::query();
		$from              = date('Y-01-01');
		$to                = date('Y-m-d');
		$ticketsCompletion = [];

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d H:i');
		}

		$ticketsCompletion = Completion::select(DB::raw('COUNT(*) AS quantity, date_format(updated_at, "%Y-%m") as month'))
										->whereBetween('updated_at', [$from, $to])
										->orderBy('month')
										->groupBy(DB::raw('MONTH(created_at)'))
										->get()
										->toArray();

		return $ticketsCompletion;
	}

	public function dashboard()
	{
		$params = Request::query();
		$from = date('Y-01-01');
		$to   = date('Y-m-d');

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d');
		}

		$from_past = Carbon::createFromFormat('Y-m-d', $from)->subYears('1')->format('Y-m-d');
		$to_past = Carbon::createFromFormat('Y-m-d', $to)->subYears('1')->format('Y-m-d');

		$social = "SELECT DATE_FORMAT(charges.paid_at, '%Y-%m') AS date,
						ROUND(SUM(charges.amount) / 100, 2) AS amount,
						SUM(CASE
										WHEN charges.description = 'Pago tardío' THEN clients.social_responsability * ROUND((debts.amount - ROUND(debts.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Extras' THEN clients.social_responsability * ROUND(((charges.amount - clients.deposit) - ROUND((charges.amount - clients.deposit) / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Cargos adicionales Aguagente' THEN 0
										WHEN charges.description = 'Depósito' THEN 0
										ELSE clients.social_responsability * ROUND((charges.amount - ROUND(charges.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
								END) AS client_social,
						SUM(CASE
										WHEN charges.description = 'Pago tardío' THEN ROUND(IF( clients.social_responsability, (charges.amount - (debts.amount - ROUND(debts.amount / (1 + (1 / 1.16 * .007)), 0))) / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
										WHEN charges.description = 'Extras' THEN ROUND( IF( clients.social_responsability, ((charges.amount - clients.deposit) - ROUND((charges.amount - clients.deposit) / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), (charges.amount - clients.deposit) / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
										WHEN charges.description = 'Cargos adicionales Aguagente' THEN ROUND(charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Depósito' THEN 0
										ELSE ROUND( IF( clients.social_responsability, (charges.amount - ROUND(charges.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
								END) AS aguagente_social
				FROM charges
				INNER JOIN clients ON clients.id_clients = charges.id_clients
				LEFT JOIN debts ON debts.id_charges = charges.id_charges
				LEFT JOIN
					( 
						SELECT 
							id_charges, 
							SUM(amount) AS refunded_amount
					 	FROM refunds
					 	GROUP BY id_charges 
					) AS refunds ON charges.id_charges = refunds.id_charges
				WHERE charges.paid_at IS NOT NULL &&
					charges.paid_at <= ? &&
				charges.paid_at >= ?
				GROUP BY date";

		$connection = new PDO('mysql:host=' . env('DB_HOST') . ';dbname=' . env('DB_DATABASE'), env('DB_USERNAME'), env('DB_PASSWORD'));
		$statement = $connection->prepare($social);
		$statement->bindValue(1, $to);
		$statement->bindValue(2, $from);
		$statement->execute();
		$selectedIncomes = $statement->fetchAll(PDO::FETCH_OBJ);

		$connection = new PDO('mysql:host=' . env('DB_HOST') . ';dbname=' . env('DB_DATABASE'), env('DB_USERNAME'), env('DB_PASSWORD'));
		$statement = $connection->prepare($social);
		$statement->bindValue(1, $to_past);
		$statement->bindValue(2, $from_past);
		$statement->execute();
		$selectedPastIncomes = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare($social);
		$statement->bindValue(1, '3000-12-31');
		$statement->bindValue(2, '2000-01-01');
		$statement->execute();
		$totales = $statement->fetchAll(PDO::FETCH_OBJ);
		
		$totals = [
			'income'           => 0,
			'client_social'    => 0,
			'aguagente_social' => 0
		];
		foreach ($totales as $totalPerMonth) {
			$totals['income']           += $totalPerMonth->amount;
			$totals['client_social']    += $totalPerMonth->client_social;
			$totals['aguagente_social'] += $totalPerMonth->aguagente_social;
		}

		$statement = $connection->prepare("SELECT count(*) as quantity, status FROM tickets GROUP BY status");
		$statement->execute();
		$tickets = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT count(*) as quantity, status, SUM(IF(serial_number IS NULL, 0, 1)) AS installed, SUM(IF(subscription_status IS NULL, 0, 1)) AS charged FROM clients GROUP BY status");
		$statement->execute();
		$clients = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT count(*) as quantity, date_format(created_at, '%Y-%m') as month, CONCAT('subscription.canceled') as type FROM `cancellations` WHERE created_at <= ? && created_at >= ? group by month");
		$statement->bindValue(1, $to);
		$statement->bindValue(2, $from);
		$statement->execute();
		$events = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT * FROM withdrawals");
		$statement->execute();
		$withdrawals = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT ROUND(SUM(amount + collection_fees + moratory_fees) / 100, 2) as amount FROM debts WHERE id_charges IS NULL AND collection_fees > 0");
		$statement->execute();
		$totals['debts'] = (float) $statement->fetchColumn();


		return [
			'totals'               => $totals,
			'incomes'              => $selectedIncomes,
			'past_incomes'         => $selectedPastIncomes,
			'clients'              => $clients,
			'subscription_events'  => $events,

			'tickets'              => [
				'opened' => array_reduce($tickets, function ($q, $ticket) {
					return $ticket->status == 'opened' ? ($q + $ticket->quantity) : $q;
				}, 0),
				'closed' => array_reduce($tickets, function ($q, $ticket) {
					return $ticket->status == 'closed' ? ($q + $ticket->quantity) : $q;
				}, 0),
				'assigned' => array_reduce($tickets, function ($q, $ticket) {
					return $ticket->status == 'assigned' ? ($q + $ticket->quantity) : $q;
				}, 0),
				'confirmed' => array_reduce($tickets, function ($q, $ticket) {
					return $ticket->status == 'confirmed' ? ($q + $ticket->quantity) : $q;
				}, 0),
				'completed' => array_reduce($tickets, function ($q, $ticket) {
					return $ticket->status == 'completed' ? ($q + $ticket->quantity) : $q;
				}, 0)
			],
			'withdrawals' => $withdrawals,
			'general' => LaravelAnalytics::performQuery(new DateTime($from), new DateTime($to), 'ga:users,ga:sessions,ga:screenviews,ga:screenviewsPerSession,ga:avgSessionDuration,ga:percentNewSessions')->rows,
			'views' => LaravelAnalytics::performQuery(new DateTime($from), new DateTime($to), 'ga:screenviews,ga:uniqueScreenviews,ga:avgScreenviewDuration,ga:exitRate', $others = array('dimensions' => 'ga:screenName', 'sort' => '-ga:screenviews', 'filters' => 'ga:screenName!~www.*;ga:screenName!~panel.*;ga:ScreenName!~localhost'))->rows
		];
	}

	public function appUsage()
	{
		$appUsageClients = AppUsage::with(['client'])->where('id_clients', '!=', 0)->orderBy('version')->get()->toArray();
		$appUsageGuest = AppUsage::with(['client'])->where('id_clients', 0)->orderBy('version')->get()->toArray();
		
		return ['clients' => $appUsageClients, 'guests' => $appUsageGuest];
	}


	function checkAppVersion()
	{
		return response()->json(true);
	}


	/*public function pastSubscriptions()
	{
		$params            = Request::query();
		$from              = date('Y-01-01');
		$to                = date('Y-m-d');
		$pastSubscriptions = [];

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d H:i');
		}

		

		return $pastSubscriptions;
	}
	*/

	/*
	public function totalShares()
	{
		$from              = date('Y-01-01');
		$to                = date('Y-m-d');
		$totalShares       = [];
		
		$totalShares = DashboardChart::select(DB::raw('COUNT(*) AS quantity, date_format(created_at, "%Y-%m") as month'))
										->whereBetween('created_at', [$from, $to])
										->orderBy('month')
										->groupBy(DB::raw('MONTH(created_at)'))
										->get()
										->toArray();

		return $totalShares;
	}
	*/

	/*
	public function sharesByType()
	{
		$params       = Request::query();
		$from         = date('Y-01-01');
		$to           = date('Y-m-d');
		$sharesByType = [];

		if (isset($params['from'])) {
			$from = Carbon::createFromTimeString($params['from'])->format('Y-m-d');
		}

		if (isset($params['to'])) {
			$to = Carbon::createFromTimeString($params['to'])->format('Y-m-d H:i');
		}

		$sharesByType = DashboardChart::select(DB::raw('COUNT(*) AS quantity, date_format(created_at, "%Y-%m") as month, type'))
										->whereBetween('created_at', [$from, $to])
										->orderBy('month')
										->groupBy(['month', 'type'])
										->get()
										->toArray();

		return $sharesByType;
	}
	*/
}
