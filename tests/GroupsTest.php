<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GroupsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Groups index test
     *
     * @return void
     */
    public function testGroupsIndex()
    {
        $this->get('/groups')
            ->assertResponseStatus(200);
    }

    public function testGroupsCreate()
    {
        // conekta errors
        $params = $this->getTestData('/data/groups/data.json');
        $this->post('/groups', $params)
            ->assertResponseStatus(201);
    }

    public function testGroupsUpdate()
    {
        // conekta errors
        $params = $this->getTestData('/data/groups/data.json');
        $group = $this->getObjectRandom(\App\Group::class);
        $this->put('/groups/' . $group->id_groups, $params)
            ->assertResponseStatus(200);
    }

    public function testGroupsShow()
    {
        $group = $this->getObjectRandom(\App\Group::class);
        $this->get('/groups/' . $group->id_groups)
            ->assertResponseStatus(200);
    }

    public function testGroupsDestroy()
    {
        // conekta error
        $group = $this->getObjectRandom(\App\Group::class);
        $this->delete('/groups/' . $group->id_groups)
            ->assertResponseStatus(200);
    }

    public function testAssociateClient()
    {
        $group = $this->getObjectRandom(\App\Group::class);
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('/groups/' . $group->id_groups . '/associateClients', ['id_clients' => [$client->id_clients]])
            ->assertResponseStatus(200);
    }

}
