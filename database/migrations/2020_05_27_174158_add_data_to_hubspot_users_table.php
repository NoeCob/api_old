<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToHubspotUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hubspot_users', function (Blueprint $table) {
            $table->string('city', 200)->nullable();
            $table->string('phone_number', 200)->nullable();
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE hubspot_users MODIFY COLUMN vid varchar(200) null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hubspot_users', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('phone_number');
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE hubspot_users MODIFY COLUMN vid varchar(200) not null');
        });
    }
}
