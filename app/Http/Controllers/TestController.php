<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\JsonableInterface;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ChargesController;
use App\Http\Controllers\ClientsController;
use App\ClientService;
use App\Client;
use App\Charge;
use App\Commission;
use App\CommissionService;
use App\Contract;
use App\Debt;
use App\Group;
use App\MySQLClientRepository;
use App\Refund;
use App\Role;
use App\Ticket;
use App\Assignation;
use App\User;
use App\Traits\DebtTrait;
use App\Traits\NotifyTrait;
use Mail;
use DB;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Conekta_ResourceNotFoundError;
use Conekta_ProcessingError;
use Conekta_Handler;
use Webklex\IMAP\Client as ClientImap;
use Illuminate\Support\Facades\Storage;
use App\Invoice;

class TestController extends Controller
{
    use DebtTrait, NotifyTrait;

    public function getClients($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $req = DB::table('clients')
            ->whereIn('referred_by', $ids)
            //->whereRaw("((clients.level != 'VIAJERO') OR ((clients.subscription_status = 'active') && (clients.debt = 0)))")
            ->get();

        return $req;
    }

    public function testComission(Request $request)
    {
        /*$cc = new ClientsController();
        $cc->setLevel(7);
        dd();
		*/
        $id   = $request->input('id');
        if ($id != '') {
            $main = Client::where('id_clients', $id)->first();
            $cs   = new CommissionService();
            return response()->json([
                'success' => true,
                'message' => '',
                'response' => $cs->calculateCommissions($main)
            ]);
        } else {
            $this->calculateCommission();
        }
    }

    public function calculateCommission()
    {
        $salesAgents = Client::where('sales_agent', '=', '1')
            //->where('id_clients', '=', '18')
            //->limit(1)
            ->get();
        $return = [];
        foreach ($salesAgents as $agent) {
            $id    = $agent->id_clients;
            $main  = Client::where('id_clients', $id)->first();
            $cs    = new CommissionService();
            $totals = $cs->calculateCommissions($main, ['', '2018-06-01']);
            $return[$id] = $totals;

            $declined = [];
            foreach ($totals as $key => $value) {
                if ($key != 'isActive') {
                    $declined[$key] = $value['declined'];
                }
            }

            $comission = new Commission;
            $comission->id_clients    = $id;
            $comission->amount        = $totals['level_1']['comission'] + $totals['level_2']['comission'] + $totals['level_3']['comission'] + $totals['level_4']['comission'];
            $comission->calculated_at = Carbon::now();
            $comission->is_active     = $totals['isActive'];
            $comission->level_1       = (isset($totals['level_1']['count']) ? $totals['level_1']['count'] : 0);
            $comission->level_2       = (isset($totals['level_2']['count']) ? $totals['level_2']['count'] : 0);
            $comission->level_3       = (isset($totals['level_3']['count']) ? $totals['level_3']['count'] : 0);
            $comission->level_4       = (isset($totals['level_4']['count']) ? $totals['level_4']['count'] : 0);
            $comission->declined      = $declined;
            //$comission->save();
        }

        dd($return);
    }

    public function testComissionOld()
    {
        //$cc = new ClientsController();
        //$cc->setLevel(18);

        $id       = 18;
        $main     = Client::where('id_clients', $id)->first();

        $ids      = [];
        $idsDebug = [];
        $comision = [];
        $totals   = [];

        if ($main->level != 'VIAJERO') {

            $clientsReferidos = $this->getClients($id);

            foreach ($clientsReferidos as $client) {
                $ids['level_1'][]      = $client->id_clients;
                $idsDebug['level_1'][] = $client->id_clients;

                if ($client->status == 'accepted' && $client->debt == '0.00') {
                    $totals['level_1']['id'][] = $client->id_clients;
                    $comision['level_1']      += 30;
                }
            }

            $maxLevelForPay = 1;
            switch ($main->level) {
                case 'CABO':
                    $maxLevelForPay = 2;
                    break;

                case 'CAPITAN':
                    $maxLevelForPay = 3;
                    break;

                case 'ALMIRANTE':
                    $maxLevelForPay = 4;
                    break;
            }

            for ($i = 2; $i <= $maxLevelForPay; $i++) {
                $level = 'level_' . ($i - 1);

                if (isset($ids[$level])) {

                    $refids = $ids[$level];
                    $ref    = $this->getClients($refids);

                    switch ($i) {
                        case 2:
                            $com = 15;
                            break;

                        case 3:
                            $com = 10;
                            break;

                        case 4:
                            $com = 5;
                            break;
                    }

                    foreach ($ref as $client) {

                        $ids['level_' . $i][]      = $client->id_clients;
                        $idsDebug['level_' . $i][] = [
                            'id'  => $client->id_clients,
                            'ref' => $client->referred_by
                        ];

                        if ($client->status == 'accepted' && $client->debt == '0.00') {
                            $totals['level_' . $i]['id'][] = $client->id_clients;
                            $comision['level_' . $i]      += $com;
                        }
                    }
                }
            }

            //dd($ids, $idsDebug);

            $clients[] = Client::find($id);
            $tree =  Client::getHierarchicalTree($id);

            $clientRepository = new MySQLClientRepository();
            $commissionService = new CommissionService(
                $clientRepository,
                new ClientService($clientRepository)
            );

            foreach ($clients as &$client) {
                $client->commissions = $commissionService->calculateCommissions($client);
            }

            foreach ($totals as $level => $val) {
                $totals[$level]['count'] = count($totals[$level]['id']);
                $totals[$level]['comission'] = $comision[$level];
            }
        }

        dd($clients[0]->commissions, $ids, $totals, $comision);
        //dd($clients, $ids, $idsDebug, $comision, array_sum($comision));
    }

    /**
     * Copia los usuarios existentes a el sandbox de conekta y actualiza su token_id
     */
    public function testCreateSandboxConektaCustomers()
    {
        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_nSQs9v4851teqNGmKTyHjA'));
        Conekta::setLocale('es');
        $cardsTest = [
            'tok_test_visa_4242',
            'tok_test_mastercard_4444',
            'tok_test_amex_0005'
        ];
        $clients = Client::get();
        foreach ($clients as $client) {
            try {
                $exists = Conekta_Customer::find($client->conekta_token);
            } catch (Conekta_ResourceNotFoundError $e) {

                $customer = Conekta_Customer::create(array(
                    'name'  => $client->name,
                    'email' => $client->email,
                    'phone' => str_limit($client->phone, 7, '')
                ));

                $cardTokenRand = array_rand($cardsTest);
                $customer->createCard(
                    array(
                        'token' => $cardsTest[$cardTokenRand]
                    )
                );

                if ($customer) {
                    $client->conekta_token = $customer->id;
                    $client->save();
                }
            }
        }
    }

    /**
     * @TODO: Iterar por los cargos sin pagar y revisar por los que estén declinados y borrarlos de la base de datos
     */
    public function testDailyChargeClients(Request $request)
    {
        $inputs = $request->all();

        $today  = date('Y-m-d');
        if ($inputs[day] != '') {
            $today = date('Y-m-') . str_pad($inputs[day], 2, '0', STR_PAD_LEFT);
        }

        $clients = Client::where('next_payday', '=', $today)
            ->orWhere('pay_tries', '>', 0)
            ->get();

        foreach ($clients as $client) {
            if ($client->status == 'accepted') {
                $debts      = Debt::where('id_clients', '=', $client->id_clients)->whereNull('id_charges')->get();
                $charges    = Charge::where('id_clients', '=', $client->id_clients)
                    ->whereNotIn('description', ['Extras', 'Pago Tardío', 'Depósito'])
                    ->whereNotNull('paid_at')
                    ->orderBy('created_at')
                    ->get();
                $lastCharge = $charges->last();
                $dateToDiff = $lastCharge->created_at;

                $DateLastCharge = explode('Aguagente Período: ', $lastCharge->description);
                if (count($DateLastCharge) > 0) {
                    $realDateLastCharge = $DateLastCharge[1] . '-' . str_pad($client->pay_day, 2, '0', STR_PAD_LEFT);
                    $dateToDiff         = $realDateLastCharge;
                }

                $arrayDiff  = $this->generateDateDiff($dateToDiff);

                if ($client->id_clients == 354) {
                    //dd($debts, $charges, $arrayDiff);
                }

                //intenta cobrar las deudas que tenga el cliente primero.
                $debtsCharged = [];
                foreach ($debts as $debt) {
                    $debtsCharged[] = $this->chargeDebt($debt->id_debts);
                }

                //Si la diferencia de dias entre el ultimo pago y hoy es mayor a 27, genera el cargo
                if ($arrayDiff[1] > 27 || is_null($lastCharge)) {
                    $validClients = [];
                    $allClients   = Client::getHierarchicalTree($client->id_clients)[0]['clients'];

                    foreach ($allClients as $clientClient) {
                        if ($clientClient[debt] == '0.00' && $clientClient[status] == 'accepted') {
                            array_push($validClients, $clientClient);
                        }
                    }

                    Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                    Conekta::setLocale('es');

                    //Aguagratis: clientes activos y al dia > 6
                    if (count($validClients) < 7) {

                        $dateDiffPaytries = $this->generateDateDiff($client->next_payday);
                        if (
                            ($client->pay_tries == 4 && $dateDiffPaytries[1] > 4) || ($client->pay_tries == 5 && $dateDiffPaytries[1] > 6)
                        ) {
                            break;
                        }

                        try {
                            $customer = Conekta_Customer::find($client->conekta_token);
                            $cards    = json_decode($customer->cards->__toJSON());

                            foreach ($cards as $i => $card) {
                                if ($card->id == $customer->default_card_id) {
                                    unset($cards[$i]);
                                    array_unshift($cards, $card);
                                    break;
                                }
                            }

                            $amount = $client->monthly_fee;
                            if ($client->social_responsability == 1) {
                                $srAmount = ($amount / 1.16) * .007;
                                $amount  += $srAmount;
                            }

                            $charged = [];
                            foreach ($cards as $card) {
                                $params = [
                                    'description' => 'Aguagente Período: ' . date('Y-m'),
                                    'amount'      => $amount,
                                    'currency'    => 'MXN',
                                    'card'        => $card->id,
                                    'details'     => [
                                        'name'  => $client->name,
                                        'phone' => $client->phone,
                                        'email' => $client->email,
                                        'line_items' => [
                                            [
                                                'name'        => 'Aguagente Período: ' . date('Y-m'),
                                                'description' => 'Aguagente Período: ' . date('Y-m'),
                                                'unit_price'  => $group->monthly_fee,
                                                'quantity'    => 1
                                            ]
                                        ],
                                        "shipment" => [
                                            "carrier" => "aguagente",
                                            "service" => "next_day",
                                            "price"   => 0,
                                            "address" => [
                                                "street1" => $client->address,
                                                "street2" => $client->address,
                                                "street3" => $client->address,
                                                "city"    => $client->county,
                                                "state"   => $client->state,
                                                "zip"     => $client->postal_code,
                                                "country" => "Mexico"
                                            ]
                                        ]
                                    ]
                                ];
                                try {
                                    $order     = Conekta_Charge::create($params);
                                    $charged[] = true;
                                    break;
                                } catch (Conekta_ProcessingError $e) {
                                    $charged[] = false;
                                }
                            }

                            $charged = collect($charged);
                            if (!$charged->contains(true)) {
                                switch ($client->pay_tries) {
                                    case 5:
                                        $debt                = $this->createDebt($client);
                                        $newNextPayday       = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));

                                        $client->next_payday = $newNextPayday;
                                        $client->debt        = $debt;
                                        $client->pay_tries   = 0;
                                        break;

                                    default:
                                        $client->pay_tries++;
                                        break;
                                }

                                $client->save();
                            } else {
                                $newNextPayday       = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));

                                $client->next_payday = $newNextPayday;
                                $client->pay_tries   = 0;
                                $client->save();
                            }
                        } catch (\Conekta\ResourceNotFoundError $e) {
                            echo $e->getMessage();
                        }

                        //si sus clientes están al dia y tiene más de 6, solo cambia su siguiente día de pago.
                    } else {
                        $newNextPayday       = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));
                        $client->next_payday = $newNextPayday;
                        $client->save();
                    }
                }
            }
        }

        dd(count($clients));
    }

    private function generateDateDiff($next_payday, $testToday = null)
    {
        $today = date_create();
        if (!is_null($testToday)) {
            $today = date_create($testToday);
        }

        $diffNowVsLastCharge = date_diff($today, date_create($next_payday));
        $arrayDiff  = [
            $diffNowVsLastCharge->format('%R'),
            $diffNowVsLastCharge->format('%a')
        ];

        return $arrayDiff;
    }

    public function migrateClientsToNewSystem(Request $request)
    {
        $clientesDel04_09     = [308, 327, 328, 339];
        $clientesFodify5years = [202, 204];
        $clientesFodify1years = [267];
        $clientesConTrial     = [365];
        $manuallyCheck        = [65, 66, 209, 212, 220, 281, 295, 310, 345, 356];

        $idsException = array_merge($clientesDel04_09, $clientesFodify5years, $clientesFodify1years, $clientesConTrial, $manuallyCheck);
        sort($idsException);
        dd($idsException);

        $clients = Client::where('status', '=', 'accepted')
            ->whereNull('pay_day')
            ->whereNotNull('subscription_status')
            ->whereNotIn('id_clients', $idsException)
            ->orderBy('id_clients', 'ASC')
            //->offset(3)->limit(1)
            ->get();

        $clientsMigrated = [];
        $idClients       = [];
        foreach ($clients as $client) {
            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');
            $customer    = Conekta_Customer::find($client->conekta_token);
            $suscripcion = $customer->subscription;

            $today             = Carbon::now();
            $billing_cycle_end = Carbon::createFromTimestamp($suscripcion->billing_cycle_end);
            $nextPayday        = $billing_cycle_end->toDateString();
            $payday            = $billing_cycle_end->day;

            //if ($nextPayday <= $today->toDateString()) {}
            if (!is_null($suscripcion->billing_cycle_end)) {
                $suscripcion->cancel();
                $client->pay_day     = $payday;
                $client->next_payday = $nextPayday;
                $client->save();

                $clientsMigrated[$client->id_clients] = [$payday, $nextPayday];
                $idClients[] = $client->id_clients;
            }
        }

        dd($clientsMigrated, $idClients);
    }

    public function testClients(Request $request)
    {
        $function = $request->input('f');
        switch ($function) {
            case 'cancelledSuscriptions':
                return $this->testCancelledSuscriptionClients($request);
                break;

            case 'doubleCharges':
                return $this->testClientsWithDoubleCharge($request);
                break;

            case 'freeWater':
                return $this->testClientsWithFreeWater($request);
                break;

            case 'clientReferidos':
                return $this->testClientReferidos($request);
                break;

            case 'chargedToday':
                return $this->testGetClientsChargedForToday($request);
                break;

            case 'totalsFromController':
                return $this->testGetTotalsFromConektaAndController($request);
                break;

            case 'debtWithCharges':
                return $this->testDebtsWithCharges($request);
                break;

            default:
                return $this->test($request);
                break;
        }
    }

    public function testCancelledSuscriptionClients(Request $request)
    {
        $clients = Client::where('subscription_status', '=', 'canceled')->whereNull('pay_day')->get()->toArray();
        foreach ($clients as &$client) {
            $paidCharges   = Charge::where('id_clients', $client['id_clients'])->whereNotNull('paid_at')->orderBy('created_at', 'DESC')->limit(5)->get();
            $unpaidCharges = Charge::where('id_clients', $client['id_clients'])->whereNull('paid_at')->orderBy('created_at', 'DESC')->limit(5)->get();

            $client['paidCharges']   = $paidCharges->toArray();
            $client['unpaidCharges'] = $unpaidCharges->toArray();
        }

        return response()->view('tests.cancelled_suscriptions', ['clients' => $clients]);
    }

    public function testClientsWithDoubleCharge(Request $request)
    {

        $clients = Client::where('status', '=', 'accepted')->get()->toArray();

        foreach ($clients as $index => &$client) {
            $paidCharges   = Charge::where('id_clients', $client['id_clients'])
                ->whereNotNull('paid_at')
                ->whereNotIn('description', ['Extras', 'Depósito'])
                ->whereBetween('created_at', ['2018-03-01', '2018-04-01'])
                ->orderBy('created_at', 'DESC')
                ->get();

            if ($paidCharges->count() < 2) {
                unset($clients[$index]);
                //break;
            } else {
                $client['paidCharges'] = $paidCharges->toArray();
                foreach ($client['paidCharges'] as &$charge) {
                    $debt = Debt::where('id_charges', '=', $charge['id_charges'])->first();
                    if ($debt) {
                        $charge['debt'] = $debt->toArray();
                    }
                }
            }
        }
        return response()->view('tests.double_pays', ['clients' => $clients]);
    }

    public function testClientsWithFreeWater(Request $request)
    {
        $clients   = Client::where('status', '=', 'accepted')->get()->toArray();
        $freeWater = [];

        foreach ($clients as &$client) {
            $allClients = Client::getHierarchicalTree($client[id_clients])[0]['clients'];

            if (count($allClients) > 5) {
                $validClients = [];
                foreach ($allClients as $clientClient) {
                    if ($clientClient[status] == 'accepted') {
                        $validClients[] = $clientClient;
                    }
                }

                if (count($validClients) > 5) {
                    $client['tree']      = $allClients;
                    $client['treeValid'] = $validClients;
                    $freeWater[]         = $client;
                }
            }
        }

        return response()->view('tests.free_water', ['clients' => $freeWater]);
    }

    public function testClientReferidos(Request $request)
    {
        $clients   = Client::where('status', '=', 'accepted')->where('id_clients', '=', $request->input('id'))->get()->toArray();
        $freeWater = [];

        foreach ($clients as &$client) {
            $allClients = Client::getHierarchicalTree($client[id_clients])[0]['clients'];

            if (count($allClients) > 5) {
                $validClients = [];
                foreach ($allClients as $clientClient) {
                    if ($clientClient[status] == 'accepted') {
                        $validClients[] = $clientClient;
                    }
                }

                if (count($validClients) > 5) {
                    $client['tree']      = $allClients;
                    $client['treeValid'] = $validClients;
                    $freeWater[]         = $client;
                }
            }
            dd($allClients);
        }

        return response()->view('tests.free_water', ['clients' => $freeWater]);
    }

    public function testGetClientsChargedForToday(Request $request)
    {
        $today = date('Y-m-d');
        if ($request->input('t') != '') {
            $today = date('Y-m-') . $request->input('t');
        }
        $clients = Client::where('next_payday', "=", $today)->get();
        dd($clients);
    }

    public function testDiffDaysAndPaytries(Request $request)
    {
        $today = date('Y-m-d');
        $clients = Client::where('id_clients', 56)->get();
        $payTries = $request->input('p');
        $date = $request->input('d');

        foreach ($clients as $client) {
            if ($client->status == 'accepted') {

                $charges    = Charge::where('id_clients', '=', $client->id_clients)
                    ->whereNotIn('description', ['Extras', 'Pago Tardío', 'Depósito'])
                    ->whereNotNull('paid_at')
                    ->orderBy('created_at')
                    ->get();

                $lastCharge = $charges->last();
                $dateToDiff = $lastCharge->created_at;

                $DateLastCharge = explode('Aguagente Período: ', $lastCharge->description);
                if (count($DateLastCharge) > 1) {
                    $realDateLastCharge = $DateLastCharge[1] . '-' . str_pad($client->pay_day, 2, '0', STR_PAD_LEFT);
                    $dateToDiff         = $realDateLastCharge;
                }

                $arrayDiff  = $this->generateDateDiff($dateToDiff);

                //dd($lastCharge, $DateLastCharge, $dateToDiff, $arrayDiff);
                //Si la diferencia de dias entre el ultimo pago y hoy es mayor a 27, genera el cargo
                if ($arrayDiff[1] > 27 || is_null($lastCharge)) {
                    $dateDiffPaytries = $this->generateDateDiff($client->next_payday, $date);

                    if (
                        ($payTries == 3 && $dateDiffPaytries[1] == 3)
                        || ($payTries == 4 && $dateDiffPaytries[1] == 5)
                    ) {
                        echo 'skip';
                    } else {
                        echo 'try';
                    }
                    dd($dateDiffPaytries);

                    if ($payTries == 4) {
                        echo $dateDiffPaytries[1];
                    }

                    if ($payTries == 5) {
                        echo $dateDiffPaytries[1];
                    }

                    //echo 'try';
                    /*if (
                        ($client->pay_tries == 3 && $dateDiffPaytries[1] == 3) ||
                        ($client->pay_tries == 4 && $dateDiffPaytries[1] >= 6)
                    ){
                    }*/
                }
            }
        }
    }

    private function getChargesFromConekta($today)
    {
        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale('es');

        $init     = $today->startOfMonth()->startOfDay()->timestamp;
        $end      = $today->lastOfMonth()->endOfDay()->timestamp;

        $charges1 = json_decode(Conekta_Charge::where(['created_at.gte' => $init, 'created_at.lte' => $end, 'limit' => 100])->__toJSON());
        $charges2 = json_decode(Conekta_Charge::where(['created_at.gte' => $init, 'created_at.lte' => $end, 'limit' => 100, 'offset' => 100])->__toJSON());
        $charges3 = json_decode(Conekta_Charge::where(['created_at.gte' => $init, 'created_at.lte' => $end, 'limit' => 100, 'offset' => 200])->__toJSON());
        $charges4 = json_decode(Conekta_Charge::where(['created_at.gte' => $init, 'created_at.lte' => $end, 'limit' => 100, 'offset' => 300])->__toJSON());
        $charges5 = json_decode(Conekta_Charge::where(['created_at.gte' => $init, 'created_at.lte' => $end, 'limit' => 100, 'offset' => 400])->__toJSON());

        $charges = [];
        for ($i = 1; $i < 6; $i++) {
            foreach (${'charges' . $i} as $charge) {
                if (!is_null($charge->paid_at)) {
                    $charges[$charge->id] = $charge;
                    $subtotal += $charge->amount;
                    $refunded += $charge->amount_refunded;
                }
            }
        }

        return $charges;
    }

    public function testGetTotalsFromConektaAndController(Request $request)
    {
        $charges = $this->getChargesFromConekta();
        $subtotal = 0;
        $refunded = 0;

        foreach ($charges as $charge) {
            if (!is_null($charge->paid_at)) {
                $subtotal += $charge->amount;
                $refunded += $charge->amount_refunded;
            }
        }

        $subtotal = $subtotal / 100;
        $refunded = $refunded / 100;
        $invoices = $this->getInvoicesFromController($request);

        dd($subtotal, $refunded, $invoices);
    }

    private function getInvoicesFromController(Request $request)
    {
        $c        = new ChargesController();
        $invoices = $c->invoice();

        $subtotal = 0;
        $ivas     = 0;
        $social   = 0;

        $Esubtotal = 0;
        $Eivas     = 0;
        $Esocial   = 0;

        foreach ($invoices as $invoice) {
            if ($invoice[tipo] == "ingreso") {
                $subtotal += $invoice[subtotal];
                $ivas     += $invoice[iva];

                foreach ($invoice['items'] as $item) {
                    if ($item['description'] == 'Responsabilidad social') {
                        $social += $item['unit_price'];
                    }
                }
            } else {
                $Esubtotal += $invoice[subtotal];
                $Eivas     += $invoice[iva];

                foreach ($invoice['items'] as $item) {
                    if ($item['description'] == 'Responsabilidad social') {
                        $Esocial += $item['unit_price'];
                    }
                }
            }
        }
        $totals = [
            'ingresos' => [
                'subtotal' => $subtotal,
                'iva'      => $ivas,
                'total'    => $subtotal + $ivas
            ],
            'egresos' => [
                'subtotal' => $Esubtotal,
                'iva'      => $Eivas,
                'total'    => $Esubtotal + $Eivas
            ]
        ];

        return $totals;
    }

    public function testGenerateInvoicesOld(Request $request)
    {
        $today = Carbon::now();
        $m     = $request->input('m');
        if ($m != '') {
            $today = Carbon::createFromDate('2018', str_pad($m, 2, '0', STR_PAD_LEFT), '01');
        }
        $year  = $today->year;
        $month = $today->month;
        $date  = $today->lastOfMonth()->endOfDay()->toDateString();


        $clientsWithRFC = array_map(
            function ($element) {
                return $element['id_clients'];
            },
            Client::whereNotNull('rfc')
                ->where('rfc', '!=', 'null')
                ->where('rfc', '!=', '')
                ->select('id_clients')
                ->get()
                ->toArray()
        );

        $clientsWithoutRFC = array_map(
            function ($element) {
                return $element['id_clients'];
            },
            Client::whereNull('rfc')
                ->orWhere('rfc', '=', 'null')
                ->orWhere('rfc', '=', '')
                ->select('id_clients')
                ->get()
                ->toArray()
        );

        $condition    =  "     paid_at IS NOT NULL
                            && invoice_status != 'invoiced'
                            && YEAR(paid_at) = ?
                            && MONTH(paid_at) = ?";

        $chargesRfc   = Charge::whereIn('id_clients', $clientsWithRFC)
            ->whereRaw($condition, [$year, $month])
            ->get()
            ->toArray();

        $chargesNoRfc = Charge::whereIn('id_clients', $clientsWithoutRFC)
            ->whereRaw($condition, [$year, $month])
            ->get()
            ->toArray();

        $refunds      = Refund::whereRaw('id_credit_notes IS NULL && YEAR(created_at) = ? && MONTH(created_at) BETWEEN ? AND 12', [$year, $month])->get();

        $charges              = array_merge($chargesRfc, $chargesNoRfc);
        $conektaCharges       = $this->getChargesFromConekta($today);
        $total                = 0;
        $totalConekta         = 0;
        $totalRefunded        = 0;
        $totalRefundedConekta = 0;
        $totalInvaildRefunds  = 0;
        $nonExistent          = [];
        $invalid              = [];
        $invalidRefunds       = [];

        foreach ($charges as $charge) {
            if (!array_key_exists($charge['id_charges'], $conektaCharges)) {
                $nonExistent[] = $charge;
            } else {
                $thisConektaCharge = $conektaCharges[$charge['id_charges']];
                $conektaAmount     = number_format($thisConektaCharge->amount, 2, '.', '');
                $conektaRefund     = number_format($thisConektaCharge->amount_refunded, 2, '.', '');

                if ($charge['amount'] != $conektaAmount) {
                    $invalid[] = $charge;
                } else {
                    $total                += $charge['amount'];
                    $totalConekta         += $conektaAmount;
                    $totalRefundedConekta += $conektaRefund;
                }
            }
        }

        foreach ($refunds as $refund) {
            $totalRefunded += $refund->amount;
            if (!array_key_exists($refund->id_charges, $conektaCharges)) {
                $invalidRefunds[] = $refund;
            }
        }

        if (count($invalidRefunds) > 0) {
            foreach ($invalidRefunds as $invalidRefund) {
                $charge                    = Charge::where('id_charges', '=', $invalidRefund->id_charges)->first();
                $totalInvaildRefunds      += $invalidRefund->amount;
                $invalidRefund->created_at = $charge->created_at;
                //$invalidRefund->save();
            }
            //Re-run the get refunds;
            //$refunds = Refund::whereRaw('id_credit_notes IS NULL && YEAR(created_at) = ? && MONTH(created_at) = ?', [$year, $month])->get();
        }

        $result = [
            'charges'              => $charges,
            'cargosNoExistentes'   => $nonExistent,
            'cargosInvalidos'      => $invalid,
            'refundInvalidos'      => $invalidRefunds,
            'totalERP'             => $total,
            'totalConekta'         => $totalConekta,
            'refundERP'            => $totalRefunded,
            'refundConekta'        => $totalRefundedConekta,
            'diffRefundErpConekta' => $totalRefunded - $totalRefundedConekta,
            'totalRefundInvalidos' => $totalInvaildRefunds
        ];
        dd($result);
    }

    public function testGenerateInvoice()
    {
        $toSend = [
            "client" => [
                "address" => "Foresta 100 Casa 93 coto encino",
                "city"    => "Tlajomulco de Zuñiga",
                "colony"  => "Foresta Santa Anita",
                "state"   => "Jalisco",
                "cp"      => "45645",
                "email"   => "raiders11@gmail.com",
                "name"    => "Laura Margarita Gonzalez Martinez / Carlos Alberto",
                "rfc"     => "GOOC741101I68",
            ],
            "items" => [
                [
                    "description" => "Aguagente Período: 2018-10 (2018-10-31)",
                    "quantity"    => 1,
                    "unit_price"  => 171.39,
                    "iva"         => 27.42,
                ],
                [
                    "description" => "Responsabilidad social",
                    "quantity"    => 1,
                    "unit_price"  => 1.37,
                    "iva"         => 0,
                ]
            ],
            "tipo"     => "ingreso",
            "subtotal" => 172.76,
            "iva"      => 27.42,
            "total"    => 200.18,
            "fecha"    => "2018-11-07"
        ];

        try {
            //$toSend = $invoice;
            //unset($toSend[ids]);

            $response = (new \GuzzleHttp\Client())
                ->request(
                    'POST',
                    'http://cfdipro.info/api/webservice/v3/aguagente_cfdi',
                    [
                        'json' => $toSend,
                        'headers' => [
                            'Authorization' => 'Basic:QUdVMTUwMjEzREI2OkFHVTE1MDIxM0RCNg=='
                        ]
                    ]
                );

            $body = json_decode($response->getBody()->__toString());

            $ids = implode(',', array_fill(0, count($invoice['ids']), '?'));
        } catch (\Exception $e) {
            dd($e);
            echo 'Client: ' . $invoice[client][name] . ' ' . $e->getMessage() . '<br>';
        }
    }

    public function testDebtsWithCharges(Request $request)
    {
        $clients = $this->getClients(18);

        foreach ($clients as &$client) {
            $client->debts = Debt::where('id_clients', $client->id_clients)->orderBy('created_at')->get();
            foreach ($client->debts as &$debt) {
                $debt->charge = Charge::where('id_charges', $debt->id_charges)->orderBy('created_at')->get();
            }
            $client->charges = Charge::where('id_clients', $client->id_clients)->orderBy('created_at')->get();
        }

        return response()->view('tests.charges_debts', [clients => $clients]);
    }

    public function testOxxo()
    {
        $url = base_path('libs/conekta/Conekta.php');
        require_once($url);

        \Conekta\Conekta::setApiKey('key_nSQs9v4851teqNGmKTyHjA');
        \Conekta\Conekta::setLocale('es');

        //$customer = \Conekta\Customer::find("cus_2inMDXxvYVXZgnEg8");

        $data  = [
            'description'    => 'Asd',
            'customer_id'    => 'cus_2inMDXxvYVXZgnEg8',
            'currency'   => 'MXN',
            'line_items' => [
                [
                    'name'        => 'Gastos de administración',
                    'description' => 'Gastos de administración',
                    'unit_price'  => 10000,
                    'quantity'    => 1
                ]
            ],
            'shipping_lines' => [
                [
                    "amount" => 0,
                    "carrier" => "AGUAGENTE"
                ]
            ],
            'shipping_contact' => [
                'address' => [
                    'street1'     => 'Asd',
                    'street2'     => 'asd',
                    'city'        => 'Merida',
                    'state'       => 'Yucatan',
                    'postal_code' => '97000',
                    'country'     => 'Mexico'
                ]
            ],
            'customer_info' => [
                'customer_id' => 'cus_2inMDXxvYVXZgnEg8'
            ],
            'charges' => [
                [
                    'description'    => 'Asd',
                    'customer_id'    => 'cus_2inMDXxvYVXZgnEg8',
                    'payment_method' => [
                        'type' => 'oxxo_cash'
                        //"type" => "default"
                    ]
                ]
            ]
        ];


        try {
            $order = \Conekta\Order::create($data);
            dd($order);
        } catch (\Exception $e) {
            $response = [
                'error'     => 1,
                'message' =>  $e->getMessage()
            ];
            dd($response);
        }
    }

    public function testCreateTickets(Request $request)
    {
        $today = Carbon::now()->endOfMonth();

        $clients = Client::whereRaw('MONTH(created_at) = ' . $today->month)->where('status', 'accepted')->get();
        $errorCode = [
            1 => 69, //odd
            2 => 70  //even
        ];

        foreach ($clients as $client) {
            $diff = $today->diffInYears($client->created_at);
            $code = 1;
            if ($diff % 2 == 0) {
                $code = 2;
            }

            $ticket = new Ticket;
            $ticket->id_clients                    = $client->id_clients;
            $ticket->id_error_codes                = $errorCode[$code];
            $ticket->description                   = 'Cambio anual de filtros (' . ($code == 1 ? '1 año' : '2 años') . ')';
            $ticket->estimated_service_fee         = null;
            $ticket->estimated_service_fee_reasons = null;
            $ticket->status                        = 'opened';
            $ticket->type                          = 'filter_change';
            $ticket->save();

            //dd($client, $ticket);
        }
    }

    public function test(Request $request)
    {   
        $clients = json_decode(Storage::disk('nosense')->get('data-0-100.json'));
        foreach($clients as $jsonClient) {
            $clientExist = Client::where('email', $jsonClient->email)->first();
            $client;
            if (count($clientExist) > 0) {
                $client = $clientExist;
            } else {
                $this->createClient($jsonClient);
            }
        }
        dd($clients);
    }

    private function createClient($client) {
        dd($client);
    }

    private function resolveRelations($client)
    {

        $group = DB::table('groups')
            ->where('id_groups', $client->id_groups)
            ->first();

        $group->sign_into = Group::find($group->sign_into);

        $client->group = $group;
        $client->contract = Contract::where('id_clients', '=', $client->id_clients)->first();

        $roles = DB::table('role_user')
            ->where('user_id', $client->id_users)
            ->get();

        foreach ($roles as &$rol) {

            $rol = Role::find($rol->role_id);
        }

        $client->roles = $roles;

        return $client;
    }


    public function testEmail()
    {

        /*$directorio = public_path('invoices_files/');
        $archivos  = scandir($directorio);

        foreach ($archivos as $archivo => $value) {

            if ($value != '.' && $value != '..') {
                $pathFile = $directorio . $value;
                $fileType = pathinfo($pathFile, PATHINFO_EXTENSION);;
                if ($fileType == 'xml') {
                    $doc       = new \DOMDocument;
                    $xmlReader = new \XMLReader;
                    $xmlReader->open($pathFile);
                    $attachmentStringXML = file_get_contents($pathFile);
                    $dateStamp           = null;
                    while ($xmlReader->read()) {
                        if ($xmlReader->name == 'cfdi:Comprobante') {
                            $nodeInvoice  = simplexml_import_dom($doc->importNode($xmlReader->expand(), true));
                            $arrayInvoice = $this->getAssociativeArrayXml($nodeInvoice->attributes());
                            $dateStamp    = $arrayInvoice["Fecha"];
                        }

                        if ($xmlReader->name == 'cfdi:Receptor') {
                            $nodeReceptor  = simplexml_import_dom($doc->importNode($xmlReader->expand(), true));
                            $arrayReceptor = $this->getAssociativeArrayXml($nodeReceptor->attributes());
                            $rfc    = $arrayReceptor["Rfc"];
                            $client = Client::where('rfc', $rfc)->orWhere('invoice_data', 'like', "%$rfc%")->first();

                            if ($client != null) {
                                Invoice::create(
                                    array(
                                        'xml'        => $attachmentStringXML,
                                        'id_clients' => $client->id_clients,
                                        'date_stamp' => $dateStamp,
                                        'filename'   => str_replace('.xml', '', $value)
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }*/
        //\Artisan::call('get_invoices_email', []);
        DB::enableQueryLog();
        $invoice = Invoice::with(['client'])->find(7);

        //$client = $invoice;

        dd(\GuzzleHttp\json_encode($invoice));
    }

    private function getAssociativeArrayXml($attributes)
    {
        $arrayData = [];
        foreach ($attributes as $a => $b) {
            $arrayData[$a] = (String)$b;
        }
        return $arrayData;
    }
}
