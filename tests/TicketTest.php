<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Client;
use App\ErrorCode;
use App\Ticket;
use App\Assignation;

class TicketTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * get tickets test.
     *
     * @return void
     */
    public function testGetTickets()
    {
        $params = $this->getTestData('/data/tickets/fetch.json');
        /* @var $params ArrayObject */
        $this->json('GET', '/tickets/get_tickets', $params)
            ->assertResponseStatus(200);
    }

    public function testTicketsIndex()
    {
        $this->get('/tickets')
            ->assertResponseStatus(200);
    }

    public function testTicketsCreate()
    {

        $clientToUse = $this->getObjectRandom(Client::class);
        $errorCodeToUse = $this->getObjectRandom(ErrorCode::class);

        $params = [
            'id_clients' => $clientToUse->id_clients,
            'id_error_codes' => $errorCodeToUse->id_error_codes,
            'description' => 'description test' . strtotime("now"),
            'estimated_service_fee' => 500.00,
            'estimated_service_fee_reasons' => 'reasons test' . strtotime("now"),
            'type' => 'installation'
        ];

        $this->post('/tickets', $params)
            ->assertResponseStatus(201);
    }

    public function testTicketsShow()
    {
        $ticketToUse = $this->getObjectRandom(Ticket::class);
        $this->get('/tickets/' . $ticketToUse->id_tickets)
            ->assertResponseStatus(200);
    }

    public function testTicketsSetStatus()
    {
        $technicians = $this->get('/users?role=Technician');
        $data = json_decode($technicians->response->content());
        $index = sizeof($data->response) - 1;
        $technician = $data->response[$index];
        $ticket =  $this->getObjectRandom(Ticket::class);
        $params = [];

        switch ($ticket->status) {
            case 'opened':
                $params = [
                    'status' => 'assigned',
                    'id_technicians' => $technician->id,
                    'details' => 'details test' . strtotime("now")
                ];
                break;
            case 'assigned':
                $assignation = Assignation::where('id_tickets', '=', $ticket->id_tickets)->latest()->first();
                $params = [
                    'id_assignations' => $assignation->id_assignations,
                    'status' => 'completed',
                    'extra_charges' => 0,
                    'extra_charges_reasons' => 'no extra charges',
                    'recipient' => 'test' . strtotime("now"),
                    'id_error_codes' => $ticket->id_error_codes,
                    'tds_in' => 0,
                    'tds_out' => 0,
                    'used_parts' => 'test',
                    'work_description' => 'test work description' . strtotime("now"),
                    'photos' => null,
                    'serial_number' => 'test serial number' . strtotime("now")
                ];
                break;
            case 'completed':
            case 'confirmed':
                $params = [
                    'status' => 'closed',
                    'final_service_fee' => 0,
                    'final_service_fee_reasons' => 'sin cargos extras' . strtotime("now")
                ];
                break;
        }

        if ($ticket->status != 'closed') {
            $this->post('tickets/' . $ticket->id_tickets . '/setStatus', $params)
                ->assertResponseStatus(200);
        } else {
            $this->assertTrue(true);
        }


    }

    public function testTicketsSendEmail()
    {
        $objects = Ticket::where('status','=','closed')->get();
        $maxLength = sizeof($objects);
        // Getting number random with the max length of the clients existing
        $index = rand(0, $maxLength - 1);
        $ticket = $objects[$index];
        print($ticket->status);
        $this->post('tickets/' . $ticket->id_tickets . '/send-mail')
            ->assertResponseStatus(200);
    }
}
