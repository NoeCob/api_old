<?php

namespace App\Http\Controllers;

use App\Group;
use App\Fee;
use App\Client;
use App\Http\Controllers\Main;
use DB;
use Request;
use Validator;
use Conekta;
use Conekta_Plan;
use Conekta_Customer;

class GroupsController extends Main {

    /**
     * index
     * Un grupo es el plan de suscripcion al cual se va a suscribir un cliente.
     * 
     * Devuelve todas los grupos después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK|Internal Server Error(500)
     */
    public function index() {

        try {

            $groups = DB::table('groups');

            foreach(Request::query() as $name => $value) {

                $groups = $groups->where($name, $value);

            }

            $groups = $groups->get();

            foreach($groups as &$group)

                $group = $this->resolveRelations($group);

            return Main::response(true, 'OK', $groups);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }

    }

    
    /**
     * create
     * Crea un grupo (\App\Group) en la base de datos y da de alta el plan en CONEKTA(Conekta_Plan).
     * 
     * @Conekta
     * @Conekta_Plan
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Group
     * @App\Fee
     * 
     * @return response Created(201)|Internal Server Error(500)
     */
    public function create() {

        try {

            $validator = Validator::make(
                $input = Request::all(),
                [
                    'name'              => 'required|unique:groups|max:50|min:5',
                    'installation_fee'  => 'required|numeric',
                    'monthly_fee'       => 'required|numeric',
                    'deposit'           => 'required|numeric',
                    'sign_into'         => 'integer',
                    'trial_days'        => 'required|integer',
                    'trial_days_price'  => 'required|numeric'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', [ 'errors' => $validator->errors() ], 400);

            }

            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale("es");

            $percentage = Fee::where('brand', '=', 'AMERICAN_EXPRESS')
                             ->where('monthly_installments', '=', 1)
                             ->first()
                             ->percentage;

            /*
            $plan = Conekta_Plan::create(
                array(
                    'name'=> $input['name'],
                    'amount'=> $input['monthly_fee'],
                    'currency'=> 'MXN',
                    'interval'=> 'month',
                    'expiry_count' => 60,
                    'trial_period_days' => $input['trial_days']
                )
            );

            $socialPlan = Conekta_Plan::create(
                array(
                    'name'=> $input['name'] . " (Social)",
                    'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] / 1.16 * .007),
                    'currency'=> 'MXN',
                    'interval'=> 'month',
                    'expiry_count' => 60,
                    'trial_period_days' => $input['trial_days']
                )
            );

            $amexPlan = Conekta_Plan::create(
                array(
                    'name'=> $input['name'] . " (Amex)",
                    'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] * $percentage / 100),
                    'currency'=> 'MXN',
                    'interval'=> 'month',
                    'expiry_count' => 60,
                    'trial_period_days' => $input['trial_days']
                )
            );

            $socialAmexPlan = Conekta_Plan::create(
                array(
                    'name'=> $input['name'] . " (Social Amex)",
                    'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] / 1.16 * .007) + ($input['monthly_fee'] * $percentage / 100),
                    'currency'=> 'MXN',
                    'interval'=> 'month',
                    'expiry_count' => 60,
                    'trial_period_days' => $input['trial_days']
                )
            );
            */

            $group = new Group;
            $group->name = $input['name'];
            $group->installation_fee = $input['installation_fee'];
            $group->monthly_fee = $input['monthly_fee'];
            $group->deposit = $input['deposit'];
            $group->sign_into = @ $input['sign_into'];
            $group->plan_id = '';
            $group->social_plan_id = '';
            $group->amex_plan_id = '';
            $group->social_amex_plan_id = '';
            $group->trial_days = $input['trial_days'];
            $group->trial_days_price = $input['trial_days_price'];
            $group->save();

            if(!isset($input['sign_into'])) {

                $group->sign_into = $group->id_groups;
                $group->save();

            }

            return Main::response(true, 'Created', $this->resolveRelations($group), 201);


        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    
    /**
     * show
     * Muestra la información de un grupo(\App\Group) por medio de su ID($id)
     * 
     * @App\Group
     * 
     * @param int       $id   ID del Grupo
     * @return response       OK|Not Found
     */
    public function show($id) {

        if($group = Group::find($id)) {

            return Main::response(true, 'OK', $group);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }


    /**
     * update
     * Guarda las modificaciones AL NOMBRE de un grupo y las actualiza en CONEKTA
     * 
     * @Conekta
     * @Conekta_Plan
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Group
     * 
     * @param int       $id ID del Grupo
     * @return reponse      OK|Bad Request(400)|Not Found(404)|Error(500)
     */
    public function update($id) {

        if($group = Group::find($id)) {

            try {

                $validator = Validator::make(
                    $input = Request::all(),
                    [
                        'name'              => "required|unique:groups,name,$group->id_groups,id_groups|max:50|min:5",
                        'installation_fee'  => 'required|numeric',
                        'monthly_fee'       => 'required|numeric',
                        'deposit'           => 'required|numeric',
                        'sign_into'         => 'integer',
                        'trial_days'        => 'required|integer',
                        'trial_days_price'  => 'required|numeric'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', [ 'errors' => $validator->errors() ], 400);

                }

                Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                Conekta::setLocale("es");

                $percentage = Fee::where('brand', '=', 'AMERICAN_EXPRESS')
                                 ->where('monthly_installments', '=', 1)
                                 ->first()
                                 ->percentage;

                /*
                $plan = Conekta_plan::find($group->plan_id);
                $plan->update(
                    array(
                        'name'=> $input['name'],
                        'amount'=> $input['monthly_fee'],
                        'trial_period_days'=> $input['trial_days']
                    )
                );

                $socialPlan = Conekta_plan::find($group->social_plan_id);
                $socialPlan->update(
                    array(
                        'name'=> $input['name'] . " (Social)",
                        'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] / 1.16 * .007),
                        'trial_period_days'=> $input['trial_days']
                    )
                );

                $amexPlan = Conekta_plan::find($group->amex_plan_id);
                $amexPlan->update(
                    array(
                        'name'=> $input['name'] . " (Amex)",
                        'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] * $percentage / 100),
                        'trial_period_days'=> $input['trial_days']
                    )
                );

                $amexSocialPlan = Conekta_plan::find($group->social_amex_plan_id);
                $amexSocialPlan->update(
                    array(
                        'name'=> $input['name'] . " (Social Amex)",
                        'amount'=> $input['monthly_fee'] + ($input['monthly_fee'] / 1.16 * .007) + ($input['monthly_fee'] * $percentage / 100),
                        'trial_period_days'=> $input['trial_days']
                    )
                );
                */

                $group->name = $input['name'];
                $group->installation_fee = $input['installation_fee'];
                $group->monthly_fee = $input['monthly_fee'];
                $group->deposit = $input['deposit'];
                $group->trial_days = $input['trial_days'];
                $group->trial_days_price = $input['trial_days_price'];
                if(isset($input['sign_into'])) $group->sign_into = $input['sign_into'];
                $group->save();

                return Main::response(true, 'OK', $this->resolveRelations($group));

            } catch(\Exception $e) {

                return Main::response(false, $e->getMessage(), null, 500);

            }

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }


    /**
     * destroy
     * ¡PELIGROSO!
     * Elimina un grupo (\App\Group) de la base de datos y de CONEKTA (Conekta_Plan)
     * 
     * @todo Al borrar un grupo, a los clientes se les debe asignar otro automáticamente.
     *
     * @param id        $id ID del Grupo
     * @return reponse      OK|Not Found(404)
     */
    public function destroy($id) {

        if($group = Group::find($id)) {

	          Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale("es");

            /*
            $plan = Conekta_plan::find($group->plan_id);
	        $plan->delete(); 
            
            $socialPlan = Conekta_plan::find($group->social_plan_id);
            $socialPlan->delete();
            
            $amexPlan = Conekta_plan::find($group->amex_plan_id);
            $amexPlan->delete();
            
            $socialAmexPlan = Conekta_plan::find($group->social_amex_plan_id);
            $socialAmexPlan->delete();
            */
            
            $group->delete();

            return Main::response(true, 'OK', null);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    
    /**
     * resolveRelations
     * ¿Devuelve los grupos relacionados a un grupo?
     * 
     * @param  Group $group Objeto Eloquent Group
     * @return Group $group Objeto Eloquent Group
     */
    private function resolveRelations($group) {

        $group->sign_into = Group::find($group->sign_into);

        return $group;

    }

    /**
     * associateClients
     * Genera la relación en la base de datos, de el grupo con un array de clientes.
     * El array de id's de clientes se obtiene de la variable, pormedio del trait de Laravel REQUEST
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Group
     * @App\Client
     * 
     * @param  int $id
     * @return response     OK|Bad Request(400)|Not Found(404)
     */
    public function associateClients($id) {

        if($group = Group::find($id)) {

            $validator = Validator::make(
                $input = Request::all(),
                [
                    'id_clients' => 'required|array'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', [ 'errors' => $validator->errors() ], 400);

            }

            foreach($input['id_clients'] as $id_client) {
              
                $group = Group::find($id);
                
                $client = Client::find($id_client);
                $client->id_groups          = $id;
                /*$client->deposit            = $group->deposit;
                $client->installation_fee   = $group->installation_fee;
                $client->monthly_fee        = $group->monthly_fee;*/
                $client->save();                

            }

            return Main::response(false, 'OK', null);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
