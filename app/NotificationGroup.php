<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationGroup extends Model
{
    protected $primaryKey = 'id_notification_groups';
    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
