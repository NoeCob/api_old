<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tickets')) {
            return;
        }

        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id_tickets');
            $table->unsignedInteger('id_clients')->nullable(false);
            $table->unsignedInteger('id_error_codes')->nullable(false);
            $table->unsignedInteger('id_closed_by')->default(NULL);
            $table->text('description');
            $table->decimal('estimated_service_fee', 10, 2)->default(NULL);
            $table->text('estimated_service_fee_reasons');
            $table->enum('status', ['opened', 'assigned', 'completed', 'confirmed', 'closed'])->nullable(false);
            $table->decimal('final_service_fee', 10, 2)->nullable(false);
            $table->text('final_service_fee_reasons')->nullable(false);
            $table->enum('type', ['installation', 'service_call', 'filter_change', 'removal', 'move'])
                ->default(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
