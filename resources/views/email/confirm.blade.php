@extends('layouts.email')

@section('name')
{{ $nombre }}
@endsection

@section('content')
Muchas gracias por registrarte el servicio de Aguagente.&nbsp;Esperamos ofrecerte un buen servicio y que disfrutes con mayor tranquilidad tu agua y empieces tu jornada hacia ‘Agua Gratis’!<br><br>
Para finalizar el proceso de registro de click en el siguiente enlace.<br><br>
<a href="{{ $confirm_data }}">Confirmar Cuenta</a>
<br/>

Gracias y disfruta de Aguagente!
@endsection