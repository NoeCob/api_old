@extends('layouts.base')

@section('title', 'Lista de Clientes y sus ultimos 15 cargos')

@section('content')
    <div class="well">
        <h4>Lista de Clientes con y sus últimos <strong class="text-warning">15 cargos</strong></h4>
        <hr>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Estatus de Cliente</th>
                <th>Estatus de Suscripcion</th>
                <th>Menusalidad</th>
                <th>Deuda</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client[id_clients]}}</td>
                    <td>{{$client[name]}}</td>
                    <td><strong>{{$client[status]}}</strong></td>
                    <td>{{$client[subscription_status]}}</td>
                    <td>{{$client[monthly_fee] / 100}}</td>
                    <td>{{number_format($client[debt] / 100,2)}}</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div style="padding:0 20px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" id="c_{{$client[id_clients]}}">
                                    <h3 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#collapse{{$client[id_clients]}}" aria-expanded="true" aria-controls="collapse{{$client[id_clients]}}">
                                            Referidos (+)
                                        </a>
                                    </h3>
                                </div>
                                <div id="collapse{{$client[id_clients]}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$client[id_clients]}}">
                                    <div class="panel-body">
                                        <h4>Cargos</h4>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-3">ID</th>
                                                    <th class="col-xs-3">Descripción</th>
                                                    <th class="col-xs-2">Estatus del pago</th>
                                                    <th class="col-xs-2">Total</th>
                                                    <th class="col-xs-1">Devolución</th>
                                                    <th class="col-xs-1">Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client->charges as $charge)
                                                    <tr>
                                                        <td>{{$charge->id}}</td>
                                                        <td>{{$charge->description}}</td>
                                                        <td><strong>{{$charge->status}}</strong></td>
                                                        <td>{{$charge->amount / 100}}</td>
                                                        <td>{{$charge->amount_refunded / 100}}</td>
                                                        <td>{{date('Y-m-d', $charge->created_at)}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {!! $clients->render() !!}

<script>
jQuery(function($){
    $('.openModal').on('click', function(e){
        e.preventDefault();
        var _this = $(this),
            _data = _this.data(),
            _monto = parseFloat(_data.info.amount+_data.info.collection_fees+_data.info.moratory_fees),
            _modal = {
                title: 'Cargo: '+_data.info.id_charges,
                message:'<h4>Deuda en ERP</h4>'+
                        '<table class="table">'+
                            '<tr><th>Deuda ID</th> <th>Monto</th> <th>Fecha</th></tr>'+
                            '<tr><td>'+_data.info.id_debts+'</td> <td>$ '+(_monto) / 100+'</td> <td>'+_data.info.created_at+'</td></tr>'+
                        '</table>',
                buttons: {
                    ok: {
                        label: "Cerrar",
                        className: 'btn-primary'
                    }
                }
            };

        bootbox.dialog(_modal);
    });
});
</script>
@endsection