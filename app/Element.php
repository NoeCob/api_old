<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Element extends Model
{

    protected $table = 'categories_elements';
    protected $primaryKey = 'id_categories_elements';

    public function category() {
        return $this->belongsTo('App\Category', 'id_categories');
    }

    public function save(array $options = [])
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Saving/Updating Element' . $this->original['id_categories_elements'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        parent::save($options);
        $log->table_id = $this->original['id_categories_elements'];
        $log->save();
    }

    public function delete()
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Deleting Category Element: ' . $this->original['id_categories_elements'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        $log->table_id = $this->original['id_categories_elements'];
        $log->save();
        return parent::delete();
    }
}

