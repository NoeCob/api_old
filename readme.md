## AguaGente

Desarrollo de la API para sitio web y app "AguaGente"

Requerimientos:
- PHP > 5.5.9
- Laravel 5.1
- MySQL

## Documentación

@todo Agregar URL
@todo Actualizar googleApi y https://github.com/spatie/laravel-analytics
@todo Agregar https://github.com/ivanvermeyen/laravel-google-drive-demo

## Issue Tracker

[https://bitbucket.org/aguagente/api/issues](https://bitbucket.org/aguagente/api/issues?status=new&status=open)


## Documentation Generators

**phpDocumentator**

php phpDocumentor.phar -d app/ -t docs/phpDoc --ignore "app/Console/,app/Events/,app/Exceptions/,app/Jobs/,app/Listeners/,app/Policies/,app/Providers/,app/Http/Requests/"

**apiGen**

Para usar apiGen, hay que seguir esta [guía](https://github.com/ApiGen/ApiGen/issues/850#issuecomment-302050948)

apiGen\vendor\bin\apigen generate --destination api\docs\apiGen -s api\app --exclude app\Console\,app\Events\,app\Exceptions\,app\Jobs\,app\Listeners\,app\Policies\,app\Providers\,app\Http\Requests\
