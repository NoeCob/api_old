<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Commission extends Model
{

    protected $table = 'commissions';
    protected $primaryKey = 'id_commissions';

    /**
     * getDeclinedAttribute
     * Laravel Muttator GETTER, convierte el string en formato JSON almacenado, en un OBJETO de PHP
     *
     * @param array $value String en formato JSON de la base de datos.
     * @return object       Objeto JSON
     */
    public function getDeclinedAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * setDeclinedAttribute
     * Laravel Muttator SETER, convierte un array u objeto en JSON.
     *
     * @param array $value Array de valores
     * @return void
     */
    public function setDeclinedAttribute($value)
    {
        $this->attributes['declined'] = json_encode($value);
    }

    public function save(array $options = [])
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Saving/Updating Commission' . $this->original['id_commissions'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        parent::save($options);
        $log->table_id = $this->original['id_commissions'];
        $log->save();
    }

    public function delete()
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Deleting Commission: ' . $this->original['id_commissions'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        $log->table_id = $this->original['id_commissions'];
        $log->save();
        return parent::delete();
    }

}
