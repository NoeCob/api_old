<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Notification;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\NotificationType;
use App\SuccessJob;

class SendNotificationEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $messageData;
    protected $subject = 'Notificación Automatica';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $message)
    {
        $this->user        = $user;
        $this->messageData = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $data = array(
            'mensaje' => $this->messageData,
            'name'    => $this->user->name,
        );

        $mail = $this->user->email;
        $name = $this->user->name;

        $idUser  = $this->user->id;
        $subject = $this->subject;

        $dataInformation = array(
            'data'    => $data,
            'mail'    => $mail,
            'name'    => $name,
            'idUser'  => $idUser,
            'subject' => $subject
        );

        SuccessJob::create(
            array(
                'payload' => json_encode($dataInformation)
            )
        );

        $mailer->send('email.notification', $data , function ($message) use($mail,$name,$subject,$idUser){
            $message->to(
                $mail,
                $name
            )->replyTo('contacto@aguagente.com', 'Contacto')
                ->subject($subject);
        });
    }
}
