<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CancellationsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testCancellationIndex()
    {
        $this->get('/cancellations')
            ->assertResponseStatus(200);
    }

    public function testCancellationShow()
    {
        $cancellation = $this->getObjectRandom(\App\Cancellation::class);
        $this->get('/cancellations/' . $cancellation->id_cancellations)
            ->assertResponseStatus(200);
    }

}
