<?php

namespace App\Http\Controllers;

use App\Helpers\FacebookVendor;
use App\Helpers\HubspotVendor;
use App\Http\Controllers\Controller;
use App\Models\HubspotUser;
use App\User;
use App\Client;
use App\Role;
use App\Group;
use App\Contract;
use App\Device;
use App\Traits\NotifyTrait;
use Illuminate\Cache\RateLimiter;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Mail\Message;
use Illuminate\Http\Request;
use Validator;
use DB;
use Conekta;
use Conekta_Customer;
use Facebook\Facebook;
use Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Main
{

    use ThrottlesLogins, NotifyTrait;

    public function __construct()
    {

        parent::__construct(['postLogin', 'postLoginErp']);
    }

    /**
     * index
     * Devuelve todas los usuarios (\App\Users) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @param  Request $request
     * @return response         OK|Internal Server Error(500)
     */
    public function index(Request $request)
    {

        try {

            $users = DB::table('users');

            foreach ($request->query() as $name => $value) {

                switch ($name) {

                    case 'name':
                        $users = $users->where($name, 'LIKE', "%$value%");
                        break;

                    case 'role':
                        $role = DB::table('roles')
                            ->where('name', $value)
                            ->first();
                        $users = DB::table('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.updated_at', 'users.image', 'users.type', 'users.phone', 'users.contractor', 'users.status')
                            ->where('role_id', $role ? $role->id : null)
                            ->whereNotNull('status');

                        break;

                    default:
                        $users = $users->where($name, $value);
                        break;
                }
            }

            $users = $users->get();

            foreach ($users as &$user) {

                $user = $this->resolveRelations($user);
            }

            return Main::response(true, 'OK', $users);
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    /**
     * getToken
     * Genera un TOKEN para que se puedan comunicar
     *
     * @return response     Token
     */
    public function getToken()
    {

        return response()->json(['token' => csrf_token()]);
    }

    /**
     * getLogout
     *
     * @return void
     */
    public function getLogout()
    {

        Auth::logout();
    }

    /**
     * loginUsername
     *
     * @return void
     */
    public function loginUsername()
    {

        return 'email';
    }

    /**
     * postLogin
     *
     * @param Request $request
     * @return void
     */
    public function postLogin(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|max:255',
                'password' => 'required|min:6|max:60'
            ]
        );

        #file_put_contents('log', json_encode($request->all()) . " " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        } else if ($this->hasTooManyLoginAttempts($request)) {

            $seconds = app(RateLimiter::class)->availableIn($this->getThrottleKey($request));

            return response("Too many login attempts. Please try again in $seconds seconds.", 401);
        } else if (Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {

            //if(!isset($input['appVersion'])) return response('WAAAAAAA', 401);

            $user = Auth::user();

            $this->clearLoginAttempts($request);

            return response()->json(
                [
                    'user' => $this->resolveRelations($user)
                ]
            );
        } else {
            //dd('qwe');
            $this->incrementLoginAttempts($request);

            return response(null, 401);
        }
    }

    /**
     * postLoginErp
     *
     * @param Request $request
     * @return void
     */
    public function postLoginErp(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|max:255',
                'password' => 'required|min:6|max:60'
            ]
        );

        if ($validator->fails()) {
            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        } else if ($this->hasTooManyLoginAttempts($request)) {
            $seconds = app(RateLimiter::class)->availableIn($this->getThrottleKey($request));

            return response("Too many login attempts. Please try again in $seconds seconds.", 401);
        } else if (Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {
            $user = Auth::user();
            $isStaff = $user->hasRole(['Administrator', 'Seller', 'Sales Agent']);


            if ($isStaff) {
                $this->clearLoginAttempts($request);

                return response()->json(
                    [
                        'user' => $this->resolveRelations($user)
                    ]
                );
            } else {
                Auth::logout();
                return Main::response(false, 'No clients admited', ['errors' => ['No clients admited']], 401);
            }
        } else {
            //dd('qwe');
            $this->incrementLoginAttempts($request);

            return response(null, 401);
        }
    }

    /**
     * postRegister
     *
     * @param Request $request
     * @return void
     */
    public function postRegister(Request $request)
    {

        $validator = Validator::make(
            $input = $request->all(),
            [
                'name' => 'required|max:255',
                'email' => 'required|unique:users|email|max:255',
                'password' => 'required|min:6|max:60',
                'role' => 'string',
                'phone' => 'string',
                'image' => 'image|min:1|max:10240',
                'contractor' => 'string'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->phone = @$input['phone'];
        $user->contractor = @$input['contractor'];
        $user->password = bcrypt($input['password']);
        $user->status = 1;

        $location = public_path('profiles');

        $user->save();

        if (isset($input['image']) && $request->hasFile('image')) {
            $name = $user->id . '-image-profile.' . $input['image']->getClientOriginalExtension();
            $input['image']->move($location, $name);
            $user->image = $name;
            $user->save();
        }

        if (isset($input['role']) && ($role = Role::where('name', '=', $input['role'])->first())) {

            $user->attachRole($role);
        } else {

            $role = Role::where('name', '=', 'Administrator')->first();

            if ($role) {

                $user->attachRole($role);
            }
        }

        $user->roles = $user->roles()->get();

        return response()->json(['user' => $user]);
    }

    public function show($id)
    {

        if ($user = User::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($user));
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    //function to active an unactive users

    /**
     * toogleStatus
     *
     * @param mixed $id
     * @return void
     */
    public function toogleStatus($id)
    {
        if ($user = User::find($id)) {

            $user->status = !$user->status;
            $user->save();
            // print_r($user);
            return Main::response(true, 'OK', $this->resolveRelations($user));
        } else {
            return Main::response(false, 'Not Found', null, 404);
            //print_r($user);
        }
    }

    /**
     * update
     *
     * @param mixed $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {

        if ($user = User::find($id)) {

            try {

                $input = $request->all();

                $validator = Validator::make(
                    $input,
                    [
                        'name' => 'string',
                        'password' => 'string|min:6',
                        'phone' => 'string',
                        // 'image'          => 'image|min:1|max:10240',
                        'contractor' => 'string'
                    ]
                );

                if ($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                }

                if (isset($input['name'])) {

                    $user->name = $input['name'];
                    $client = Client::where('id_users', '=', $user->id)->first();
                    if ($client) {
                        $client->name = $input['name'];
                        $client->save();
                    }
                }

                if (isset($input['password'])) {

                    $user->password = bcrypt($input['password']);
                }

                if (isset($input['phone'])) {

                    $user->phone = $input['phone'];
                }
                $location = public_path('profiles');

                if (isset($input['image']) && $request->hasFile('image')) {
                    unlink($location . $user->image);
                    $name = $user->id . '-image-profile.' . $input['image']->getClientOriginalExtension();
                    $input['image']->move($location, $name);
                    $user->image = $name;
                }

                if (isset($input['contractor'])) {

                    $user->contractor = $input['contractor'];
                }

                if (isset($input['email'])) {

                    if ($user->email != $input['email']) {
                        $user->email = $input['email'];
                    }

                    $client = Client::where('id_users', '=', $user->id)->first();
                    if ($client) {
                        $client->email = $input['email'];
                        $client->save();
                    }
                }

                $user->save();

                return Main::response(true, 'OK', $this->resolveRelations($user));
            } catch (\Exception $e) {

                return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
            }
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * resolveRelations
     *
     * @param mixed $user
     * @return void
     */
    private function resolveRelations($user)
    {

        $roles = DB::table('role_user')
            ->where('user_id', $user->id)
            ->get();

        foreach ($roles as &$rol) {

            $rol = Role::find($rol->role_id);
        }

        $client = DB::table('clients')
            ->where('id_users', $user->id)
            ->first();
        $user->client = $client;

        $user->roles = $roles;

        return $user;
    }

    /**
     * postEmail
     *
     * @param Request $request
     * @return void
     */
    public function postEmail(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'email' => 'required|email'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $response = Password::sendResetLink(
            $request->only('email'),
            function (Message $message) {
                $message->subject('Reestablecer contraseña');
            }
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return Main::response(true, 'OK', null);
            case Password::INVALID_USER:
                return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * postReset
     *
     * @param Request $request
     * @return void
     */
    public function postReset(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'token' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $_user = null;

        $response = Password::reset(
            $request->only('email', 'password', 'token', 'password_confirmation'),
            function ($user, $password) use (&$_user) {
                $_user = $user;
                $user->password = bcrypt($password);
                $user->save();
                Auth::login($user);
            }
        );

        switch ($response) {
            case Password::PASSWORD_RESET:
                return Main::response(true, 'OK', $this->resolveRelations($_user));
            default:
                return Main::response(false, 'Internal Server Error', trans($response), 500);
        }
    }

    /**
     * template
     *
     * @param mixed $id
     * @return void
     */
    public function template(Request $request, $id)
    {
        $nId = isset($id) ? $id : 15;
        $url = "https://panelaguagente.xyz/front/one_click_registration.html?seller=$nId";
        return Redirect::to($url);
        //$input = $request->all();
        //$url = env('URL_API') . '/auth/facebook' . (isset($input['id']) ? "?seller={$input['id']}" : '');
        //$url = env('URL_API') . '/auth/facebook';
        //$request->session()->put('seller', $id);
        /*
        return '<html>
            <head>
            	<title>Aguagente</title>
            	<meta property="og:type"          content="website" />
            	<meta property="og:title"         content="Regístrate en Aguagente" />
            	<meta property="og:description"   content="Entra y registráte para conocer el mundo de Aguagente." />
            	<meta property="og:image"         content="https://aguagente.app/registrarme.png" />
            </head>
            <body>
                <script>
                    window.location = "'.$url.'";
                </script>
            </body>
        </html>';
        */
    }

    /**
     * getRegisterFacebook
     *
     * @param Request $request
     * @return void
     */
    public function getRegisterFacebook(Request $request)
    {

        $input = $request->all();

        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
            'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
            'default_graph_version' => 'v2.8'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email'];

        return Main::response(
            true,
            'Found',
            null,
            302,
            [
                //'Location' => $helper->getLoginUrl(env('URL_API') . '/auth/facebook-register'.(isset($input['seller']) ? "?seller={$input['seller']}" : ''), $permissions)
                'Location' => $helper->getLoginUrl(env('URL_API') . '/auth/facebook-register', $permissions)
            ]
        );
    }

    /**
     * registerFacebook
     *
     * @param Request $request
     * @return void
     */
    public function registerFacebook(Request $request)
    {

        $inputs = $request->all();
        $name = null;
        $email = null;
        try {

            if (!$inputs['social_login'] == 'true') {
                $fb = new Facebook([
                    'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
                    'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
                    'default_graph_version' => 'v2.10'
                ]);

                $helper = $fb->getRedirectLoginHelper();
                $fb->setDefaultAccessToken($helper->getAccessToken());

                $response = $fb->get('/me?fields=first_name,last_name,email,picture.width(400).height(400)');
                $userNode = $response->getGraphUser();
                $name = $userNode->getFirstName() . " " . $userNode->getLastName();
                $email = $userNode->getEmail()->first();
            } else {

                if (isset($inputs['decode'])) {
                    $data = json_decode(base64_decode($inputs['params']));
                    $name = $data->name;
                    $email = $data->email;
                } else {
                    $name = $inputs['name'];
                    $email = $inputs['email'];
                }
            }

            if (User::where('email', '=', $email)->first()) {
                if (!$inputs['social_login'] == 'true') {
                    return Main::response(true, 'Found', null, 302, ['Location' => env('URL_MICUENTA') . '/#/login?error=Usuario ha sido registrado']);
                } else {
                    return Main::response(false, 'Found', null, 302);
                }
            }


            if ($inputs['manual_register'] == 'false') {
                //$password = str_random(10);
                $password = rand(100000, 999999);

                DB::beginTransaction();

                $user = new User();
                $user->name = $name;
                $user->email = $email;
                $user->password = bcrypt($password);
                $user->save();
                $user->attachRole(
                    Role::where('name', '=', 'Client')->first()
                );
                if (!$inputs['social_login'] == 'true') {
                    $seller = $request->session()->get('seller');
                } else {
                    $seller = $inputs['seller'];
                }
                if ($seller) {
                    $referrer = Client::where('id_clients', '=', $seller)->first();
                    $referrerGroup = Group::find($referrer->id_groups);
                    $signIntoGroup = Group::find($referrerGroup->sign_into);
                } else {
                    $referrer = Client::where('id_clients', '=', 15)->first();
                    $referrerGroup = Group::find($referrer->id_groups);
                    $signIntoGroup = Group::find($referrerGroup->sign_into);
                }

                $client = new Client();
                $client->id_users = $user->id;
                $client->id_groups = $signIntoGroup->id_groups;
                $client->name = $user->name;
                $client->email = $user->email;
                $client->signed_in_id = $signIntoGroup->id_groups;
                $client->signed_in = $signIntoGroup->name;
                $client->deposit = $signIntoGroup->deposit;
                $client->installation_fee = $signIntoGroup->installation_fee;
                $client->monthly_fee = $signIntoGroup->monthly_fee;
                $client->referred_by = @$referrer->id_clients;
                $client->status = 'invalid';
                $client->save();

                Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                Conekta::setLocale('es');

                $customer = Conekta_Customer::create(
                    array(
                        'name' => $client->name,
                        'email' => $client->email
                    )
                );

                $client->conekta_token = $customer->id;
                $client->save();

                $contract = new Contract();
                $contract->id_clients = $client->id_clients;
                $contract->save();

                $this->notifyPassword([
                    'client' => $client,
                    'password' => $password
                ]);

                DB::commit();

                $request->session()->forget('seller');
                if (!$inputs['social_login'] == 'true') {
                    return response()->view('facebook.success', ['client' => $client]);
                } else {
                    if (isset($inputs['decode'])) {
                        return response()->view('facebook.success', ['client' => $client]);
                    } else {
                        return Main::response(true, ['client' => $client], null, 200);
                    }

                }
            } else {
                $this->sentConfirmationEmail([
                    'name' => $name,
                    'email' => $email,
                    'seller' => $inputs['seller']
                ]);
                $client = new Client();
                $client->name = $name;
                $client->email = $email;
                return Main::response(true, ['client' => $client], null, 200);
            }

            //return Main::response(true, 'Found', null, 302, ['Location'=>env('URL_MICUENTA').'/#/login?success=Usuario registrado correctamente']);

        } catch (\Exception $e) {

            DB::rollback();

            return Main::response(false, 'Internal Server Error', null, 500);
        }
    }

    /**
     * loginFacebook
     *
     * @param Request $request
     * @return void
     */
    public function loginFacebook(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'token' => 'required|string'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
            'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
            'default_graph_version' => 'v2.10'
        ]);

        try {

            $response = $fb->get('/me?fields=email', $input['token']);
            $userNode = $response->getGraphUser();

            $user = User::where('email', '=', $userNode->getEmail())->first();

            Auth::login($user);

            return response()->json(
                [
                    'user' => $this->resolveRelations($user)
                ]
            );
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    /**
     * registerDevice
     *
     * @param Request $request
     * @return void
     */
    public function registerDevice(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'registration_id' => 'required|string',
                'platform' => 'required|in:ios,android'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $user = Auth::user();

        $device = Device::where('id_users', '=', $user->id)
            ->where('registration_id', '=', $input['registration_id'])
            ->first();

        if (!$device) {

            $device = new Device;
            $device->registration_id = $input['registration_id'];
            $device->platform = $input['platform'];
            $device->id_users = $user->id;
            $device->save();
        }

        return Main::response(true, 'OK', $device, 200);
    }

    public function getReferidos(Request $request, $id_client)
    {
        $query = Client::where('referred_by', $id_client)->get();
        $total = 0;

        foreach ($query as $client) {
            $total++;
        }

        return Main::response(true, 'OK', ['referidos' => $total]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function leadSignup(Request $request)
    {
        $hubspot = new HubspotVendor();
        $inputs = $request->all();
        $name = null;
        $email = null;
        $phone = null;
        try {
            $name = $inputs['name'];
            $email = $inputs['email'];
            $phone = $inputs['phone'];

            $from = $inputs['from'] ? $inputs['from'] : '';
            $leadId = $inputs['leadId'] ? $inputs['leadId'] : '';

            $leadInformation = "{leadId:{$leadId}, from:{$from}}";

            if (User::withoutGlobalScopes()->where('email', '=', $email)->first()) {
                return Main::response(false, 'Found', null, 302);
            }

            $password = rand(100000, 999999);

            //DB::beginTransaction();

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->phone = $phone;
            $user->password = bcrypt($password);
            /*$user->save();
            $user->attachRole(
                Role::withoutGlobalScopes()->where('name', '=', 'Client')->first()
            );*/


            if (isset($inputs['seller']) && !empty($inputs['seller'])) {
                $seller = $inputs['seller'];
                $referrer = Client::withoutGlobalScopes()->where('id_clients', '=', $seller)->first();
                $referrerGroup = Group::withoutGlobalScopes()->find($referrer->id_groups);
            } else {
                $referrer = Client::withoutGlobalScopes()->where('id_clients', '=', 15)->first();
                $referrerGroup = Group::withoutGlobalScopes()->find($referrer->id_groups);
            }

            $client = new Client();
            $client->name = $user->name;
            $client->email = $user->email;
            $client->phone = $user->phone;
            //$client->id_users = $user->id;
            $client->id_groups = $referrerGroup->sign_into;
            $client->signed_in_id = $referrerGroup->id_groups;
            $client->signed_in = $referrerGroup->name;
            $client->deposit = $referrerGroup->deposit;
            $client->installation_fee = $referrerGroup->installation_fee;
            //$client->monthly_fee        = $referrerGroup->monthly_fee;
            $client->monthly_fee = $referrerGroup->monthly_fee;
            $client->referred_by = $referrer->id_clients;
            $client->status = 'prospect';
            $client->signed_in_group = $referrerGroup;
            $client->lead = $leadInformation;

            //Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            //Conekta::setLocale('es');

            /*$customer = Conekta_Customer::create(
                array(
                    'name' => $client->name,
                    'email' => $client->email
                )
            );

            $client->conekta_token = $customer->id;
            $client->save();

            $contract = new Contract();
            $contract->id_clients = $client->id_clients;
            $contract->save();

            DB::commit();*/

            // --------------------------
            // Sending contact to hubspot
            // ---------------------------

            $hubspotContactData = [
                [
                    "property" => "email",
                    "value" => $user->email
                ],
                [
                    "property" => "firstname",
                    "value" => $user->name
                ],
                [
                    "property" => "lastname",
                    "value" => ""
                ],
                [
                    "property" => "phone",
                    "value" => $user->phone
                ],
                [
                    "property" => "lifecyclestage",
                    "value" => "lead"
                ],
                [
                    "property" => "hs_lead_status",
                    "value" => "NEW"
                ],
                [
                    "property" => "origin_of_lead",
                    "value" => "{$from} {$leadId}"
                ]
            ];

            $resultFromHubspot = $hubspot->createContact($hubspotContactData);

            /* $hubspotHapikey = !empty(env('HUBSPOT_HAPIKEY')) ? env('HUBSPOT_HAPIKEY') : '';
            if (empty($hubspotHapikey)) {
                $resultFromHubspot = 'HAPIKEY no puede ser vacia';
            } else {
                $post_json = json_encode($hubspotContactData);
                $clientHttp = new \GuzzleHttp\Client(['base_uri' => 'https://api.hubapi.com']);
                $response = $clientHttp->request('POST', '/contacts/v1/contact?hapikey=' . $hubspotHapikey,
                    [
                        'headers' => ['Content-Type' => 'application/json'],
                        'body' => $post_json
                    ]);
                $response = $response->getBody()->getContents();
                $resultFromHubspot = json_decode($response, true);
            } */

            // ------------------------------------------------------------------
            // If there is an error from hubspot, send an email to staff members
            // -------------------------------------------------------------------
            $dataFromHubspot = [];
            if (!$resultFromHubspot['success']) {
                $this->nofifyStaffHubspotError($client, $resultFromHubspot['data']);
            } else {
                $dataFromHubspot['client'] = $resultFromHubspot['data'];
                $expirationDate = date('Y-m-d', strtotime(date('Y-m-d') . ' + 3 days'));
                $dateTime = new \DateTime($expirationDate);
                $engagement = [
                    'active' => true,
                    'type' => 'TASK',
                    'timestamp' => $dateTime->getTimestamp() * 1000
                ];

                $associations = [
                    'contactIds' => [$resultFromHubspot['data']->vid]
                ];

                $metadata = [
                    'body' => 'Nuevo contacto creado, pendiente para contactar',
                    'subject' => 'Nuevo contacto creado',
                    'status' => 'IN_PROGRESS',
                    'forObjectType' => 'CONTACT'
                ];

                $dataToTask['engagement'] = $engagement;
                $dataToTask['associations'] = $associations;
                $dataToTask['metadata'] = $metadata;

                $dataFromEngagement = $hubspot->createEngagement($dataToTask);

                $dataForDeal = [
                    "associations" => [
                        "associatedVids" => [
                            $resultFromHubspot['data']->vid
                        ]
                    ],
                    "properties" => [
                        [
                            "value" => $referrerGroup->name,
                            "name" => "dealname"
                        ],
                        [
                            "value" => "appointmentscheduled",
                            "name" => "dealstage"
                        ],
                        [
                            "value" => "default",
                            "name" => "pipeline"
                        ],
                        [
                            "value" => $dateTime->getTimestamp() * 1000,
                            "name" => "closedate"
                        ],
                        [
                            "value" => "newbusiness",
                            "name" => "dealtype"
                        ],
                    ]
                ];

                $dataFromDeal = $hubspot->createDeal($dataForDeal);

                if ($dataFromEngagement['success']) {
                    $dataFromHubspot['engagement'] = $dataFromEngagement['data'];
                } else {
                    $this->nofifyStaffHubspotError($client, $dataFromEngagement['data']);
                }

                if ($dataFromDeal['success']) {
                    $dataFromHubspot['deal'] = $dataFromDeal['data'];
                } else {
                    $this->nofifyStaffHubspotError($client, $dataFromDeal['data']);
                }
            }

            return Main::response(true, ['client' => $client, 'hubspot' => $dataFromHubspot], null, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Main::response(false, 'Internal Server Error', null, 500);
        }
    }

    public function webhookMessenger(Request $request)
    {
        $inputs = $request->all();
        $challenge = $inputs['hub_challenge'];
        $verify_token = $inputs['hub_verify_token'];

        if ($verify_token === env('APP_ID')) {
            echo $challenge;
        }
        $input = json_decode(file_get_contents('php://input'), true);
        error_log(print_r($input, true));

        //echo json_encode(['asd' => 'asd']);
        $fp = fopen('lidn.txt', 'w');
        fwrite($fp, json_encode($input));
        fclose($fp);

        return response('', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function facebookWebhook(Request $request)
    {
        $inputs = $request->all();
        $challenge = $inputs['hub_challenge'];
        $verify_token = $inputs['hub_verify_token'];

        if ($verify_token === env('APP_ID')) {
            echo $challenge;
        }
        $input = json_decode(file_get_contents('php://input'), true);

        /*$input = json_decode('{"object":"page","entry":[{"id":"692315380887249","time":1587583758,
        "changes":[{"value":{"ad_id":"23845280617770601","form_id":"652630645300915","leadgen_id":"705061536735549",
        "created_time":1587583756,"page_id":"692315380887249","adgroup_id":"23845280617770601"},
        "field":"leadgen"}]}]}', true);
        $input = json_decode('{"object":"page","entry":[{"id":"692315380887249","time":1587668026,
        "changes":[{"value":{"form_id":"221752855910098","leadgen_id":"2580493102221862","created_time":1587668025,
        "page_id":"692315380887249"},"field":"leadgen"}]}]}', true);*/
        /*$input = json_decode('{"object":"page","entry":[{"id":"692315380887249","time":1589780333343,
        "messaging":[{"sender":{"id":"2607440626027529"},"recipient":{"id":"692315380887249"},
        "timestamp":1589780333068,"message":{"mid":"m_LOqp7O0wjP0ldt6ipigSd1aV-6RBFpqtfR4Hb-5rs-1jF9lYg2CfxShJM1WmOm0AioFz9vsXjHBPeyK9jK7boA",
        "text":"M\u00e9rida","nlp":{"entities":{"email":[{"suggested":true,"confidence":0.93586999177933,"value":"futpiz_56789@hotmail.com","type":"value"}]},"detected_locales":[{"locale":"es_XX","confidence":0.9906}]}}}]}]}', true);*/
        if ($input) {
            $facebook = new FacebookVendor();
            foreach ($input['entry'] as $entry) {
                if (isset($entry['messaging'])) {
                    foreach ($entry['messaging'] as $message) {
                        $entities = $message['message']['nlp']['entities'];
                        $entitiesExist = ['location', 'email', 'phone_number'];
                        foreach ($entitiesExist as $item) {
                            if (isset($entities[$item])) {
                                $recipientId = $message['sender']['id'];
                                $userInfo = $facebook->getUser($recipientId);

                                // If exist the send id, we need to update the user info in hubspot
                                $hubspotUser = HubspotUser::where('facebook_user_id', '=', $recipientId)->first();

                                switch ($item) {
                                    case 'location':
                                        if (!empty($hubspotUser)) {
                                            $hubspotUser->city = $entities[$item][0]['value'];
                                            $hubspotUser->save();
                                        } else {
                                            $hubspotUser = new HubspotUser();
                                            $hubspotUser->facebook_user_id = $recipientId;
                                            $hubspotUser->city = $entities[$item][0]['value'];
                                            $hubspotUser->is_active = true;
                                            $hubspotUser->save();
                                        }

                                        break;
                                    case 'email':
                                        $hubspot = new HubspotVendor();
                                        $userFromHubspot = $hubspot->getContact($entities[$item][0]['value']);
                                        // main case, no existe user hubspot con el email dado, actualizarlo en hubspot y base de datos
                                        if (!$userFromHubspot['success'] && !empty($hubspotUser)) {
                                            $hubspotUser->email = $entities[$item][0]['value'];
                                            $hubspotUser->save();

                                            $this->sendHubspotData($entities[$item][0]['value'], $userInfo['first_name'], '', 'LEAD COMPLETED', $userInfo['last_name'], '', $hubspotUser->vid);
                                            // ya existe un usuario en hubspot con el mismo email
                                        } else if ($userFromHubspot['success'] && !empty($hubspotUser)) {
                                            $this->sendHubspotData('', $userInfo['first_name'], '', 'An User already has the email: ' . $entities[$item][0]['value'], $userInfo['last_name'], '', $hubspotUser->vid);
                                        }

                                        break;
                                    case 'phone_number':
                                        if (!empty($hubspotUser)) {
                                            // Add city from database
                                            $res = $this->sendHubspotData('', $userInfo['first_name'], $entities[$item][0]['value'], 'LEAD INCOMPLETE', $userInfo['last_name'], $hubspotUser->city);
                                            if (!empty($res)) {
                                                $hubspotUser->vid = $res->vid;
                                                $hubspotUser->phone_number = $entities[$item][0]['value'];
                                                $hubspotUser->save();
                                            }
                                        }

                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }

                if (isset($entry['changes'])) {
                    foreach ($entry['changes'] as $change) {
                        $leadgenId = $change['value']['leadgen_id'];
                        $formData = $facebook->getLeadInformationById($leadgenId);
                        $email = '';
                        $phone = '';
                        $name = '';
                        $city = '';
                        foreach ($formData['field_data'] as $fieldData) {
                            switch ($fieldData['name']) {
                                case 'correo_electrónico_':
                                case '¿cuál_es_tu_dirección_de_correo_electrónico?':
                                case 'email':
                                    $email = isset($fieldData['values'][0]) ? $fieldData['values'][0] : '';
                                    break;
                                case 'número_de_teléfono':
                                case "¿cuál_es_tu_número_de_teléfono?":
                                case 'phone':
                                    $phone = isset($fieldData['values'][0]) ? $fieldData['values'][0] : '';
                                    break;
                                case 'nombre_completo':
                                case 'name':
                                case '¿cuál_es_tu_nombre?':
                                    $name = isset($fieldData['values'][0]) ? $fieldData['values'][0] : '';
                                    break;
                                case 'ciudad':
                                case '¿en_qué_ciudad_vives?':
                                case 'city':
                                    $city = isset($fieldData['values'][0]) ? $fieldData['values'][0] : '';
                                    break;
                                default :
                                    break;
                            }
                        }

                        $hubspotUser = HubspotUser::where('email', '=', $email);

                        if (empty($hubspotUser)) {
                            // **** Send data only to hubspot *****
                            $res = $this->sendHubspotData($email, $name, $phone, $leadgenId, $city);
                            $hubspotUser = new HubspotUser();
                            $hubspotUser->email = $email;
                            $hubspotUser->vid = $res->vid;
                            $hubspotUser->save();
                        }
                    }
                }
            }
        }

        return response('', 200)
            ->header('Content-Type', 'text/plain');
    }

    private function sendHubspotData($email, $name, $phone, $leadId, $lastname = '', $city = '', $id = '')
    {
        $hubspot = new HubspotVendor();
        $hubspotContactData = [
            [
                "property" => "lifecyclestage",
                "value" => "lead"
            ],
            [
                "property" => "hs_lead_status",
                "value" => "NEW"
            ],
            [
                "property" => "origin_of_lead",
                "value" => "Campaña Mayo 23845538265900601 Facebook {$leadId}"
            ],
        ];

        if (!empty($email)) {
            $hubspotContactData = array_merge($hubspotContactData, [[
                "property" => "email",
                "value" => $email
            ]]);
        }
        if (!empty($city)) {
            $hubspotContactData = array_merge($hubspotContactData, [[
                "property" => "city",
                "value" => $city
            ]]);
        }
        if (!empty($name)) {
            $hubspotContactData = array_merge($hubspotContactData, [[
                "property" => "firstname",
                "value" => $name
            ]]);
        }
        if (!empty($lastname)) {
            $hubspotContactData = array_merge($hubspotContactData, [[
                "property" => "lastname",
                "value" => $lastname
            ]]);
        }

        if (!empty($phone)) {
            $hubspotContactData = array_merge($hubspotContactData, [[
                "property" => "phone",
                "value" => $phone
            ]]);
        }

        if (!empty($id)) {
            $resultFromHubspot = $hubspot->updateContact($id, $hubspotContactData);
        } else {
            $resultFromHubspot = $hubspot->createContact($hubspotContactData);

            // ------------------------------------------------------------------
            // If there is an error from hubspot, send an email to staff members
            // -------------------------------------------------------------------
            $dataFromHubspot = [];
            if (!$resultFromHubspot['success']) {
                $this->nofifyStaffHubspotError(['email' => $email, 'phone' => $phone, 'name' => $name], $resultFromHubspot['data']);
            } else {
                $dataFromHubspot['client'] = $resultFromHubspot['data'];
                $expirationDate = date('Y-m-d', strtotime(date('Y-m-d') . ' + 3 days'));
                $dateTime = new \DateTime($expirationDate);
                $engagement = [
                    'active' => true,
                    'type' => 'TASK',
                    'timestamp' => $dateTime->getTimestamp() * 1000
                ];

                $associations = [
                    'contactIds' => [$resultFromHubspot['data']->vid]
                ];

                $metadata = [
                    'body' => 'Nuevo contacto creado, pendiente para contactar',
                    'subject' => 'Nuevo contacto creado',
                    'status' => 'IN_PROGRESS',
                    'forObjectType' => 'CONTACT'
                ];

                $dataToTask['engagement'] = $engagement;
                $dataToTask['associations'] = $associations;
                $dataToTask['metadata'] = $metadata;

                $dataFromEngagement = $hubspot->createEngagement($dataToTask);

                $dataForDeal = [
                    "associations" => [
                        "associatedVids" => [
                            $resultFromHubspot['data']->vid
                        ]
                    ],
                    "properties" => [
                        [
                            "value" => "$279 group",
                            "name" => "dealname"
                        ],
                        [
                            "value" => "appointmentscheduled",
                            "name" => "dealstage"
                        ],
                        [
                            "value" => "default",
                            "name" => "pipeline"
                        ],
                        [
                            "value" => $dateTime->getTimestamp() * 1000,
                            "name" => "closedate"
                        ],
                        [
                            "value" => "newbusiness",
                            "name" => "dealtype"
                        ],
                    ]
                ];

                $dataFromDeal = $hubspot->createDeal($dataForDeal);

                if ($dataFromEngagement['success']) {
                    $dataFromHubspot['engagement'] = $dataFromEngagement['data'];
                } else {
                    $this->nofifyStaffHubspotError(['email' => $email, 'phone' => $phone, 'name' => $name], $dataFromEngagement['data']);
                }

                if ($dataFromDeal['success']) {
                    $dataFromHubspot['deal'] = $dataFromDeal['data'];
                } else {
                    $this->nofifyStaffHubspotError(['email' => $email, 'phone' => $phone, 'name' => $name], $dataFromDeal['data']);
                }
            }
        }

        return isset($resultFromHubspot['data']) ? $resultFromHubspot['data'] : [];
    }
}
