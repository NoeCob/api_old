<?php

namespace App\Http\Controllers;

use App\Role;
use App\Http\Controllers\Main;
use DB;
use Request;
use Validator;

class RolesController extends Main {

    /**
     * index
     * Un Rol es el nivel de usuario en el sistema.
     * Devuelve todas los roles.
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {
        
        try {

            return Main::response(true, 'OK', DB::table('roles')->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
        
    }

    /**
     * create
     * Crea un una rol.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Role
     * 
     * @return response     Created|Bad Request|Internal Server Error
     */
    public function create() {

        try {

            $validator = Validator::make(
                $input = Request::all(),
                [
                    'name'    => 'required|unique:roles|max:255',
                    'display_name' => 'max:255',
                    'description' => 'max:255'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', [ 'errors' => $validator->errors() ], 400);

            }

            $role = new Role();
            $role->name         = $input['name'];
            $role->display_name = isset($input['display_name']) ? isset($input['display_name']) : null;
            $role->description  = isset($input['description']) ? isset($input['description']) : null;
            $role->save();

            return Main::response(true, 'Created', $role, 201);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);
            
        }

    }

    /**
     * show
     * Muestra un rol (\App\Role) por medio del ID
     *
     * @\App\Role
     * 
     * @param  int      $id ID del rol
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($role = Role::find($id)) {

            return Main::response(true, 'OK', $role);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * update
     * Guarda las modificaciones realizadas a un rol (\App\Role) por medio del ID
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Role
     * 
     * @param  int      $id ID del rol
     * @return response     NULL|Bad Request|Not Found|Internal Server Error
     */
    public function update($id) {
            
        try {

            if($role = Role::find($id)) {

                $input = Request::all();

                $validator = Validator::make(
                    $input = Request::all(),
                    [
                        'name'    => 'required|unique:roles|max:255',
                        'display_name' => 'max:255',
                        'description' => 'max:255'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', [ 'errors' => $validator->errors() ], 400);

                }

                $role->name         = $input['name'];
                $role->display_name = isset($input['display_name']) ? $input['display_name'] : null;
                $role->description  = isset($input['description']) ? $input['description'] : null;
                $role->save();

                return Main::response(true, null, $role);

            } else {

                return Main::response(false, 'Not Found', null, 404);

            }

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);
            
        }

    }

    /**
     * destroy
     * Borra de la base de datos un rol(\App\Role) por medio de un ID
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Role
     * 
     * @param  int      $id ID del rol
     * @return response     Ok|Not Found
     */
    public function destroy($id) {

        if($role = Role::find($id)) {

            $role->delete();

            return Main::response(true, 'OK', null);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}