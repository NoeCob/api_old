<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CouponTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     */
    public function testCouponIndex()
    {
        $this->get('/coupon')
            ->assertResponseStatus(200);
    }

    public function testCouponGenerate()
    {
        $params = [
            'email' => 'test@gmail.com',
            'level' => '1'
        ];
        $this->post('/coupon', $params)
            ->assertResponseStatus(201);

    }

    public function testManuallyStatusClient()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $params = [
            'level' => 'VIAJERO'
        ];
        $this->post('/client/' . $client->id_clients . '/level', $params)
            ->assertResponseStatus(200);

    }

}
