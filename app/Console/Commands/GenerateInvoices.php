<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Client;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:generate {year?} {month?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the invoices at the end of each month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Obtiene todos los Sales agents y calcula las comisiones del mes en curso.
     *
     * @return void
     */
    public function handle()
    {
        $today = date('Y-m-d');
        $this->info('Fecha: '.$today);
        
        $url = 'generate-invoices';
        if ($this->argument('year')) {
            $url .= '?y='.$this->argument('year').'&m='.$this->argument('month');
        }

        $request = Request::create($url, 'GET');
        $this->info(app()->make(\Illuminate\Contracts\Http\Kernel::class)->handle($request));
    }
}