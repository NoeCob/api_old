<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id_logs');
            $table->longText('before_data')->nullable(true);
            $table->longText('after_data');
            $table->string('route', 255)->nullable(true);
            $table->string('model_name', 100);
            $table->string('message', 255)->nullable(true);
            $table->string('request_type', 25)->nullable(true);
            $table->char('table_id', 32);
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
