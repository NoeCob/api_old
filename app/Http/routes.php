<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('dashboard', 'Main@dashboard');
Route::get('clients', 'Main@clients');
Route::get('free_water','Main@freeWater');
Route::get('subscriptions','Main@subscriptions');
Route::get('shares','Main@shares');
Route::get('tickets_completion','Main@ticketsCompletion');
Route::get('appUsage','Main@appUsage');

// Route::get('past_subscriptions','Main@pastSubscriptions');
// Route::get('shares_by_type','Main@sharesByType');
Route::post('dashboard_charts', 'DashboardChartController@storeSharedClick');

Route::get('users', 'UsersController@index');
Route::get('users/{id}', 'UsersController@show');
Route::get('users/show/{id}', 'UsersController@show');
Route::get('users/toogleStatus/{id}', 'UsersController@toogleStatus');
Route::put('users/{id}', 'UsersController@update');
Route::post('users/registerDevice', 'UsersController@registerDevice');
Route::get('users/referidos/{id}', 'UsersController@getReferidos');

Route::post('auth/login', 'UsersController@postLogin');
Route::post('auth/login/erp', 'UsersController@postLoginErp');
Route::get('auth/logout', 'UsersController@getLogout');
Route::post('auth/register', 'UsersController@postRegister');
Route::get('auth/token', 'UsersController@getToken');
Route::post('auth/email', 'UsersController@postEmail');
Route::post('auth/reset', 'UsersController@postReset');
Route::post('auth/unregistered', 'ClientsController@unregistered');
Route::get('auth/facebook', 'UsersController@getRegisterFacebook');
Route::get('auth/facebook-register', 'UsersController@registerFacebook');
Route::post('auth/lead-signup', 'UsersController@leadSignup');
Route::post('auth/login-facebook', 'UsersController@loginFacebook');
Route::get('facebook/{id?}', 'UsersController@template');
//Route::get('user/webhook', 'UsersController@testingWebhook');
Route::match(['get', 'post'], 'user/webhook', 'UsersController@facebookWebhook');

Route::post('check_app_version', 'Main@checkAppVersion');

Route::get('contracts', 'ContractsController@index');
Route::post('contracts', 'ContractsController@create');
Route::post('contracts/on', 'ContractsController@on');
Route::get('contracts/{id}', 'ContractsController@show');
Route::put('contracts/{id}', 'ContractsController@update');
    Route::post('contracts/{id}', 'ContractsController@update');
Route::post('contracts/on', 'ContractsController@on');

Route::get('categories', 'CategoriesController@index');
Route::post('categories', 'CategoriesController@create');
Route::get('categories/{id}', 'CategoriesController@show');
Route::put('categories/{id}', 'CategoriesController@update');
Route::delete('categories/{id}', 'CategoriesController@destroy');

Route::get('elements', 'ElementsController@index');
Route::post('elements', 'ElementsController@create');
Route::get('elements/{id}', 'ElementsController@show');
Route::put('elements/{id}', 'ElementsController@update');
Route::delete('elements/{id}', 'ElementsController@destroy');

Route::get('clients', 'ClientsController@index');
Route::get('clients/get_clients', 'ClientsController@getClients');
Route::get('clients/{id}', 'ClientsController@show');
Route::put('clients/{id}', 'ClientsController@update');
Route::delete('clients/{id}', 'ClientsController@delete');
Route::post('clients/{id}/updatePhoto', 'ClientsController@updatePhoto');
Route::post('clients/{id}/setStatus', 'ClientsController@setStatus');
Route::post('clients/{id}/setGroup', 'ClientsController@setGroup');
Route::post('clients/{id}/subscribeToPlan', 'ClientsController@subscribeToPlan');
Route::post('clients/{id}/subscribeToPlanOffline', 'ClientsController@subscribeToPlanOffline');
Route::post('clients/{id}/unsubscribeToPlan', 'ClientsController@unsubscribeToPlan');
Route::get('clients/{id}/hierarchicalTree', 'ClientsController@hierarchicalTree');
Route::post('clients/{id}/attachRole', 'ClientsController@attachRole');
Route::post('clients/sendPassword', 'ClientsController@sendPassword');
Route::post('clients/{id}/setLevel', 'ClientsController@setLevel');
Route::post('clients/{id}/regenerateConektaId', 'ClientsController@regenerateConektaUser');

Route::get('clients/{id}/cards', 'CardsController@index');
Route::post('clients/{id}/cards', 'CardsController@create');
Route::get('clients/{id}/cards/{id_cards}', 'CardsController@show');
Route::delete('clients/{id}/cards/{id_cards}', 'CardsController@delete');
Route::post('clients/{id}/cards/{id_cards}/setAsDefault', 'CardsController@setAsDefault');
Route::get('clients/{id}/missingPolls', 'PollsController@missingPolls');
Route::post('clients/{id}/switchInvoice', 'ClientsController@switchInvoice');
Route::post('clients/registerCommissions', 'ClientsController@registerCommissions');

Route::get('clients/auth/token', 'ClientsController@getToken');
Route::post('clients/auth/login', 'ClientsController@postLogin');

Route::get('roles', 'RolesController@index');
Route::post('roles', 'RolesController@create');
Route::get('roles/{id}', 'RolesController@show');
Route::put('roles/{id}', 'RolesController@update');
Route::delete('roles/{id}', 'RolesController@destroy');

Route::get('permissions', 'PermissionsController@index');
Route::post('permissions', 'PermissionsController@create');
Route::get('permissions/{id}', 'PermissionsController@show');
Route::put('permissions/{id}', 'PermissionsController@update');
Route::delete('permissions/{id}', 'PermissionsController@destroy');

Route::get('invalidations', 'InvalidationsController@index');
Route::get('invalidations/{id}', 'InvalidationsController@show');

Route::get('rejections', 'RejectionsController@index');
Route::get('rejections/{id}', 'RejectionsController@show');

Route::get('cancellations', 'CancellationsController@index');
Route::get('cancellations/{id}', 'CancellationsController@show');

Route::get('corrections', 'CorrectionsController@index');
Route::get('corrections/{id}', 'CorrectionsController@show');

Route::get('groups', 'GroupsController@index');
Route::post('groups', 'GroupsController@create');
Route::get('groups/{id}', 'GroupsController@show');
Route::put('groups/{id}', 'GroupsController@update');
Route::delete('groups/{id}', 'GroupsController@destroy');
Route::post('groups/{id}/associateClients', 'GroupsController@associateClients');

Route::get('polls', 'PollsController@index');
Route::post('polls', 'PollsController@create');
Route::get('polls/{id}', 'PollsController@show');
Route::put('polls/{id}', 'PollsController@update');
Route::delete('polls/{id}', 'PollsController@destroy');

Route::get('answers', 'AnswersController@index');
Route::post('polls/{id}/answer', 'AnswersController@answer');

Route::get('commissions', 'CommissionsController@index');
Route::post('commissions/{id}/registerAsPaid', 'CommissionsController@registerAsPaid');
Route::get('commissions/calculateCommissions', 'CommissionsController@calculateCommissions');
Route::get('commissions/getCommissions/{id}', 'CommissionsController@getClientCommissionUpToToday');
Route::get('commissions/getLoosingCommissions/{id}', 'CommissionsController@getClientLoosingCommission');
Route::get('commissions/getDeclined/{id}', 'CommissionsController@getDeclinedClientsPerCommission');

Route::get('error-codes', 'ErrorCodesController@index');
Route::post('error-codes', 'ErrorCodesController@create');
Route::get('error-codes/{id}', 'ErrorCodesController@show');
Route::put('error-codes/{id}', 'ErrorCodesController@update');
Route::delete('error-codes/{id}', 'ErrorCodesController@destroy');

Route::get('tickets', 'TicketsController@index');
Route::get('tickets/get_tickets', 'TicketsController@getTickets');
Route::get('tickets/get_map_tickets', 'TicketsController@getMapTickets');
Route::post('tickets', 'TicketsController@create');
Route::get('tickets/{id}', 'TicketsController@show');
Route::post('tickets/{id}/setStatus', 'TicketsController@setStatus');
Route::post('tickets/{id}/send-mail', 'TicketsController@sendMail');

Route::get('assignations', 'AssignationsController@index');
Route::get('assignations/{id}', 'AssignationsController@show');

Route::get('completions', 'CompletionsController@index');
Route::get('completions/{id}', 'CompletionsController@show');

Route::get('confirmations', 'ConfirmationsController@index');
Route::post('confirmations', 'ConfirmationsController@create');
Route::get('confirmations/{id}', 'ConfirmationsController@show');

Route::get('tickets-job', 'TicketsController@job');

Route::get('charges', 'ChargesController@index');
Route::get('charges/{id}', 'ChargesController@show');
Route::post('charges', 'ChargesController@create');
Route::get('charges/invoice', 'ChargesController@invoice');
Route::post('charges/{id}/refund', 'ChargesController@refund');

Route::get('fees', 'FeesController@index');
Route::post('canceledSubscriptions', 'ClientsController@canceledSubscriptions');

Route::post('manual', 'ClientsController@manual');
Route::get('posts', 'PostsController@index');
Route::post('posts', 'PostsController@create');
Route::get('posts/{id}', 'PostsController@show');
Route::put('posts/{id}', 'PostsController@update');
Route::delete('posts/{id}', 'PostsController@destroy');

Route::post('notifyUser', 'PushController@notifyUser');
Route::post('notifyGroup', 'PushController@notifyGroup');
Route::post('notifyRole', 'PushController@notifyRole');
Route::post('notifyUsersGroup', 'PushController@notifyUsersGroup');
Route::resource('notification_groups', 'NotificationGroupsController');

Route::get('debts', 'DebtsController@index');
Route::get('debts/{id}', 'DebtsController@show');
Route::post('debts/{id}/charge', 'DebtsController@charge');

Route::get('subscription-events', 'SubscriptionEventsController@index');
Route::get('finance-clients', 'FinanceClientsController@index');
Route::get('notifications', 'NotificationsController@index');
Route::post('notifications/report', 'NotificationsController@report');

Route::get('withdrawals', 'WithdrawalsController@index');
Route::post('withdrawals', 'WithdrawalsController@create');
Route::get('withdrawals/{id}', 'WithdrawalsController@show');

Route::get('coupon', 'CouponController@index');
Route::post('coupon', 'CouponController@generate');
Route::post('client/{id}/level', 'CouponController@manuallyStatusClient');

Route::get('invoices', 'InvoicesController@index');
Route::post('invoices/send_invoice', 'InvoicesController@sendInvoice');
Route::get('invoices/download_files/{id}', 'InvoicesController@downloadFiles');
Route::get('generate-invoices', 'ChargesController@invoice');

Route::post('bad-debts/moveBadDebt/{id}', 'BadDebtsController@moveBadDebt');
Route::post('bad-debts/restoreClient/{id}', 'BadDebtsController@restoreClient');

Route::get('logs', 'LogsController@index');

Route::get('test-clients', 'TestController@testClients');
Route::get('test', 'TestController@test');

Route::get('regenerate/clients/{fileName}', 'Regenerate\RegenerateController@createUsers');
Route::get('regenerate/charges/{fileName}', 'Regenerate\RegenerateController@getCharges');
Route::get('regenerate/debts', 'Regenerate\RegenerateController@getDebts');
Route::get('regenerate/nonexistent/charges', 'Regenerate\RegenerateController@getNonExistentCharges');
Route::get('regenerate/nonexistent/clients', 'Regenerate\RegenerateController@getNonExistentClients');
Route::get('regenerate/info/clients', 'Regenerate\RegenerateController@updateClientInfoWithLastCharge');
