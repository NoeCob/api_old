<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Request;

class LogsController extends Main
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $logs = DB::table('logs');
            $id_clients = Request::query()['id_clients'];
            $id_users = Request::query()['id_users'];
            $user = User::find($id_users);
            /** @var $user User */
            $logs->where('model_name', 'App\User')
                ->where('table_id', $user->id)
                ->orWhere('model_name', 'App\Client')
                ->where('table_id', $id_clients);
            $response = [];
            foreach ($logs->get() as $log) {
                $userFromLog = User::find($log->user_id);
                $log->user_name = $userFromLog->name;
                $response[] = $log;
            }
            return Main::response(true, null, $response);

        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
