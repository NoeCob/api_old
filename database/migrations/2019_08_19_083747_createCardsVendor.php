<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_vendors', function (Blueprint $table) {
            $table->increments('id_cards_vendors');
            $table->unsignedInteger('id_cards')->nullable(false);
            $table->unsignedInteger('id_vendor')->nullable(false);
            $table->string('card_token', 100)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards_vendors');
    }
}
