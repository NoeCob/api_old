<?php

namespace App\Http\Controllers;

use App\Cancellation;
use App\Client;
use App\ClientService;
use App\Coupon;
use App\Commission;
use App\CommissionService;
use App\Contract;
use App\ErrorCode;
use App\Extras;
use App\Fee;
use App\Debt;
use App\Group;
use App\Http\Controllers\Main;
use App\Invalidation;
use App\Level;
use App\MySQLClientRepository;
use App\Rejection;
use App\Role;
use App\Ticket;
use App\User;
use App\Charge;
use App\LevelService;
use App\Traits\ChargeTrait;
use Conekta;
use Conekta_Charge;
use Conekta_Customer;
use Conekta_Plan;
use Conekta_ProcessingError;
use Conekta_ParameterValidationError;
use DB;
use Illuminate\Support\Facades\Auth;
use Mail;
use Request;
use Validator;
use DateTime;
use DateInterval;
use Davibennun\LaravelPushNotification\PushNotification;
use App\Traits\NotifyTrait;
use App\Card;

/**
 * array_find
 * Busqueda recursiva dentro de un mismo array
 * @param  array    $elements Array de búsqueda.
 * @param  function $callback Función para comparar y realizar la busqueda.
 * @return mixed              Elemento encontrado|null
 */
function array_find(array $elements, $callback)
{
    foreach ($elements as $element)
        if (call_user_func($callback, $element) === true)
            return $element;
    return null;
}

class ClientsController extends Main
{

    use ChargeTrait;
    /**
     * __construct
     * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct()
    {

        parent::__construct(['index', 'getClients']);
    }

    /**
     * index
     * Devuelve todas los clientes después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK|Internal Server Error
     */
    public function index()
    {

        try {

            $params = Request::query();

            $resolve = 'true';
            if (isset($params['resolve'])) {
                $resolve = $params['resolve'];
                unset($params['resolve']);
            }

            //$clients = DB::table('clients');
            $clients = new Client;

            foreach ($params as $name => $value) {

                switch ($name) {

                    case 'name':
                    case 'email':
                        $clients = $clients->where($name, 'LIKE', "%$value%");
                        break;
                    case 'role':
                        $clients = $clients->whereIn(
                            'id_users',
                            array_map(
                                function ($element) {
                                    return $element->user_id;
                                },
                                DB::table('role_user')
                                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                                    ->where('roles.name', $value)
                                    ->select('role_user.user_id')
                                    ->get()
                            )
                        );
                        break;
                    default:
                        $clients = $clients->where($name, $value);
                        break;
                }
            }

            $clients = $clients->get();

            if ($resolve === 'true') {

                foreach ($clients as &$client) {

                    $client = $this->resolveRelations($client);
                }
            }

            return Main::response(true, 'OK', $clients);
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    /**
     * getClients
     * Devuelve los clientes por medio de paginación
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK|Internal Server Error
     */
    public function getClients()
    {
        $params = Request::all();

        $resolve = 'true';
        if (isset($params['resolve'])) {
            $resolve = $params['resolve'];
            unset($params['resolve']);
        }

        $skip = $params['start'];
        $take = $params['length'];

        $total = Client::get()->count();
        $totalFilters = $total;
        $clients = new Client;

        if ($params['status'] != '') {
            $clients = $clients->where('status', $params['status']);
            $totalFilters = $clients->count();
        }

        $bad_debt = $params['bad_debt'];
        if ($bad_debt != '') {
            if ($bad_debt == 1) {
                $clients = $clients->where('bad_debt', 1);
            }
            $totalFilters = $clients->count();
        }

        $serialNumber = $params['serial_number'];
        if ($serialNumber != '') {
            if ($serialNumber == 'instaled') {
                $clients = $clients->where('serial_number', '<>', '');
            } else {
                $clients = $clients->where('serial_number', '');
            }
            $totalFilters = $clients->count();
        }

        $salesAgent = $params['sales_agent'];
        if ($salesAgent != '') {
            if ($salesAgent == 1) {
                $clients = $clients->where('sales_agent', 1);
            } else {
                $clients = $clients->where('sales_agent', 0);
            }
            $totalFilters = $clients->count();
        }

        if ($params['search']['value'] != '') {
            $clients = $clients->where('name', 'like', '%' . $params['search']['value'] . '%')
                ->orWhere('email', 'LIKE', '%' . $params['search']['value'] . '%');
            $totalFilters = $clients->count();
        }

        foreach ($params['columns'] as $column) {
            switch ($column['data']) {
                case 'name':
                case 'email':
                    if ($column['search']['value'] != '') {
                        $clients = $clients->where($column['data'], 'LIKE', "%$column[search][value]%");
                    }
                    break;
                case 'role':
                    $clients = $clients->whereIn(
                        'id_users',
                        array_map(
                            function ($element) {
                                return $element->user_id;
                            },
                            DB::table('role_user')
                                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                                ->where('roles.name', $value)
                                ->select('role_user.user_id')
                                ->get()
                        )
                    );
                    break;
                default:
                    break;
            }
        }

        $columnsFilter = ['id_clients', 'name', 'id_clients', 'created_at', 'subscription_status', 'email', 'id_clients', 'next_payday'];

        if ($params['order'] != null) {

            foreach ($params['order'] as $order) {
                $columnFilter = $columnsFilter[$order['column']];
                $clients = $clients->orderBy($columnFilter, $order['dir']);
            }
        }

        $clients = $clients->skip($skip)->take($take)->get();
        //$paginate = $clients::paginate()->setPageName("p");
        if ($resolve === 'true') {
            foreach ($clients as &$client) {
                $client = $this->resolveRelations($client);
            }
        }

        $return = [
            "draw"            => (int) $params['draw'],
            "recordsTotal"    => $total,
            "recordsFiltered" => $totalFilters,
            "data"            => $clients
        ];

        return response()->json($return);
    }

    /**
     * show
     * Muestra un cliente (\App\Client) por medio del ID
     *
     * @\App\Client
     * 
     * @param  int      $id ID del Cliente
     * @return response     NULL|ERROR(404)
     */
    public function show($id)
    {

        if ($client = Client::find($id)) {

            return Main::response(true, null, $this->resolveRelations($client));
        } else {

            return Main::response(false, null, null, 404);
        }
    }

    /**
     * setStatus
     * Le asigna un status al cliente(\App\Client) por medio de un $id.
     * Status aplicables:
     *   - invalid
     *     Se genera una invalidación(\App\Invalid), con la descripción de esto.
     *   - rejected
     *     Se genera un rechazo(\App\Rejection), con la descripción de esto.
     *   - accepted
     *   - canceled
     *     Se genera una cancelación(\App\Cancelation), con la descripción de esto.
     *     Se cancela la suscripción en CONEKTA
     *   - standby
     *  Se le envía un EMAIL al cliente, sobre el cambio de status.
     *
     * @Conekta
     * @Mail
     * @App\Client
     * @App\User
     * @App\Invalid
     * @App\Rejection
     * @App\Cancelation
     * 
     * @param int $id ID del cliente
     * @return response     NULL|Bad Request|ERROR(404)
     */
    public function setStatus($id)
    {

        if ($client = Client::find($id)) {

            $input = Request::all();

            $validator = Validator::make($input, ['status' => 'required|in:invalid,rejected,accepted,canceled,standby']);

            if ($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
            }

            if (in_array($input['status'], array('invalid', 'rejected', 'canceled'))) {

                $validator = Validator::make($input, ['reasons' => 'required']);

                if ($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                }
            }

            switch ($input['status']) {

                case 'accepted':

                    $user = User::find($client->id_users);

                    if (!$user->hasRole('Seller')) {

                        $user->attachRole(Role::where('name', '=', 'Seller')->first());
                        $user->save();
                    }

                    $this->notifyStatusContract($client, $input['status']);

                    break;

                case 'invalid':

                    $invalidation = new Invalidation;
                    $invalidation->id_clients = $client->id_clients;
                    $invalidation->reasons = $input['reasons'];
                    $invalidation->save();
                    // if ($client->status == 'prospect') {
                    //     $password = rand(100000, 999999);
                    //     $usr = User::where('email', $client->email)->first();
                    //     $usr->password = bcrypt($password);
                    //     $usr->save();

                    //     $this->notifyPassword([
                    //         'client'   => $client,
                    //         'password' => $password
                    //     ]);
                    // } else {
                    //     $this->notifyStatusContract($client, $input['status'], $input['reasons']);
                    // }
                    $this->notifyStatusContract($client, $input['status'], $input['reasons']);
                    break;

                case 'rejected':

                    $rejection = new Rejection;
                    $rejection->id_clients = $client->id_clients;
                    $rejection->reasons = $input['reasons'];
                    $rejection->save();
                    $this->notifyStatusContract($client, $input['status'], $input['reasons']);

                    break;

                case 'canceled':

                    $cancellation = new Cancellation;
                    $cancellation->id_clients = $client->id_clients;
                    $cancellation->reasons = $input['reasons'];
                    $cancellation->save();
                    $client->pay_day     = null;
                    $client->next_payday = null;
                    $client->subscription_status = 'canceled';
                    $this->notifyStatusContract($client, $input['status'], $input['reasons']);
                    /*if($client->subscription_status != 'canceled' && !is_null($client->subscription_status)) {
                        Conekta::setApiKey(env('CONEKTA_API_KEY'));
                        Conekta::setLocale('es');
                        $customer = Conekta_Customer::find($client->conekta_token);
                        $customer->subscription->cancel();
                    }*/

                    break;
            }

            $client->status = $input['status'];
            $client->save();

            return Main::response(true, null, $client);
        } else {

            return Main::response(false, null, null, 404);
        }
    }

    /**
     * setGroup
     * Le asigna un grupo(\App\Groups) al cliente(\App\Client) por medio de un $id.
     * 
     * @App\Client
     * @App\Groups
     * 
     * @param int $id ID del cliente
     * @return response     NULL|Bad Request|ERROR(404)
     */
    public function setGroup($id)
    {

        if ($client = Client::find($id)) {

            $validator = Validator::make(
                $input = Request::all(),
                [
                    'id_groups' => 'required|integer|exists:groups'
                ]
            );

            if ($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
            }

            $group = Group::find($input['id_groups']);
            $client->id_groups          = $input['id_groups'];
            //$client->deposit            = $group->deposit;
            //$client->installation_fee   = $group->installation_fee;
            //$client->monthly_fee        = $group->monthly_fee;

            if ($client->status == 'invalid' || $client->status == 'rejected' || $client->status == 'standby') {
                $charged = Charge::where('customer_id', $client->conekta_token)
                    ->whereNotNull('paid_at')
                    ->get();

                if (count($charged) < 1) {
                    $client->status       = 'standby';
                    $client->collected_at = null;
                    $client->pay_day      = null;
                    $client->next_payday  = null;
                }
            }

            $client->save();

            //self::subscription('update', $client);

            return Main::response(true, 'OK', $client);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * defaultCardFirst
     * Organiza las tarjetas almacenadas en CONKECTA, y pone en primer lugar la predeterminada.
     * 
     * @param  json   $cards         Objeto JSON de CONKETA con las tarjetas
     * @param  string $defaultCardId ID de tarjeta predeterminada
     * @return array                 Array de tarjetas, con la predeterminada de primero.
     */
    private static function defaultCardFirst(array $cards, $defaultCardId)
    {

        $result = [];

        foreach ($cards as $card) {

            $card->id == $defaultCardId ? array_unshift($result, $card) : array_push($result, $card);
        }

        return $result;
    }

    /**
     * selectBrand
     * Devuelve la marca de la tarjeta, SOLO HACE DIFERENCIA SI ES AMEX O VISA(cualquier otra que no sea visa, la maneja como visa)
     * 
     * @param  array  $cards [description]
     * @return string        AMERICAN_EXPRESS|VISA
     */
    private static function selectBrand(array $cards)
    {

        return array_find($cards, function ($card) {
            return $card->brand === 'AMERICAN_EXPRESS';
        }) ? 'AMERICAN_EXPRESS' : 'VISA';
    }

    /**
     * subscription
     * @deprecated version 2
     * Suscribe un cliente a un plan de pagos en CONEKTA.
     * Pasos:
     *   1) Revisa si se esta intentando agregar una suscripcion con promocion (trial days)
     *   2) Obtiene el ID de PLAN (plan_id), dependiendo de:
     *     - Si la tarjeta es AMEX
     *     - Si el cliente esta donando la responsabilidad social.
     *   3) Crea o actualiza la suscripción del cliente en CONEKTA
     *   4) Si la suscripcion de CONEKTA está activa, se le asigna en la base de datos el costo.
     *   4.1) Si el cliente no es VIAJERO (ya tiene a los referidos para su agua gratis), su suscripción se pausa.
     * 
     * @Conekta
     * @Conekta_Customer
     * @Conekta_Charge
     * @App\Client
     * @App\Fee
     * @App\Group
     * 
     * @param  string $action create|update
     * @param  Client $client Objeto Eloquent Client
     * @return mixed          void|Conekta_ProcessingError
     */
    /*
    public static function subscription($action, $client) {
      
      Conekta::setApiKey(env('CONEKTA_API_KEY'));
      Conekta::setLocale('es');
      
      $customer = Conekta_Customer::find($client->conekta_token);
      $cards = self::defaultCardFirst(json_decode($customer->cards->__toJSON()), $customer->default_card_id);
      $brand = self::selectBrand($cards);
      
      $group = Group::find($client->id_groups);

      if($group->trial_days_price != 0) {

          $params = array(
              'description' => 'Promoción',
              'amount' => $group->trial_days_price,
              'currency' => 'MXN',
              'details' => array(
                  'name' => $client->name,
                  'phone' => $client->phone,
                  'email' => $client->email,
                  'line_items' => [[
                      'name' => 'Promoción',
                      'description' => 'Promoción',
                      'unit_price' => $group->trial_days_price,
                      'quantity' => 1,
                      'type' => 'digital'
                  ]],
                  "shipment" => array(
                    "carrier" => "aguagente",
                    "service" => "next_day",
                    "price" => 0,
                    "address" => array(
                      "street1" => $client->address,
                      "street2" => $client->address,
                      "street3" => $client->address,
                      "city" => $client->county,
                      "state" => $client->state,
                      "zip" => $client->postal_code,
                      "country" => "Mexico"
                    )
                  )
              )
          );

          $charged = false;

          foreach($cards as $card) {

              try {

                  $params['card'] = $card->id;
                  Conekta_Charge::create($params);
                  $charged = true;
                  break;

              } catch(\Exception $e) {
                
                // try next card
                
              }

          }

          if(!$charged)
              throw new Conekta_ProcessingError();

      }

      if($client->social_responsability && $brand === 'AMERICAN_EXPRESS') {
          $plan_id = $group->social_amex_plan_id;
      } else if($brand === 'AMERICAN_EXPRESS') {
          $plan_id = $group->amex_plan_id;
      } else if($client->social_responsability) {
          $plan_id = $group->social_plan_id;
      } else {
          $plan_id = $group->plan_id;
      }
      
      switch($action) {
        case 'create': $subscription = $customer->createSubscription([ 'plan_id' => $plan_id ]); break;
        case 'update': $subscription = $customer->subscription->update([ 'plan_id' => $plan_id ]); break;
        default: break;
      }
      
      if(($subscription->status == 'active') ||
         ($subscription->status == 'in_trial') ||
         ($subscription->status == 'paused')) {
          
        $client->subscription_fee = Fee::where('brand', '=', $brand)->where('monthly_installments', '=', 1)->first()->percentage * $group->amount / 100;
        $client->save();
        
        if($subscription->status != 'paused' && isset($client->level) && $client->level != 'VIAJERO') {

          $subscription->pause();

        }

      } else {
          throw new Conekta_ProcessingError();
      }
      
    }
    */

    /**
     * charge
     * Genera el 1er cargo al cliente y le genera su pay_day y su next_payday.
     *
     * @param \App\Client $client   Objeto Eloquent con la información del Cliente.
     * @return mixed                void|CONKETA_ERROR
     */
    private function charge($client)
    {
        Conekta::setApiKey(env('CONEKTA_API_KEY'));
        Conekta::setLocale('es');

        $customer = Conekta_Customer::find($client->conekta_token);
        $cards    = self::defaultCardFirst(json_decode($customer->cards->__toJSON()), $customer->default_card_id);
        $brand    = self::selectBrand($cards);

        $group    = $client->signed_in_group;
        if (is_null($group) || $group == '') {
            $group = Group::find($client->id_groups);
        }

        // ($group->name, $group->trial_days);
        
        $dayToday = date('d');
        if ($dayToday > 27) {
            $dayToday = 27;
        }

        $today      = date('Y-m-') . $dayToday;
        $nextPayDay = date('Y-m-d', strtotime('+1 month', strtotime($today)));
        $client->collected_at = $today;
        $client->next_payday  = $nextPayDay;
        $client->pay_day      = str_pad($dayToday, 2, '0', STR_PAD_LEFT);

        if ($group->trial_days > 0) {
            $nextPayDay          = date('Y-m-d', strtotime('+' . $group->trial_days . ' days', strtotime($today)));
            $client->next_payday = $nextPayDay;
            $client->pay_day     = str_pad($dayToday, 2, '0', STR_PAD_LEFT);
        }

        if (
            ($group->trial_days > 0 && $group->trial_days_price > 0)
            || ($group->trial_days == 0)
        ) {
            $charged = false;
            $amount = $group->monthly_fee;
            if ($group->trial_days_price > 0) {
                $amount = $group->trial_days_price;
            }
            if ($client->social_responsability == 1) {
                $sr = ($amount / 1.16) * .007;
                $amount += $sr;
            }
            $params  = [
                'description' => 'Aguagente Período: ' . date('Y-m'),
                'amount'      => $amount,
                'currency'    => 'MXN',
                'details'     => [
                    'name'       => $client->name,
                    'phone'      => str_limit($client->phone, 20, ''),
                    'email'      => $client->email,
                    'line_items' => [
                        [
                            'name'        => 'Aguagente Período: ' . date('Y-m', strtotime($today)),
                            'description' => 'Aguagente Período: ' . date('Y-m', strtotime($today)),
                            'unit_price'  => $group->monthly_fee,
                            'quantity'    => 1
                        ]
                    ],
                    "shipment" => [
                        "carrier" => "aguagente",
                        "service" => "next_day",
                        "price"   => 0,
                        "address" => [
                            "street1" => $client->address,
                            "street2" => $client->address,
                            "street3" => $client->address,
                            "city"    => $client->county,
                            "state"   => $client->state,
                            "zip"     => $client->postal_code,
                            "country" => "Mexico"
                        ]
                    ]
                ]
            ];

            foreach ($cards as $card) {

                try {
                    $params['card'] = $card->id;
                    Conekta_Charge::create($params);

                    $charged                     = true;
                    $client->subscription_status = 'active';
                    $client->subscription_fee    = Fee::where('brand', '=', $brand)->where('monthly_installments', '=', 1)->first()->percentage * $group->amount / 100;
                    $client->save();
                    break;
                } catch (\Exception $e) { }
            }
        } else {
            $charged                     = true;
            $client->subscription_status = 'active';
            $client->subscription_fee    = Fee::where('brand', '=', $brand)->where('monthly_installments', '=', 1)->first()->percentage * $group->amount / 100;
            $client->save();
        }

        if (!$charged) {
            throw new Conekta_ProcessingError();
        }
    }

    /**
     * subscribeToPlan
     * Genera un cargo(\App\Charge) al cliente(\App\Client) y después lo suscribe a un plan de CONEKTA.
     * Pasos:
     *  1) Revisa si ya se ha generado un cargo (\App\Charge)  por la instalación, si no, le genera el cargo en CONEKTA
     *      - Calcula el monto inicial (setup fee) a cobrar:
     *        + Extras
     *        + Responsabilidad social
     *        + Deposito
     *      - Genera el cargo en CONEKTA
     *  2) Si hay el cargo, pero no tiene una suscripción activa:
     *      - Ejecuta la funcion "suscription" para generar la suscripción incial.
     *  3) Revisa si no hay un ticket de instalación generado:
     *      - Genera el Ticket
     *      - Envia un Email al cliente avisando sobre el status de su instalación.
     * 
     * @Conekta
     * @Conekta_Chargue
     * @Mail
     * @\App\Client
     * @\App\Charge
     * @\App\Contract
     * @\App\Extras
     * @\App\Fee
     * 
     * @param  int $id ID de cliente
     * @return response     OK|Frobidden|Payment Required|Internal Server Error|Not Found
     */
    public function subscribeToPlan($id)
    {
        // NOE si algo falla, elimina estas lineas
        $now = \Carbon\Carbon::now();
        $endOfmonth = \Carbon\Carbon::now()->endOfMonth();

        if ($endOfmonth->isToday() && $now->format('H') > 17) {
            return Main::response(false, 'Forbidden', 'You cant charge a client on the last day of the month after 5pm', 403);
        }
        // HASTA aqui

        if ($client = Client::find($id)) {

            $charge = Charge::where('customer_id', '=', $client->conekta_token)
                ->where('description', '=', 'Extras')
                ->whereNotNull('paid_at')
                ->first();

            if (!$charge) {

                Conekta::setApiKey(env('CONEKTA_API_KEY'));
                Conekta::setLocale('es');

                $customer    = Conekta_Customer::find($client->conekta_token);
                $cards       = self::defaultCardFirst(json_decode($customer->cards->__toJSON()), $customer->default_card_id);
                $brand       = self::selectBrand($cards);

                $contract    = Contract::where('id_clients', '=', $client->id_clients)->first();
                $extras      = Extras::where('id_contracts', '=', $contract->id_contracts)->get();

                $extrasTotal = 0;

                $lineItems   = [];
                $discount    = [];

                foreach ($extras as $extra) {

                    if ($extra->element_name == 'Acero inoxidable') {

                        $extrasTotal += $client->installation_fee;

                        $lineItems[] = array(
                            'name'        => $extra->element_name,
                            'description' => $extra->element_name,
                            'unit_price'  => $client->installation_fee,
                            'quantity'    => 1,
                            'type'        => 'digital'
                        );
                    } else {

                        $extrasTotal += $extra->price * 100;

                        $lineItems[] = array(
                            'name'        => $extra->element_name,
                            'description' => $extra->element_name,
                            'unit_price'  => $extra->price * 100,
                            'quantity'    => 1,
                            'type'        => 'digital'
                        );
                    }
                }

                $extrasResponsability = 0;

                if ($client->social_responsability) {

                    $extrasResponsability = $extrasTotal / 1.16 * .007;

                    $lineItems[] = array(
                        'name'        => 'Responsabilidad social',
                        'description' => 'Responsabilidad social',
                        'unit_price'  => $extrasResponsability,
                        'quantity'    => 1,
                        'type'        => 'digital'
                    );
                }

                if ($client->coupon != 0) {
                    $coupon = Coupon::where('id_coupon', $client->coupon)->first();
                    $discount[] = [
                        'code'   => 'Cupón de descuento ' . $coupon->token,
                        'amount' => $coupon->rewards[$coupon->level],
                        'type'   => 'coupon'
                    ];
                }

                if ($client->coupon != 0) {
                    $coupon = Coupon::where('id_coupon', $client->coupon)->first();
                    $discount[] = [
                        'code'   => 'Cupón de descuento ' . $coupon->token,
                        'amount' => $coupon->rewards[$coupon->level] * 100,
                        'type'   => 'coupon'
                    ];
                }

                $lineItems[] = array(
                    'name'        => 'Depósito',
                    'description' => 'Depósito',
                    'unit_price'  => $client->deposit,
                    'quantity'    => 1,
                    'type'        => 'digital'
                );

                $chargeAmount = $extrasTotal + $extrasResponsability + $client->deposit;

                if (($chargeAmount + 5000) > $discount[0]['amount']) {
                    $chargeAmount = $chargeAmount - $discount[0]['amount'];
                }

                if ($chargeAmount != 0) {

                    $percentage = Fee::where('brand', '=', $brand)
                        ->where('monthly_installments', '=', $client->monthly_installments)
                        ->first()
                        ->percentage;

                    if ($percentage != 0) {

                        $commission   = $chargeAmount * $percentage / 100;
                        $chargeAmount = $chargeAmount + $commission;
                        $lineItems[]  = array(
                            'name'        => 'Comisiones bancarias',
                            'description' => 'Comisiones bancarias',
                            'unit_price'  => $commission,
                            'quantity'    => 1,
                            'type'        => 'digital'
                        );
                        $client->charge_fee = $commission;
                        $client->save();
                    }

                    $params = array(
                        'description' => 'Extras',
                        'amount'      => $chargeAmount,
                        'currency'    => 'MXN',
                        'details'     => array(
                            'name'       => $client->name,
                            'phone'      => $client->phone,
                            'email'      => $client->email,
                            'line_items' => $lineItems,
                            "shipment"   => array(
                                "carrier" => "aguagente",
                                "service" => "next_day",
                                "price"   => 0,
                                "address" => array(
                                    "street1" => $client->address,
                                    "street2" => $client->address,
                                    "street3" => $client->address,
                                    "city"    => $client->county,
                                    "state"   => $client->state,
                                    "zip"     => $client->postal_code,
                                    "country" => "Mexico"
                                )
                            )
                        )
                    );

                    if ($client->coupon != 0) {
                        $params['discount_lines'] = $discount;
                    }

                    if ($client->monthly_installments != 1)
                        $params['monthly_installments'] = $client->monthly_installments;

                    $charged = true;

                    foreach ($cards as $card) {

                        try {

                            $params['card'] = $card->id;
                            Conekta_Charge::create($params);
                            $charged = true;
                            break;
                        } catch (\Exception $e) {

                            // do nothing

                        }
                    }

                    if (!$charged)
                        return Main::response(false, 'Payment Required', null, 402);
                }
            }

            //TRY CHARGE to conekta instead of suscription
            if (!$client->subscription_status) {

                try {
                    $this->charge($client);
                    //$this->subscription("create", $client);

                } catch (Conekta_ProcessingError $e) {

                    return Main::response(false, 'Payment Required', $e->getMessage(), 402);
                } catch (Exception $e) {

                    return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
                }
            }

            if (!Ticket::where('id_clients', '=', $client->id_clients)->first()) {

                $errorCode = ErrorCode::where('name', 'LIKE', '%Instal%')->first();

                if (!$errorCode) {

                    return Main::response(false, 'Internal Server Error', 'Error code not found', 500);
                } else {

                    $ticket = new Ticket;
                    $ticket->id_clients = $client->id_clients;
                    $ticket->id_error_codes = $errorCode->id_error_codes;
                    $ticket->description = 'Favor de instalar equipo.';
                    $ticket->estimated_service_fee = null;
                    $ticket->estimated_service_fee_reasons = null;
                    $ticket->status = 'opened';
                    $ticket->type = 'installation';
                    $ticket->save();

                    $ticket->client = $client;

                    $this->ticketCreation($ticket);
                }
            }

            return Main::response(true, 'OK', null);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * subscribeToPlanOffline
     * Genera la referencia para el 1er cargo por pago offline (OXXO/SPEI)
     *
     * @param int $id
     * @return response     OK|Conflict|Not Found
     */
    public function subscribeToPlanOffline($id)
    {
        if ($client = Client::find($id)) {

            $charge = Charge::where('customer_id', '=', $client->conekta_token)
                ->where('description', '=', 'Extrass')
                ->whereNotNull('paid_at')
                ->first();

            if (!$charge) {

                /*
                Conekta::setApiKey(env('CONEKTA_API_KEY'));
                Conekta::setLocale('es');
                
                $customer    = Conekta_Customer::find($client->conekta_token);
                $cards       = self::defaultCardFirst(json_decode($customer->cards->__toJSON()), $customer->default_card_id);
                $brand       = self::selectBrand($cards);
                */

                $inputs   = Request::all();
                $contract = Contract::where('id_clients', '=', $client->id_clients)->first();
                $extras   = Extras::where('id_contracts', '=', $contract->id_contracts)->get();
                $group    = Group::find($client->id_groups);

                $extrasTotal = 0;
                $lineItems   = [];
                $dayToday    = date('d');

                if ($dayToday > 27) {
                    $dayToday = 27;
                }

                $today = date('Y-m-') . $dayToday;

                $params = [
                    'client'      => $client,
                    'description' => 'Extras',
                    'type'        => $inputs['type']
                ];

                foreach ($extras as $extra) {

                    if ($extra->element_name == 'Acero inoxidable') {

                        $extrasTotal += $client->installation_fee;

                        $lineItems[] = array(
                            'name'        => $extra->element_name,
                            'description' => $extra->element_name,
                            'unit_price'  => $client->installation_fee,
                            'quantity'    => 1,
                            'type'        => 'digital'
                        );
                    } else {

                        $extrasTotal += $extra->price * 100;

                        $lineItems[] = array(
                            'name'        => $extra->element_name,
                            'description' => $extra->element_name,
                            'unit_price'  => $extra->price * 100,
                            'quantity'    => 1,
                            'type'        => 'digital'
                        );
                    }
                }

                $extrasResponsability = 0;

                if ($client->social_responsability) {

                    $extrasResponsability = $extrasTotal / 1.16 * .007;

                    $lineItems[] = array(
                        'name'        => 'Responsabilidad social',
                        'description' => 'Responsabilidad social',
                        'unit_price'  => $extrasResponsability,
                        'quantity'    => 1,
                        'type'        => 'digital'
                    );
                }

                $lineItems[] = array(
                    'name'        => 'Depósito',
                    'description' => 'Depósito',
                    'unit_price'  => $client->deposit,
                    'quantity'    => 1,
                    'type'        => 'digital'
                );

                if (
                    ($group->trial_days > 0 && $group->trial_days_price > 0)
                    || ($group->trial_days == 0)
                ) {
                    $lineItems[] = [
                        'name'        => 'Aguagente Período: ' . date('Y-m', strtotime($today)),
                        'description' => 'Aguagente Período: ' . date('Y-m', strtotime($today)),
                        'unit_price'  => $group->monthly_fee + ($client->social_responsability ? ($group->monthly_fee / 1.16) * .007 : 0),
                        'quantity'    => 1
                    ];
                }

                if (isset($inputs['months_ahead'])) {
                    $description = 'Aguagente Período: Pago adelantado ( ' . $inputs['months_ahead'] . ' )';

                    $monthly_fee = $group->monthly_fee;
                    $sr          = ($client->social_responsability == 1 ? ($group->monthly_fee / 1.16) * .007 : 0);
                    $amount      = ($monthly_fee + $sr) * $inputs['months_ahead'];

                    $lineItems[] = [
                        'name'        => $description,
                        'description' => $description,
                        'unit_price'  => $amount,
                        'quantity'    => 1
                    ];

                    $params['months_ahead'] = $inputs['months_ahead'];
                }

                $params['line_items'] = $lineItems;

                $order = $this->createCharge($params);

                switch ($order['error']) {
                    case 0:
                        return Main::response(true, 'OK', $order, 200);
                        break;

                    case 1:
                        return Main::response(true, 'ERROR', $order, 500);
                        break;
                }
            }
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * unsubscribeToPlan
     * Busca al cliente y cancela sus suscripcion en CONEKTA
     * 
     * @Conekta
     * @Conekta_Customer
     * @App\Client
     * 
     * @param int $id ID de cliente
     * @return response     OK|Conflict|Not Found
     */
    public function unsubscribeToPlan($id)
    {

        if ($client = Client::find($id)) {

            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');

            $customer = Conekta_Customer::find($client->conekta_token);

            if (isset($customer->subscription)) {

                $customer->subscription->cancel();

                return Main::response(true, 'OK', null);
            } else {

                return Main::response(false, 'Conflict', null, 409);
            }
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * hierarchicalTree
     * Devuelve el Arbol organizado en jeraquías del Cliente(\App\Client)
     * 
     * @App\Client
     * 
     * @param int $id ID de cliente
     * @return response     OK|Internal Server Error
     */
    public function hierarchicalTree($id)
    {

        try {

            return Main::response(true, 'OK', Client::getHierarchicalTree($id));
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    /**
     * resolveRelations
     * Asigna las relaciones de Group(\App\Group), Contrato (\App\Contract) y Roles (\App\Role) al objeto $client
     * 
     * @App\Group
     * @App\Contract
     * @App\Role
     * 
     * @param Client $client Objeto Eloquent Client
     * @return Client $client Objeto Eloquent Client
     */
    private function resolveRelations($client)
    {

        $group = DB::table('groups')
            ->where('id_groups', $client->id_groups)
            ->first();

        $group->sign_into = Group::find($group->sign_into);

        $client->group = $group;
        $client->contract = Contract::where('id_clients', '=', $client->id_clients)->first();
        $client->cards = Card::where('id_clients', '=', $client->id_clients)->get();
        $roles = DB::table('role_user')
            ->where('user_id', $client->id_users)
            ->get();

        foreach ($roles as &$rol) {

            $rol = Role::find($rol->role_id);
        }

        $client->roles = $roles;

        return $client;
    }

    /**
     * unregistered
     * Envía un Email contacto@aguagente.com, es el fromulario de contacto.
     *
     * @Mail
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response void|Bad Request
     */
    public function unregistered()
    {

        $validator = Validator::make(
            $input = Request::all(),
            [
                'nombre'    => 'required|max:256',
                'correo'    => 'required|email',
                'telefono'  => 'required|max:32',
                'interesa'  => 'required|max:256',
                'mensaje'   => 'required|max:8192'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $this->contactEmail($input);
    }

    /**
     * sendPassword
     *  Envía el password al usuario cuando se registra
     * 
     * @Mail
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return mixed    void|response(Bad Request)
     */
    public function sendPassword()
    {

        $validator = Validator::make(
            $input = Request::all(),
            [
                'email'     => 'required|email',
                'name'      => 'required|string',
                'password'  => 'required|string'
            ]
        );

        if ($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
        }

        $this->sentPassword($input);
    }

    public function updatePhoto($id)
    {
        $client = Client::find($id);

        if ($client) {
            try {

                $input    = Request::all();
                $contract = Contract::where('id_clients', $id)->first();

                $location = realpath('documents/contracts') . "/$client->referred_by";
                if (!file_exists($location)) {
                    mkdir($location);
                }

                $location .= "/$contract->id_contracts";

                if (!file_exists($location)) {
                    mkdir($location);
                }

                if (!file_exists($location . "/history")) {
                    mkdir($location . "/history");
                }

                if (isset($input['proof_address']) && $input['proof_address']) {
                    foreach (glob("$location/proof_of_address.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                        //unlink($file);
                    }
                    $input['proof_address']->move($location, 'proof_of_address' . '.' . $input['proof_address']->guessExtension());
                }

                if (isset($input['id']) && $input['id']) {
                    foreach (glob("$location/id.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                        //unlink($file);
                    }
                    $input['id']->move($location, 'id' . '.' . $input['id']->guessExtension());
                }

                if (isset($input['id_reverse']) && $input['id_reverse']) {
                    foreach (glob("$location/id_reverse.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                    }
                    $input['id_reverse']->move($location, 'id_reverse' . '.' . $input['id_reverse']->guessExtension());
                }

                if (isset($input['profile']) && $input['profile']) {
                    foreach (glob("$location/profile.*") as $file) {
                        $afrchivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                    }
                    $input['profile']->move($location, 'profile' . '.' . $input['profile']->guessExtension());
                }
                return Main::response(true, null, $this->resolveRelations($client));
            } catch (\Exception $e) {

                return Main::response(false, null, $e->getMessage(), 400);
            }
        } else {

            return Main::response(false, null, null, 404);
        }
    }

    /**
     * update
     * Guarda las modificaciones realizadas a un cliente (\App\Cliente) por medio del ID.
     * Al actualizar los datos, el status del cliente, si es inválido, vuelve a ser "standby"
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Client
     * @\App\Contract
     * 
     * @param int $id ID de Cliente
     * @return response null|Exception Error|Error(404)
     */
    public function update($id)
    {

        $client = Client::find($id);

        if ($client) {

            try {

                $input = Request::all();
                $isProspect = false;

                $client->name            = $input['name'];
                $client->rfc             = ($input['rfc'] == 'null' || $input['rfc'] == '') ? null : $input['rfc'];
                $client->phone           = $input['phone'];
                $client->colony          = $input['colony'];
                $client->address         = $input['address'];
                $client->outdoor_number  = ($input['outdoor_number'] == 'null' || $input['outdoor_number'] == '') ? null : $input['outdoor_number'];
                $client->inside_number   = ($input['inside_number'] == 'null' || $input['inside_number'] == '') ? null : $input['inside_number'];
                $client->between_streets = $input['between_streets'];
                $client->postal_code     = $input['postal_code'];
                $client->state           = $input['state'];
                $client->county          = $input['county'];
                // $client->monthly_fee     = $input['monthly_fee'];
                $client->extra_data      = $input['extra_data'];
                $client->invoice_data    = ($input['invoice_data'] == 'null' || $input['invoice_data'] == '') ? null : $input['invoice_data'];
                
                if ($input['coordinate'] != '') {
                    $client->coordinate = json_decode($input['coordinate']);
                }

                if (isset($input['email'])) {

                    $user = User::where('email', '=', $client->email)->first();
                    if ($user) {
                        $user->email = $input['email'];
                        $user->save();
                    }
                    $client->email = $input['email'];
                }

                if ($client->status == 'prospect') {
                    $isProspect = true;
                }

                if ($client->status == 'invalid' || $client->status == 'rejected' || $client->status == 'standby' || $client->status == 'prospect') {
                    $charged = Charge::where('customer_id', $client->conekta_token)
                        ->whereNotNull('paid_at')
                        ->get();

                    if (count($charged) < 1) {
                        $client->status       = 'standby';
                        $client->collected_at = null;
                        $client->pay_day      = null;
                        $client->next_payday  = null;
                    }
                }

                $contract = Contract::where('id_clients', $id)->first();

                $location = realpath('documents/contracts') . "/$client->referred_by";

                if (!file_exists($location)) {
                    mkdir($location);
                }

                $location .= "/$contract->id_contracts";

                if (!file_exists($location)) {
                    mkdir($location);
                }

                if (!file_exists($location . "/history")) {
                    mkdir($location . "/history");
                }

                if (isset($input['proof_address']) && $input['proof_address']) {
                    foreach (glob("$location/proof_of_address.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                        //unlink($file);
                    }
                    $input['proof_address']->move($location, 'proof_of_address' . '.' . $input['proof_address']->guessExtension());
                }

                if (isset($input['id']) && $input['id']) {
                    foreach (glob("$location/id.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                        //unlink($file);
                    }
                    $input['id']->move($location, 'id' . '.' . $input['id']->guessExtension());
                }

                if (isset($input['id_reverse']) && $input['id_reverse']) {
                    foreach (glob("$location/id_reverse.*") as $file) {
                        $archivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                    }
                    $input['id_reverse']->move($location, 'id_reverse' . '.' . $input['id_reverse']->guessExtension());
                }


                if (isset($input['profile']) && $input['profile']) {
                    foreach (glob("$location/profile.*") as $file) {
                        $afrchivo = pathinfo($file);
                        rename($file, $location . "/history/" . $archivo['filename']  . strtotime("now") . "." . $archivo['extension']);
                    }
                    $input['profile']->move($location, 'profile' . '.' . $input['profile']->guessExtension());
                }

                foreach ($input['cards'] as $card) {
                    $cards_client = Card::where('id_cards', $card['id_cards'])->get();
                    foreach ($cards_client as $card_client) {
                        $card_client->name           = $card['name'] != '' ? $card['name'] : $client->name;
                        $card_client->address        = $card['address'];
                        $card_client->outdoor_number = $card['outdoor_number'];
                        $card_client->inside_number  = $card['inside_number'];
                        $card_client->phone          = $card['phone'];
                        $card_client->email          = $card['email'] != '' ? $card['email'] : $client->email;
                        $card_client->postal_code    = $card['postal_code'];
                        $card_client->colony         = $card['colony'];
                        $card_client->county         = $card['county'];
                        $card_client->state          = $card['state'];
                        $card_client->country        = $card['country'];
                        $card_client->save();
                    }
                }

                $client->save();

                if ($isProspect) {
                    $password = rand(100000, 999999);
                    $usr = User::where('email', $client->email)->first();
                    $usr->password = bcrypt($password);
                    $usr->save();

                    $this->notifyPassword([
                        'client'   => $client,
                        'password' => $password
                    ]);
                }

                return Main::response(true, null, $this->resolveRelations($client));
            } catch (\Exception $e) {

                return Main::response(false, null, $e->getMessage(), 400);
            }
        } else {

            return Main::response(false, null, null, 404);
        }
    }

    /**
     * registerCommissions
     * Genera y guarda en la base de datos las comisiones generadas para cada cliente, por sus referidos.
     * Esta función usa de manera principal \App\CommissionService para realizar todos los calculos.
     * Si hay error, genera un registro en el log (comissions.log)
     * 
     * @App\MySQLClientRepository
     * @App\CommissionService
     * @App\ClientService
     * @App\Client
     * @App\User
     * @App\Commission
     *
     * @return void
     */
    public function registerCommissions()
    {

        try {

            $clientRepository = new MySQLClientRepository();

            $commissionService = new CommissionService(
                $clientRepository,
                new ClientService($clientRepository)
            );

            foreach (Client::get() as $client) {

                $commissions = $commissionService->calculateCommissions($client);

                $user = User::find($client->id_users);

                if (
                    $user && $user->hasRole('Sales Agent') &&
                    $client->level != Level::VIAJERO &&
                    $commissions['is_active'] &&
                    $commissions['total'] > 0
                ) {

                    $commission = new Commission;
                    $commission->id_clients = $client->id_clients;
                    $commission->calculated_at = date('Y-m-d H:i:s');
                    $commission->amount = $commissions['total'];
                    $commission->is_active = $commissions['is_active'];
                    $commission->level_1 = $commissions['itemization']['level_1'];
                    $commission->level_2 = $commissions['itemization']['level_2'];
                    $commission->level_3 = $commissions['itemization']['level_3'];
                    $commission->level_4 = $commissions['itemization']['level_4'];
                    $commission->save();
                } else {

                    $commission = new Commission;
                    $commission->id_clients = $client->id_clients;
                    $commission->calculated_at = date('Y-m-d H:i:s');
                    $commission->amount = 0;
                    $commission->is_active = $commissions['is_active'];
                    $commission->level_1 = $commissions['itemization']['level_1'];
                    $commission->level_2 = $commissions['itemization']['level_2'];
                    $commission->level_3 = $commissions['itemization']['level_3'];
                    $commission->level_4 = $commissions['itemization']['level_4'];
                    $commission->save();
                }
            }
        } catch (\Exception $e) {

            file_put_contents('commissions.log', date('Y-m-d H:i:s') . " : " . $e->getMessage() . "\n", FILE_APPEND);
        }
    }

    /**
     * switchInvoice
     * Cambia el valor de "require_invoice" del cliente, el valor cambia al negar su valor actual.
     * 
     * @App\Client
     * 
     * @param int       $id ID del cliente
     * @return response     NULL|ERROR(404)
     */
    public function switchInvoice($id)
    {

        if ($client = Client::find($id)) {

            $client->require_invoice = !$client->require_invoice;
            $client->save();

            return Main::response(true, null, $this->resolveRelations($client));
        } else {

            return Main::response(false, null, null, 404);
        }
    }
    /**
     * delete
     * SOLO SE PUEDE BORRAR UN CLIENTE RECHAZADO(rejected).
     * Elimina todo lo relacionado al cliente:
     *  - Contrato (\App\Contract)
     *  - Extras (\App\Extras)
     *  - Usuario (\App\User)
     *
     * @App\Client
     * @App\Contract
     * @App\Extras
     * @App\User
     * 
     * @param int       $id ID del cliente
     * @return response     OK|Forbidden|Not Found
     */
    public function delete($id)
    {

        if ($client = Client::find($id)) {

            if ($client->status != 'rejected')
                return Main::response(false, 'Forbidden', ['errors' => ['client' => ['Client is not rejected']]], 403);

            $contract = Contract::where('id_clients', '=', $client->id_clients)->first();
            if ($contract) {
                $extras = Extras::where('id_contracts', '=', $contract->id_contracts)->get();
                foreach ($extras as $extra) {
                    $extra->delete();
                }
                $contract->delete();
            }

            $user = User::find($client->id_users);
            $user->delete();

            $client->delete();

            return Main::response(true, 'OK', null, 200);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * attachRole
     * Busca al cliente por medio de su ID y le asigna un Rol(\App\Role)
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Client
     * @App\Role
     * @App\User
     * 
     * @param int       $id ID del cliente
     * @return response     OK|Not Found    
     */
    public function attachRole($id)
    {

        if ($client = Client::find($id)) {

            $input = Request::all();

            $validator = Validator::make($input, ['role' => 'exists:roles,name']);

            if ($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
            }

            $user = User::find($client->id_users);

            if (!$user->hasRole($input['role'])) {

                $user->attachRole(Role::where('name', '=', $input['role'])->first());
                $user->save();
            }

            return Main::response(true, 'OK', $this->resolveRelations($client), 200);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * setLevel
     * Asigna el nivel del cliente(\App\Level), dependiendo de cuantos clientes referidos tenga.
     * Esta función se encarga de iniciar el proceso pesado de revisar la jerarquía de clientes y los niveles de estos a \App\LevelService
     *
     * @App\Client
     * @App\LevelService
     * @App\MySQLClientRepository
     * 
     * @param int       $id ID del cliente
     * @return response     OK|Not Found
     */
    public function setLevel($id)
    {

        if ($client = Client::find($id)) {

            $levelService = new LevelService(new MySQLClientRepository());
            $levelService->setLevels($client);

            return Main::response(true, 'OK', null, 200);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    public function regenerateConektaUser($id)
    {
        $client = Client::find($id);

        if ($client) {
            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');

            $customer = Conekta_Customer::create(array(
                'name'  => $client->name,
                'email' => $client->email,
                'phone' => $client->phone
            ));

            $client->conekta_token = $customer->id;
            $client->save();

            return Main::response(true, 'OK', null, 200);
        }
        return Main::response(false, 'Not Found', null, 500);
    }

    /**
     * canceledSubscriptions
     * Cancela suscripcion, genera los montos de deudas y manda notificacion PUSH a los clientes
     * 
     * @todo do not allow non accepted clients to subscribe to plan
     * @todo what happens if there is no founds in the account
     * 
     * @return boolean
     */

    /*
    public function canceledSubscriptions() {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale('es');

        $customerExceptions = \Config::get('constants.clientsExceptionList');

        foreach(Client::where('status', '=', 'accepted')->get() as $client) {

            $customer = Conekta_Customer::find($client->conekta_token);

            $cards = json_decode($customer->cards->__toJSON());

            $brand = 'AMERICAN_EXPRESS';
            foreach($cards as $card) {
                $brand = $card->brand;
                if($card->brand === 'AMERICAN_EXPRESS')
                    break;
            }

            if(isset($customer->subscription) &&
                     $customer->subscription->status === 'canceled' &&
                     date('d') === date('d', $customer->subscription->billing_cycle_start)) {

                $group = Group::find($client->id_groups);

                if($client->social_responsability && $brand === 'AMERICAN_EXPRESS') {
                    $plan_id = $group->social_amex_plan_id;
                } else if($brand === 'AMERICAN_EXPRESS') {
                    $plan_id = $group->amex_plan_id;
                } else if($client->social_responsability) {
                    $plan_id = $group->social_plan_id;
                } else {
                    $plan_id = $group->plan_id;
                }

                if ( !in_array($client->id_clients, $customerExceptions) ){
                    $subscription = $customer->createSubscription(array('plan_id' => $plan_id));
                }

                if($subscription->status !== 'active') {

                    # failed for the first time.

                }

            }

        }

        $push = new PushNotification();

        foreach(Debt::whereNull('id_charges')->get() as $debt) {

            if($debt->next_try_to_charge == date('Y-m-d')) {

                $client = Client::find($debt->id_clients);

                $customer = Conekta_Customer::find($client->conekta_token);

                $cards = json_decode($customer->cards->__toJSON());

                foreach($cards as $i => $card)
                    if($card->id == $customer->default_card_id) {
                        unset($cards[$i]);
                        array_unshift($cards, $card);
                        break;
                    }

                $charged = false;

                foreach($cards as $card) {

                    try {

                        $charge = Conekta_Charge::create(
                            array(
                                'description' => 'Pago tardío',
                                'amount' => $debt->amount + $debt->collection_fees + $debt->moratory_fees,
                                'currency' => 'MXN',
                                'card' => $card->id,
                                'details' => array(
                                    'name' => $client->name,
                                    'phone' => $client->phone,
                                    'email' => $client->email,
                                    'line_items' => array(
                                        array(
                                            'name' => 'Suscripción',
                                            'description' => 'Suscripción',
                                            'unit_price' => $debt->amount,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        ),
                                        array(
                                            'name' => 'Gastos de cobranza',
                                            'description' => 'Gastos de cobranza',
                                            'unit_price' => $debt->collection_fees,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        ),
                                        array(
                                            'name' => 'Interés moratorio',
                                            'description' => 'Interés moratorio',
                                            'unit_price' => $debt->moratory_fees,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        )
                                    )
                                )
                            )
                        );

                        $client->debt -= $charge->amount;
                        $client->save();

                        $debt->id_charges = $charge->id;
                        $debt->save();

                        $levelService = new LevelService(new MySQLClientRepository());
                        $levelService->setLevels($client);
                        $charged = true;

                        break;

                    } catch(\Exception $e) {

                        // do nothing

                    }

                }

                if(!$charged) {
                    $debt->next_try_to_charge = date('Y-m-d', strtotime('+6 days'));
                    $debt->save();
                }

            } else if(
                (new DateTime($debt->next_try_to_charge))
                    ->sub(new DateInterval('P2D'))
                    ->format('Y-m-d') == date('Y-m-d')
                ) {

                $message = sprintf(
                    'Nuestros informes indican que usted adeuda un pago por la suma de $%.2f MXN.
                    En 48 hrs. intentaremos realizar un cobro a la tarjeta que ha introducido en nuestro sistema.
                    Por favor verifique que tiene suficientes fondos ó modifique su método de pago.',
                    (
                        $debt->amount +
                        $debt->collection_fees +
                        $debt->moratory_fees
                    ) / 100
                );

                $devices = DB::table('devices')
    				->join('users', 'users.id', '=', 'devices.id_users')
    				->join('clients', 'users.id', '=', 'clients.id_users')
    				->where('clients.conekta_token', '=', $debt->customer_id)
    				->select('devices.*')
    				->get();

                foreach($devices as $device) {

        			$devices[$device->platform][] = $push->Device($device->registration_id, array('badge' => 1));

        		}

                if(isset($devices['ios']))
    				$push
    					->app('aguagente-push-ios')
    					->to($push->DeviceCollection($devices['ios']))
    					->send($message);

    			if(isset($devices['android']))
    				$push
    					->app('aguagente-push-android')
    					->to($push->DeviceCollection($devices['android']))
    					->send(
                            $message
                        );

            } else {

                # do nothing

            }

        }

    }
    */
}
