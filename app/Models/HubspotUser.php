<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HubspotUser extends Model
{

    protected $table = 'hubspot_users';
    protected $primaryKey = 'id';

}