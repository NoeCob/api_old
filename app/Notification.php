<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{

    protected $table = 'notifications';
    protected $primaryKey = 'id_notifications';
    protected $fillable = ['id_users', 'type', 'message','id_notification_type','title'];

    const PUSH = 1;
    const EMAIL = 2;

    public function save(array $options = [])
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Saving/Updating Notification' . $this->original['id_notifications'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        parent::save($options);
        $log->table_id = $this->original['id_notifications'];
        $log->save();
    }

    public function delete()
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Deleting Notification: ' . $this->original['id_notifications'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        $log->table_id = $this->original['id_notifications'];
        $log->save();
        return parent::delete();
    }

    public function type_notification()
    {
        return $this->belongsTo('App\NotificationType');
    }
}
