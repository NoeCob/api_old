<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SubscriptionEvent extends Model
{

    protected $table = 'subscription_events';
    protected $primaryKey = 'id_subscription_events';

    public function save(array $options = [])
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Saving/Updating Subscription Event' . $this->original['id_subscription_events'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        parent::save($options);
        $log->table_id = $this->original['id_subscription_events'];
        $log->save();
    }

    public function delete()
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Deleting Subscription Event: ' . $this->original['id_subscription_events'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        $log->table_id = $this->original['id_subscription_events'];
        $log->save();
        return parent::delete();
    }

}
