<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <!-- NAME: 1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bienvenido a Aguagente</title>

    <style type="text/css">
        #outlook a,h1,h2,h3,h4,h5,h6,p{padding:0}#bodyTable,#templateFooter,#templatePreheader,body{background-color:#FAFAFA}#templateFooter,#templateHeader,#templatePreheader{border-bottom:0;padding-top:9px}#templateBody,#templateFooter,#templatePreheader{border-top:0;padding-bottom:9px}p{margin:10px 0}table{border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0}h1,h2,h3,h4,h5,h6{display:block;margin:0}a img,img{border:0;height:auto;outline:0;text-decoration:none}#bodyCell,#bodyTable,body{height:100%;margin:0;padding:0;width:100%}img{-ms-interpolation-mode:bicubic}.ExternalClass,.ReadMsgBody{width:100%}a,blockquote,li,p,td{mso-line-height-rule:exactly}a[href^=sms],a[href^=tel]{color:inherit;cursor:default;text-decoration:none}a,blockquote,body,li,p,table,td{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}h1,h2,h3,h4{color:#202020;font-style:normal;font-weight:700;line-height:125%;letter-spacing:normal;text-align:left;font-family:Helvetica}#bodyCell{padding:10px;border-top:0}.templateContainer{max-width:600px!important;border:0}a.mcnButton{display:block}.mcnImage{vertical-align:bottom}.mcnTextContent{word-break:break-word}.mcnTextContent img{height:auto!important}.mcnDividerBlock{table-layout:fixed!important}h1{font-size:26px}h2{font-size:22px}h3{font-size:20px}h4{font-size:18px}#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:left}#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{color:#656565;font-weight:400;text-decoration:underline}#templateHeader{background-color:#FFF;border-top:0;padding-bottom:0}#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left}#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{color:#2BAADF;font-weight:400;text-decoration:underline}#templateBody{background-color:#FFF;border-bottom:2px solid #EAEAEA;padding-top:0}#templateBody .mcnTextContent,#templateBody .mcnTextContent p{color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left}#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{color:#2BAADF;font-weight:400;text-decoration:underline}#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center}#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{color:#656565;font-weight:400;text-decoration:underline}@media only screen and (min-width:768px){.templateContainer{width:600px!important}}@media only screen and (max-width:480px){.mcnBoxedTextContentContainer,body{min-width:100%!important}.mcnImage,.mcpreview-image-uploader,body{width:100%!important}h1,h2,h3{line-height:125%!important}a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}#bodyCell{padding-top:10px!important}.mcnBoxedTextContentContainer,.mcnCaptionBottomContent,.mcnCaptionLeftImageContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightImageContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionTopContent,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageGroupContentContainer,.mcnTextContentContainer{max-width:100%!important;width:100%!important}.mcnImageGroupContent{padding:9px!important}.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{padding-top:9px!important}.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent,.mcnImageCardTopImageContent{padding-top:18px!important}.mcnImageCardBottomImageContent{padding-bottom:9px!important}.mcnImageGroupBlockInner{padding-top:0!important;padding-bottom:0!important}.mcnImageGroupBlockOuter{padding-top:9px!important;padding-bottom:9px!important}.mcnBoxedTextContentColumn,.mcnTextContent{padding-right:18px!important;padding-left:18px!important}.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{padding-right:18px!important;padding-bottom:0!important;padding-left:18px!important}.mcpreview-image-uploader{display:none!important}h1{font-size:22px!important}h2{font-size:20px!important}h3{font-size:18px!important}h4{font-size:16px!important;line-height:150%!important}#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p,.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{font-size:14px!important;line-height:150%!important}#templatePreheader{display:block!important}#templateBody .mcnTextContent,#templateBody .mcnTextContent p,#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{font-size:16px!important;line-height:150%!important}#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{font-size:14px!important;line-height:150%!important}}
    </style>

</head>

<body>
    <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
                <td align="center" valign="top" id="bodyCell">
                    <!-- BEGIN TEMPLATE // -->
                    <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                        <![endif]-->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                        <tr>
                            <td valign="top" id="templateHeader">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                    <tbody class="mcnImageBlockOuter">
                                        <tr>
                                            <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                                <img align="center" alt="" src="https://gallery.mailchimp.com/1873e6cf1411f341bec3efde9/images/2f8576ce-3974-49d1-9817-54f5a204a8cb.png" width="122.5" style="max-width:245px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                    <tbody class="mcnImageBlockOuter">
                                        <tr>
                                            <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                                <img align="center" alt="" src="https://gallery.mailchimp.com/1873e6cf1411f341bec3efde9/images/9c26a5a6-e132-43ea-ac4a-2461db2971a5.png" width="600" style="max-width:750px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" id="templateBody">
                                <table class="mcnTextBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                            <td class="mcnTextBlockInner" valign="top">
                                                <table style="min-width:100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;" valign="top">
                                                                <h1><br>
                                                                        <span style="color:#696969">
                                                                            <span style="font-family:trebuchet ms,lucida grande,lucida sans unicode,lucida sans,tahoma,sans-serif">¡Hola {{$ticket->client->name}}!</span>
                                                                        </span>
                                                                    </h1>
                                                                <p>
                                                                    <br>
                                                                    <span style="color:#696969">
                                                                            <span style="font-size:12px">
                                                                                <span style="font-family:verdana,geneva,sans-serif">
                                                                                    
                                                                                    Te informamos que el técnico ha finalizado el ticket # {{$ticket->id_tickets}}, para completar el proceso favor de responder a este correo.

                                                                                </span>
                                                                            </span>
                                                                    </span>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                            <td valign="top" class="mcnTextBlockInner">
                                                <div class="mcnTextContent">
                                                    <a href="#"><img src="">
                                                    </a>
                                                </div>
                                                <a href="#"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" id="templateFooter">
                                <table class="mcnCodeBlock" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                            <td class="mcnTextBlockInner" valign="top">
                                                <div class="mcnTextContent" style="text-align: center">
                                                    <a href="https://itunes.apple.com/mx/app/aguagente/id1089456395?l=en&mt=8">
                                                        <img src="{{asset('img/apple.png')}}" width="151" height="52">
                                                    </a>
                                                    <a href="https://play.google.com/store/apps/details?id=com.Arizaga.AguagenteClient&hl=es">
                                                        <img src="{{asset('img/google-play.png')}}" width="155" height="52">
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
                                    <tbody class="mcnFollowBlockOuter">
                                        <tr>
                                            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" style="padding-left:9px;padding-right:9px;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="top">
                                                                                                <!--[if mso]>
                                                                                                <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                <![endif]-->
                                                                                                <!--[if mso]>
                                                                                                <td align="center" valign="top">
                                                                                                <![endif]-->
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                                <a href="http://www.twitter.com/Aguagente" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class="">
                                                                                                                                                </a>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <!--[if mso]>
                                                                                                </td>
                                                                                                <![endif]-->
                                                                                                <!--[if mso]>
                                                                                                <td align="center" valign="top">
                                                                                                <![endif]-->
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                                <a href="http://www.facebook.com/aguagente" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class="">
                                                                                                                                                </a>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                                <!--[if mso]>
                                                                                                </td>
                                                                                                <![endif]-->
                                                                                                <!--[if mso]>
                                                                                                <td align="center" valign="top">
                                                                                                <![endif]-->
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>

                                                                                                                                            <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                                                                                <a href="http://www.aguagente.com/" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" height="24" width="24" class="">
                                                                                                                                                </a>
                                                                                                                                            </td>


                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>

                                                                                                <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                                                                                <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                    <tbody class="mcnDividerBlockOuter">
                                        <tr>
                                            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                                                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <span></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                            <td valign="top" class="mcnTextBlockInner">

                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                                                    <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; text-align: center;">

                                                                <em>Copyright © 2016 Aguagente, Todos los derechos reservados.</em>
                                                                <br>
                                                                <br>
                                                                <strong>Nuestro correo electrónico es:</strong>
                                                                <br> contacto@aguagente.com
                                                                <br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    <!-- // END TEMPLATE -->
                </td>
            </tr>
        </table>
    </center>
</body>

</html>
