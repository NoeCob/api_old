<?php

namespace App\Http\Controllers;

use App\Invalidation;
use App\Http\Controllers\Main;
use DB;
use Request;

class InvalidationsController extends Main {

    /**
     * index
     * Las invalidaciones son
     * Devuelve todas las invalidaciones después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Internal Server Error
     */
    public function index() {
        
        try {

            $invalidations = DB::table('invalidations');

            foreach(Request::query() as $name => $value) {
                
                $invalidations = $invalidations->where($name, $value);

            }

            return Main::response(true, null, $invalidations->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
   
    }

    /**
     * show
     * Muestra una invalidación (\App\Invalidation) por medio del ID
     *
     * @\App\Invalidation
     * 
     * @param  int      $id ID de la invalidacion
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($invalidation = Invalidation::find($id)) {

            return Main::response(true, 'OK', $invalidation);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}