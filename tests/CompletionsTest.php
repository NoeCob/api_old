<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CompletionsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testCompletionIndex()
    {
        $this->get('/completions')
            ->assertResponseStatus(200);
    }

    public function testCompletionShow()
    {
        $completion = $this->getObjectRandom(\App\Completion::class);
        $this->get('/completions/' . $completion->id_completions)
            ->assertResponseStatus(200);
    }

}
