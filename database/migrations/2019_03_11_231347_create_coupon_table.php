<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('coupon')) {
            return;
        }

        Schema::create('coupon', function (Blueprint $table) {
            $table->increments('id_coupon');
            $table->string('email', 100)->nullable(false);
            $table->string('token', 50)->nullable(false);
            $table->unsignedTinyInteger('level')->nullable(false);
            $table->string('reward', 100)->nullable(false);
            $table->integer('user')->nullable(false)->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
