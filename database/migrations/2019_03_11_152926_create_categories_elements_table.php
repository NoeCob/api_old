<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('categories_elements')) {
            return;
        }

        Schema::create('categories_elements', function (Blueprint $table) {
            $table->increments('id_categories_elements');
            $table->unsignedInteger('id_categories')->nullable(false);
            $table->string('name')->nullabble(false);
            $table->decimal('price', 10, 2)->nullable(false);
            $table->boolean('active')->default(1);
            $table->string('type', 64);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->foreign('id_categories', 'categories')
                ->references('id_categories')
                ->on('categories')
                ->onDelete('NO ACTION')
                ->onUpdate('NO ACTION');
        });
        DB::statement("ALTER TABLE categories_elements ADD image  MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('categories_elements')) {
            Schema::table('categories_elements', function (Blueprint $table) {
                $table->dropForeign('categories');
            });
            Schema::dropIfExists('categories_elements');
        }

    }
}
