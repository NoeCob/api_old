<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('groups')) {
            return;
        }

        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id_groups');
            $table->string('name', 50)->nullable(false);
            $table->decimal('installation_fee',10,2)->nullable(false);
            $table->decimal('monthly',10,2)->nullable(false);
            $table->unsignedInteger('sign_into')->default(NULL);
            $table->decimal('deposit',10,2)->nullable(false);
            $table->string('plan_id', 64)->nullable(false);
            $table->string('amex_plan_id', 64)->nullable(false);
            $table->integer('trial_days')->nullable(false)->default(0);
            $table->decimal('trial_days_price',10,2)->nullable(false)->default(0.00);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
