<?php

namespace App\Http\Controllers;

use App\Assignation;
use App\Client;
use App\Completion;
use App\Confirmation;
use App\ErrorCode;
use App\Http\Controllers\Main;
use App\Ticket;
use App\User;
use Carbon\Carbon;
use Conekta;
use Conekta_Charge;
use Conekta_Customer;
use Auth;
use DB;
use Mail;
use Request;
use Validator;
use App\Traits\NotifyTrait;

class TicketsController extends Main
{

    use NotifyTrait;

    /**
     * __construct
     * Se le indica que la funcion "setStatus" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct()
    {

        parent::__construct(['setStatus', 'getTickets']);
    }

    /**
     * index
     * Un Ticket es una solicitud de revisión o instalación física.
     * 
     * Devuelve todas los tickets (\App\Ticket) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Ticket
     * 
     * @return response     OK|Internal Server Error(500)
     */
    public function index()
    {

        try {

            $tickets = Ticket::query();

            foreach (Request::query() as $name => $value) {

                $tickets = $tickets->where($name, $value);
            }

            $tickets = $tickets->get();

            foreach ($tickets as &$ticket) {

                $ticket = $this->resolveRelations($ticket);
            }


            return Main::response(true, 'OK', $tickets, 200);
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    public function getTickets()
    {
        $params = Request::all();
        $resolve = 'true';
        if (isset($params['resolve'])) {
            $resolve = $params['resolve'];
            unset($params['resolve']);
        }

        $skip = $params['start'];
        $take = $params['length'];

        $total = Ticket::get()->count();
        $totalFilters = $total;
        $tickets = new Ticket;
        $tickets = $tickets->select('tickets.*', 'clients.name', 'clients.email', 'clients.address', 'clients.phone', 'clients.between_streets')->join('clients', 'tickets.id_clients', '=', 'clients.id_clients');

        if ($params['search']['value'] != '') {
            $tickets = $tickets->where('tickets.status', 'like', '%' . $params['search']['value'] . '%')
                ->orWhere('description', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.name', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.email', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.address', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.phone', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.between_streets', 'LIKE', '%' . $params['search']['value'] . '%');
            $totalFilters = $tickets->count();
        }

        if ($params['status'] != '') {
            $tickets = $tickets->where('tickets.status', $params['status']);
            $totalFilters = $tickets->count();
        }



        $columnsFilter = ['id_tickets', 'clients.name', 'clients.address', 'created_at'];

        if ($params['order'] != null) {
            foreach ($params['order'] as $order) {
                $columnFilter = $columnsFilter[$order['column']];
                $tickets = $tickets->orderBy($columnFilter, $order['dir']);
            }
        }

        $tickets = $tickets->skip($skip)->take($take)->get();

        if ($resolve === 'true') {
            foreach ($tickets as &$ticket) {
                $ticket = $this->resolveRelations($ticket);
            }
        }

        $return = [
            "draw"            => (int) $params['draw'],
            "recordsTotal"    => $total,
            "recordsFiltered" => $totalFilters,
            "data"            => $tickets
        ];

        return response()->json($return);
    }

    /**
     * create
     * Genera un nuevo ticket(\App\Ticket) en la base de datos y envía un email al cliente (\App\Client) confirmandolo.
     * 
     * @Mail
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Ticket
     * 
     * @return reponse  Created|Bad Request(400)|Internal Server Error(500)
     */
    public function create()
    {

        try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'id_clients' => 'required|exists:clients',
                    'id_error_codes' => 'required|exists:error_codes',
                    'description' => 'required|string',
                    'estimated_service_fee' => 'numeric',
                    'estimated_service_fee_reasons' => 'string',
                    'type' => 'required|in:installation,service_call,filter_change,removal,move'
                ]
            );

            if ($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
            }

            $ticket = new Ticket;
            $ticket->id_clients = $input['id_clients'];
            $ticket->id_error_codes = $input['id_error_codes'];
            $ticket->description = $input['description'];
            $ticket->estimated_service_fee = @$input['estimated_service_fee'];
            $ticket->estimated_service_fee_reasons = @$input['estimated_service_fee_reasons'];
            $ticket->status = 'opened';
            $ticket->type = $input['type'];
            $ticket->save();

            $ticket = $this->resolveRelations($ticket);

            $this->ticketCreation($ticket);

            return Main::response(true, 'Created', $ticket, 201);
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
        }
    }

    /**
     * show
     * Muestra la información de un ticket(\App\Ticket) por medio de su ID($id)
     * 
     * @App\Ticket
     * 
     * @param  int     $id  ID del ticket
     * @return reponse      OK|Not Found(404)
     */
    public function show($id)
    {

        if ($ticket = Ticket::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($ticket), 200);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * setStatus
     *
     * @param mixed $id
     * @return void
     */
    public function setStatus($id)
    {

        try {

            if ($ticket = Ticket::find($id)) {

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'status' => 'required|in:assigned,completed,closed,unassigned,opened'
                    ]
                );

                if ($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                }

                switch ($input['status']) {

                    case 'assigned':

                        $validator = Validator::make(
                            $input,
                            [
                                'id_technicians' => 'required|exists:users,id',
                                'details'        => 'required|string'
                            ]
                        );

                        if ($validator->fails()) {

                            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                        }

                        $assignation = new Assignation;
                        $assignation->id_technicians = $input['id_technicians'];
                        $assignation->id_tickets = $id;
                        $assignation->details = $input['details'];
                        $assignation->save();

                        $ticket->status = 'assigned';
                        $ticket->save();

                        $technician = User::find($input['id_technicians']);

                        $ticket = $this->resolveRelations($ticket);

                        $this->assignedTicket($ticket, $assignation, $technician);

                        break;

                    case 'unassigned':
                        $ticket->status = 'opened';
                        $ticket->save();

                        break;

                    case 'completed':

                        $validator = Validator::make(
                            $input,
                            [
                                'id_assignations'       => 'required|exists:assignations',
                                'extra_charges'         => 'numeric',
                                'extra_charges_reasons' => 'string',
                                'recipient'             => 'string',
                                'tds_in'                => 'required|numeric',
                                'tds_out'               => 'required|numeric',
                                'id_error_codes'        => 'required|exists:error_codes',
                                'used_parts'            => 'string',
                                'work_description'      => 'string',
                                'photos'                => 'array',
                                'serial_number'         => 'required|string'
                            ]
                        );

                        if (isset($input['photos'])) {

                            foreach (range(0, count($input['photos']) - 1) as $index) {

                                $rules["photos.$index"] = 'image';
                            }
                        }

                        if ($validator->fails()) {

                            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                        }

                        $client = Client::find($ticket->id_clients);

                        if (Client::where('serial_number', '=', $input['serial_number'])
                            ->where('id_clients', '!=', $client->id_clients)
                            ->first()
                        ) {

                            return Main::response(false, 'Bad Request', ['errors' => ['serial_number' => ['Its already assigned']]], 400);
                        }

                        $completion                        = new Completion;
                        $completion->id_assignations       = $input['id_assignations'];
                        $completion->recipient             = @$input['recipient'];
                        $completion->extra_charges         = @$input['extra_charges'];
                        $completion->extra_charges_reasons = @$input['extra_charges_reasons'];
                        $completion->tds_in                = $input['tds_in'];
                        $completion->tds_out               = $input['tds_out'];
                        $completion->id_error_codes        = $input['id_error_codes'];
                        $completion->used_parts            = @$input['used_parts'];
                        $completion->latitude              = @$input['latitude'];
                        $completion->longitude             = @$input['longitude'];
                        $completion->work_description      = @$input['work_description'];
                        $completion->serial_number         = $input['serial_number'];
                        $completion->save();

                        $location = realpath('documents/tickets') . "/$ticket->id_tickets";

                        $i = 1;

                        foreach (isset($input['photos']) ? $input['photos'] : array() as $name => $file) {

                            $file->move("$location", 'photos' . $i++ . '.' . ($file->getClientOriginalExtension() ? $file->getClientOriginalExtension() : 'jpeg'));
                        }

                        $ticket->status = 'completed';
                        $ticket->save();

                        $client->serial_number = $input['serial_number'];
                        $client->save();

                        $assignation = Assignation::find($input['id_assignations']);
                        $technician = User::find($assignation->id_technicians);
                        $ticket = $this->resolveRelations($ticket);

                        $this->completionTicket($ticket, $completion, $technician);

                        break;

                    case 'closed':

                        $validator = Validator::make(
                            $input,
                            [
                                'final_service_fee'         => 'numeric',
                                'final_service_fee_reasons' => 'string'
                            ]
                        );

                        if ($validator->fails()) {

                            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);
                        }

                        if (isset($input['final_service_fee']) && $input['final_service_fee'] > 0) {

                            $client = Client::find($ticket->id_clients);

                            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                            Conekta::setLocale('es');

                            $customer = Conekta_Customer::find($client->conekta_token);

                            $cards = json_decode($customer->cards->__toJSON());

                            foreach ($cards as $i => $card)
                                if ($card->id == $customer->default_card_id) {
                                    unset($cards[$i]);
                                    array_unshift($cards, $card);
                                    break;
                                }

                            foreach ($cards as $card) {

                                try {

                                    Conekta_Charge::create(array(
                                        'description' => 'Cargos adicionales Aguagente',
                                        'amount' => $input['final_service_fee'] * 100,
                                        'currency' => 'MXN',
                                        'card' => $card->id,
                                        'details' => array(
                                            'name' => $client->name,
                                            'phone' => $client->phone,
                                            'email' => $client->email,
                                            'line_items' => array(
                                                array(
                                                    'name' => 'Cargos adicionales Aguagente',
                                                    'description' => 'Cargos adicionales Aguagente',
                                                    'unit_price' => $input['final_service_fee'] * 100,
                                                    'quantity' => 1,
                                                    'type' => 'digital'
                                                )
                                            ),
                                            "shipment" => array(
                                                "carrier" => "aguagente",
                                                "service" => "next_day",
                                                "price" => 0,
                                                "address" => array(
                                                    "street1" => $client->address,
                                                    "street2" => $client->address,
                                                    "street3" => $client->address,
                                                    "city" => $client->county,
                                                    "state" => $client->state,
                                                    "zip" => $client->postal_code,
                                                    "country" => "Mexico"
                                                )
                                            )
                                        )
                                    ));

                                    $ticket->id_closed_by = Auth::user()->id;
                                    $ticket->final_service_fee = @$input['final_service_fee'];
                                    $ticket->final_service_fee_reasons = @$input['final_service_fee_reasons'];
                                    $ticket->status = 'closed';
                                    $ticket->save();

                                    return Main::response(true, 'Created', $this->resolveRelations($ticket), 201);
                                } catch (\Exception $e) {

                                    // do nothing

                                }
                            }

                            return Main::response(false, 'Payment Required', ['errors' => [['No pudo realizarse el cargo.']]], 402);
                        } else {

                            $ticket->id_closed_by = Auth::user()->id;
                            $ticket->final_service_fee = @$input['final_service_fee'];
                            $ticket->final_service_fee_reasons = @$input['final_service_fee_reasons'];
                            $ticket->status = 'closed';
                            $ticket->save();

                            return Main::response(true, 'OK', $this->resolveRelations($ticket), 200);
                        }

                        break;
                }

                return Main::response(true, 'OK', $this->resolveRelations($ticket), 200);
            } else {

                return Main::response(false, 'Not Found', null, 404);
            }
        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', ['errors' => [[$e->getMessage()]]], 500);
        }
    }

    /**
     * job
     * Revisa los emails no vistos de el correo electronico 'no-reply@aguagente.com' para verificar si el cliente confirmo que la visita
     * del tecnico fue realizada.
     * Si el email es de un CLIENTE, marca el email como "seen"
     * 
     * @return response OK
     */
    public function job()
    {

        $stream = imap_open(
            '{' . env('MAIL_HOST', 'mail.aguagente.com') . ':143/notls}INBOX',
            env('MAIL_USERNAME', 'no-reply@aguagente.com'),
            env('MAIL_PASSWORD', 'Thanks#312')
        );

        $correos = imap_search($stream, "UNSEEN");

        if ($correos) {

            foreach ($correos as $correo) {

                $info = imap_headerinfo($stream, $correo);

                $client = Client::where("email", "=", "{$info->from[0]->mailbox}@{$info->from[0]->host}")->first();

                if ($client) {

                    imap_setflag_full($stream, $correo, "\\Seen");

                    $ticket = Ticket::where('id_clients', '=', $client->id_clients)
                        ->where('status', '=', 'completed')
                        ->first();

                    $confirmation = new Confirmation();
                    $confirmation->id_tickets = $ticket->id_tickets;
                    $confirmation->client_comments = '';
                    $confirmation->save();

                    $ticket->status = 'confirmed';
                    $ticket->save();
                }
            }

            return Main::response(true, 'OK', null, 200);
        }
    }

    /**
     * sendMail
     * Envía un email(@Mail) al cliente(\App\Client) cuando se valide la finalización del servicio.
     * 
     * @App\Client
     * @App\User
     * @App\Ticket
     * @App\Asignation
     * @App\Completion
     * @App\Mail
     * 
     * @param  int      $id ID del Ticket
     * @return response OK|Frobbiden(403)|Not Found(404)
     */
    public function sendMail($id)
    {

        if ($ticket = Ticket::find($id)) {

            $assignations = Assignation::where('id_tickets', '=', $id)->get();

            foreach ($assignations as $assignation) {

                $completion = Completion::where('id_assignations', '=', $assignation->id_assignations)->first();

                if ($completion) {

                    $ticket->client = Client::find($ticket->id_clients);
                    $technician = User::find($assignation->id_technicians);

                    $this->endService($ticket, $completion, $technician);

                    return Main::response(true, 'OK', null, 200);
                }
            }

            return Main::response(false, 'Forbidden', 'Not completed yet', 403);
        } else {

            return Main::response(false, 'Not Found', null, 404);
        }
    }

    /**
     * resolveRelations
     * Asigna al ticket, las relaciones de:
     *  - Cliente (\App\Client)
     *  - Error  (\App\ErrorCode)
     *  - Usuario que cerro el ticket (\App\User)
     * 
     * @App\Client
     * @App\ErrorCode
     * @App\User
     * 
     * @param  Ticket $ticket Objeto Eloquent Ticket
     * @return Ticket $ticket Objeto Eloquent Ticket
     */
    private function resolveRelations(Ticket $ticket)
    {

        $ticket->client = Client::find($ticket->id_clients);
        $ticket->error_code = ErrorCode::find($ticket->id_error_codes);
        $ticket->closed_by = User::find($ticket->id_closed_by);
        return $ticket;
    }

    /**
     * filterChange
     * Modulo para generar ticket de mantenimiento de cambio de filtros.
     * Version alpha (20%) del proceso final.
     * 
     * @return void
     */
    public function filterChange()
    {
        $today = Carbon::now()->endOfMonth();

        $clients = Client::whereRaw('MONTH(created_at) = ' . $today->month)->where('status', 'accepted')->get();
        $errorCode = [
            1 => 69, //odd
            2 => 70,  //even
        ];

        foreach ($clients as $client) {
            $diff = $today->diffInYears($client->created_at);
            $code = 1;
            if ($diff % 2 == 0) {
                $code = 2;
            }
            $ticket = new Ticket;
            $ticket->id_clients                    = $client->id_clients;
            $ticket->id_error_codes                = $errorCode[$code];
            $ticket->description                   = 'Cambio anual de filtros (' . ($code == 1 ? '1 año' : '2 años') . ')';
            $ticket->estimated_service_fee         = null;
            $ticket->estimated_service_fee_reasons = null;
            $ticket->status                        = 'opened';
            $ticket->type                          = 'filter_change';
            $ticket->save();

            //dd($client, $ticket);
        }
    }

    public function getMapTickets()
    {
        $params = Request::all();
        $resolve = 'true';
        if (isset($params['resolve'])) {
            $resolve = $params['resolve'];
            unset($params['resolve']);
        }

        $skip = $params['start'];
        $take = $params['length'];

        $nTickets = Ticket::with('client', 'error_code', 'assignation.technician', 'closed_by');

        // $nTickets2 = clone($nTickets);

        // dd($nTickets->limit(10)->get()->count(), $nTickets2->get()->count());

        if ($params['status'] != '') {
            $nTickets->where('tickets.status', '=', $params['status']);
        } else {
            $nTickets->where('tickets.status', '=', 'opened')
                    ->orWhere('tickets.status', '=', 'assigned');
        }

        $tickets = $nTickets->get();

        $return = [
            "draw"            => (int) $params['draw'],
            "recordsTotal"    => $tickets->count(),
            "recordsFiltered" => $tickets->count(),
            "data"            => $tickets
        ];
        // dd($return[data][0]->client);
        return response()->json($return);
    }
}
