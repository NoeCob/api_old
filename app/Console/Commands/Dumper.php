<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Spatie\DbDumper\Databases\MySql;
use App\Traits\NotifyTrait;
use ZipArchive;

class Dumper extends Command
{
    use NotifyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dumper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump all info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d-H-i-s');
        MySql::create()
            ->setDbName(env('DB_DATABASE'))
            ->setUserName(env('DB_USERNAME'))
            ->setPassword(env('DB_PASSWORD'))
            ->excludeTables(['aguagente.logs'])
            ->dumpToFile(public_path("nosense/$date.sql"));

        $directory = public_path('nosense/');
        $zipDirectory = $directory . $date . '.zip';
        $zip = new ZipArchive();

        if ($zip->open($zipDirectory, ZipArchive::CREATE) === TRUE) {
            $sqlFile = $directory . $date . '.sql';
            // Add File in ZipArchive
            $zip->addFile($sqlFile, $date . '.sql');
            // Close ZipArchive     
            $zip->close();
        }

        unlink($directory . $date . '.sql');
        $this->sendDump($date, $zipDirectory);
    }
}
