<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class Cors implements Middleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        return $next($request)
            ->header('Access-Control-Allow-Origin', $request->header('ORIGIN') ? $request->header('ORIGIN') : '*')
            //->header('Access-Control-Allow-Origin', '*')
			->header('Access-Control-Allow-Credentials' , 'true')
            ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With,  X-CSRF-TOKEN, Application, Cache-Control, Access-Control-Allow-Origin');

        /*$response = $next($request);

        $response->headers->set('Access-Control-Allow-Origin' , $request->header('ORIGIN') ? $request->header('ORIGIN') : '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');

        return $response;*/
    }
}
