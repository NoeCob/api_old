<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('subscription_events')) {
            return;
        }

        Schema::create('subscription_events', function (Blueprint $table) {
            $table->increments('id_subscription_events');
            $table->integer('id_clients')->nullable(false);
            $table->char('customer_id', 32)->nullable(false);
            $table->string('type', 32)->nullable(false);
            $table->enum('subscription_status', ['in_trial', 'active', 'paused', 'canceled', 'past_due'])
                ->nullable(false);
            $table->dateTime('date')->nullable(false);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->index('id_clients', 'id_clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_events');
    }
}
