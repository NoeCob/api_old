<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testSubscriptionEvents()
    {
        $this->get('/subscription-events')
            ->assertResponseStatus(200);
    }

    public function testFinanceClients()
    {
        $this->get('/finance-clients')
            ->assertResponseStatus(200);
    }

    public function testNotifications()
    {
        print_r($this->get('/notifications')->response->content());
    }

    public function testNotificationsReport()
    {
        $this->get('/notifications/report')
            ->assertResponseStatus(200);
    }

}
