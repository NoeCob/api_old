<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('charges')) {
            return;
        }

        Schema::create('charges', function (Blueprint $table) {
            $table->char('id_charges', 32)->nullable(false);
            $table->integer('id_clients')->nullable(false);
            $table->char('customer_id', 32)->nullable(false);
            $table->string('description', 128)->nullable(false);
            $table->decimal('amount', 10, 2)->nullable(false);
            $table->decimal('fee', 10, 2)->nullable(false)->default(0.00);
            $table->timestamp('paid_at')->default(NULL);
            $table->string('failure_message', 256)->default(NULL);
            $table->text('extra_data');
            $table->enum('invoice_status', ['missing', 'invoiced', 'failed'])->nullable(false);
            $table->integer('id_invoices')->default(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->index('id_clients');
            $table->primary('id_charges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('charges');


    }
}
