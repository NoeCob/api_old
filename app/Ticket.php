<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{

    protected $table = 'tickets';
    protected $primaryKey = 'id_tickets';

    public function client()
    {
        return $this->belongsTo(Client::class, 'id_clients');
    }

    public function error_code()
    {
        return $this->belongsTo(ErrorCode::class, 'id_error_codes');
    }

    public function closed_by()
    {
        return $this->belongsTo(User::class, 'id_closed_by');
    }

    public function assignation()
    {
        return $this->hasOne(Assignation::class,'id_tickets');
    }

    public function save(array $options = [])
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Saving/Updating Ticket' . $this->original['id_tickets'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        parent::save($options);
        $log->table_id = $this->original['id_tickets'];
        $log->save();
    }

    public function delete()
    {
        $log = new Log();
        $log->before_data = json_encode($this->original);
        $log->after_data = json_encode($this->attributes);
        $log->model_name = get_class($this);
        $user = Auth::user();
        $log->user_id = !empty($user) ? $user->id : 0;
        $log->message = 'Deleting Ticket: ' . $this->original['id_tickets'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
            $_SERVER['REQUEST_URI'];
        $log->route = $link;
        $log->request_type = $_SERVER['REQUEST_METHOD'];
        $log->table_id = $this->original['id_tickets'];
        $log->save();
        return parent::delete();
    }
}
