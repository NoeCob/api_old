@extends('layouts.email')

@section('name')
{{ $name }}
@endsection


@section('content')
Te informamos que tu contrato ha sido {{$status}}:
<br/>
<strong>{{ $reasons }}</strong>
<br/>
Gracias y disfruta de Aguagente!
@endsection