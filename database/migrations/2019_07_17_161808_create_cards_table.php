<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Faker\Provider\es_ES\Address;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id_cards');
            $table->unsignedInteger('id_clients')->nullable(false);
            $table->text('address')->nullable(false);
            $table->text('outdoor_number')->nullable(true);
            $table->text('inside_number')->nullable(true);
            $table->string('phone',13)->nullable(false);
            $table->text('colony')->nullable(false);
            $table->integer('postal_code')->length(5)->nullable(false);
            $table->string('county',50)->nullable(false);
            $table->string('state',30)->nullable(false);
            $table->string('country',50)->nullable(false);
            $table->integer('card_number')->nullable(true);
            $table->integer('exp_month')->length(2)->nullable(true);
            $table->integer('exp_year')->length(2)->nullable(true);
            $table->integer('default_card')->length(4)->nulleable(false)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards');
    }
}
