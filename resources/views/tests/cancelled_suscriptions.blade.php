@extends('layouts.base')

@section('title', 'Lista de clientes sin suscripcion')

@section('content')

    <div class="well">
        <h4>Lista de Clientes sin suscripción<br><small>Que no están en en nuevo equema de cobros</small></h4>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Nivel</th>
                <th>Estatus de Cliente</th>
                <th>Estatus de Suscripcion</th>
                <th>Menusalidad</th>
                <th>¿Resp. Social?</th>
                <th>Deuda</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
                <tr @if($client[status] == 'canceled') class="danger" @endif>
                    <td>{{$client[id_clients]}}</td>
                    <td>{{$client[name]}}</td>
                    <td>{{$client[level]}}</td>
                    <td><strong>{{$client[status]}}</strong></td>
                    <td>{{$client[subscription_status]}}</td>
                    <td>{{$client[monthly_fee] / 100}}</td>
                    <td>{{$client[social_responsability] ? 'SI':'NO'}}</td>
                    <td>{{number_format($client[debt] / 100,2)}}</td>
                </tr>
                <tr @if($client[status] == 'canceled') class="danger" @endif>
                    <td colspan="7">
                        <div style="padding:0 20px;">
                            <h4>Últimos 5 Pagos exitosos</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="col-xs-3">Fecha</th>
                                        <th class="col-xs-3">Descripcion</th>
                                        <th class="col-xs-3">Monto</th>
                                        <th class="col-xs-3">Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($client[paidCharges] as $paidCharge)
                                        <tr>
                                            <td>{{ $paidCharge[paid_at] }}</td>
                                            <td>{{ $paidCharge[description] }}</td>
                                            <td>{{ $paidCharge[amount] / 100 }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div style="padding:0 20px;">
                            <h4>Últimos 3 Pagos fallidos</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="col-xs-3">Fecha</th>
                                        <th class="col-xs-3">Descripcion</th>
                                        <th class="col-xs-3">Monto</th>
                                        <th class="col-xs-3">Razón</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($client[unpaidCharges] as $unpaidCharges)
                                        <tr>
                                            <td>{{ $unpaidCharges[created_at] }}</td>
                                            <td>{{ $unpaidCharges[description] }}</td>
                                            <td>{{ $unpaidCharges[amount] / 100 }}</td>
                                            <td>{{ $unpaidCharges[failure_message] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection