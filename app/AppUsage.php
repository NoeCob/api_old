<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AppUsage extends Model
{
    protected $table = 'app_usage';

    public function client() {
        return $this->belongsTo('App\Client', 'id_clients');
    }

}
