<?php

namespace App\Http\Controllers;

use App\ErrorCode;
use App\Http\Controllers\Main;
use DB;
use Request;
use Validator;

class ErrorCodesController extends Main {

    /**
     * __construct
     * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct() {

        parent::__construct(['index']);

    }
    
    /**
     * index
     * Devuelve todas los codigos de error (\App\ErrorCode) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\ErrorCode
     * 
     * @return response Ok|Internal Server Error
     */
    public function index() {
        
        try {

            $errorCodes = ErrorCode::query();

            foreach(Request::query() as $name => $value) {
                
                $errorCodes = $errorCodes->where($name, $value);

            }

            $errorCodes = $errorCodes->where('active', '=', 1);

            return Main::response(true, 'OK', $errorCodes->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }
        
    }

    /**
     * create
     * Crea un nuevo codigo de error(\App\ErrorCode).
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\ErrorCode
     * 
     * @return response     Created|Bad Request|Internal Server Error
     */
    public function create() {

        try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'name' => 'required|string',
                    'code' => 'required|integer',
                    'category' => 'required|string'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            $errorCode = new ErrorCode;
            $errorCode->name = $input['name'];
            $errorCode->code = $input['code'];
            $errorCode->category = $input['category'];
            $errorCode->active = 1;
            $errorCode->save();

            return Main::response(true, 'Created', $errorCode, 201);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
            
        }

    }

    /**
     * show
     * Muestra una codigo de error (\App\ErrorCode) por medio del ID
     *
     * @\App\ErrorCode
     * 
     * @param  int      $id ID del codigo de error
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        $errorCode = ErrorCode::find($id);

        if($errorCode && $errorCode->active == 1) {

            return Main::response(true, 'OK', $errorCode, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * update
     * Guarda las modificaciones realizadas a un codigo de error (\App\ErrorCode) por medio del ID
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\ErrorCode
     * 
     * @param  int      $id ID del codigo de error
     * @return response     Created|Bad Request|Internal Server Error
     */
    public function update($id) {

        $errorCode = ErrorCode::find($id);
        
        if($errorCode && $errorCode->active == 1) {
            
            try {

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'name' => 'required|string',
                        'code' => 'required|integer',
                        'category' => 'required|string'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                $errorCode->name = $input['name']; 
                $errorCode->code = $input['code'];
                $errorCode->category = $input['category'];   
                $errorCode->save();

                return Main::response(true, 'OK', $errorCode, 200);

            } catch(\Exception $e) {

                return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);
                
            }

        } else {

            return Main::response(false, 'Not Found', null, 400);

        }

    }

    /**
     * destroy
     * Desactiva un codigo de error(\App\ErrorCode) por medio de un ID
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\ErrorCode
     * 
     * @param  int      $id ID del codigo de error
     * @return response     OK|Not Found(404)
     */
    public function destroy($id) {

        $errorCode = ErrorCode::find($id);
        
        if($errorCode && $errorCode->active == 1) {
            
            $errorCode->active = 0;
            $errorCode->save();

            return Main::response(true, 'OK', null, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
