<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Client;

class CleanResidualDebt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:clear-residual-debt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean the residual DEBT for the clients.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Obtiene todos los Sales agents y calcula las comisiones del mes en curso.
     *
     * @return void
     */
    public function handle()
    {
        $clients = Client::whereBetween('debt', [0.01, 50])->get();
        foreach($clients as $client) {
            $this->info($client->name.' '.$client->debt);
            $client->debt = 0;
            $client->save();
            $this->info($client->name.' '.$client->debt);
        }
    }
}