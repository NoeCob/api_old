<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
	</head>
	<body>
		<div class="opps" style="width: 496px; border-radius: 4px;	box-sizing: border-box;	padding: 0 45px;	margin: 40px auto;	overflow: hidden;	border: 1px solid #b0afb5;	font-family: 'Open Sans', sans-serif;color: #4f5365;">
			<div class="opps-header">
				<div class="opps-reminder" style="position: relative;	top: -1px;	padding: 9px 0 10px;	font-size: 11px;	text-transform: uppercase;	text-align: center;	color: #ffffff;background: #000000;">Ficha digital. No es necesario imprimir.</div>
				<div class="opps-info" style="margin-top: 26px;position: relative;">
					<div class="opps-brand" style="width: 45%;float: left;">
						<img src="https://panelaguagente.xyz/api/public/img/oxxopay_brand.png" alt="OXXOPay" style="max-width: 150px;margin-top: 2px;">
						<h3>Hola <?php echo $nombre?></h3>
                    </div>
					<div class="opps-ammount" style="width: 55%;float: right;">
						<h3 style="margin-bottom: 10px;	font-size: 15px;	font-weight: 600;text-transform: uppercase;">Monto a pagar</h3>
						<h2 style="font-size: 36px;	color: #000000;	line-height: 24px;margin-bottom: 15px;">$ <?php echo number_format($total,2); ?> <sup style="font-size: 16px;	position: relative;top: -2px">MXN</sup></h2>
						<p style="font-size: 10px;line-height: 14px;">OXXO cobrará una comisión adicional al momento de realizar el pago.</p>
					</div>
				</div>
                <div style="visibility: hidden;     display: block;     font-size: 0;     content: ' ';     clear: both;height: 0;"></div>
                <br><br><br><br><br><br>
				<div class="opps-reference" style="margin-top: 14px;">
					<h3 style="margin-bottom: 10px;	font-size: 15px;	font-weight: 600;text-transform: uppercase;">Referencia</h3>
					<h1 style="font-size: 27px;	color: #000000;	text-align: center;	margin-top: -1px;	padding: 6px 0 7px;	border: 1px solid #b0afb5;	border-radius: 4px;background: #f8f9fa;"><?php echo $reference?></h1>
				</div>
			</div>
			<div class="opps-instructions" style="margin: 32px -45px 0;	padding: 32px 45px 45px;	border-top: 1px solid #b0afb5;background: #f8f9fa;">
				<h3 style="margin-bottom: 10px;	font-size: 15px;	font-weight: 600;text-transform: uppercase;">Instrucciones</h3>
				<ol style="margin: 17px 0 0 16px;">
					<li>Acude a la tienda OXXO más cercana. <a href="https://www.google.com.mx/maps/search/oxxo/" target="_blank" style="color: #1155cc;">Encuéntrala aquí</a>.</li>
					<li>Indica en caja que quieres realizar un pago de <strong>OXXOPay</strong>.</li>
					<li>Dicta al cajero el número de referencia en esta ficha para que tecleé directamete en la pantalla de venta.</li>
					<li>Realiza el pago correspondiente con dinero en efectivo.</li>
					<li>Al confirmar tu pago, el cajero te entregará un comprobante impreso. <strong>En el podrás verificar que se haya realizado correctamente.</strong> Conserva este comprobante de pago.</li>
				</ol>
				<div class="opps-footnote" style="margin-top: 22px;	padding: 22px 20 24px;	color: #108f30;	text-align: center;	border: 1px solid #108f30;	border-radius: 4px;background: #ffffff;">Al completar estos pasos recibirás un correo confirmando tu pago.</div>
			</div>
		</div>	
	</body>
</html>