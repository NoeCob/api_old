<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RejectionsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testRejectionIndex()
    {
        $this->get('/rejections')
            ->assertResponseStatus(200);
    }

    public function testRejectionShow()
    {
        $rejection = $this->getObjectRandom(\App\Rejection::class);
        $this->get('/rejections/' . $rejection->id_rejections)
            ->assertResponseStatus(200);
    }

}
