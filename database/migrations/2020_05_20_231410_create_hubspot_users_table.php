<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubspotUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hubspot_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vid', 250)->nullable(false)->unique();
            $table->string('facebook_user_id', 250)->nullable(true)->unique();
            $table->string('email', 250)->nullable(true)->unique();
            $table->boolean('is_active')->nullable(false)->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hubspot_users');
    }
}
