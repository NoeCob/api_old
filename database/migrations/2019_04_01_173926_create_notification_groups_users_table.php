<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationGroupsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_group_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('notification_group_id')->unsigned();
            $table->foreign('notification_group_id')->references('id_notification_groups')->on('notification_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_group_user', function (Blueprint $table) {
            $table->dropForeign('notification_group_user_user_id_foreign');
            $table->dropForeign('notification_group_user_notification_group_id_foreign');
        });
        Schema::drop('notification_group_user');
    }
}
