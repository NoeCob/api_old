@extends('layouts.base')

@section('title', 'Lista de Clientes con más de un cobro')

@section('content')
    <div class="well">
        <h4>Lista de Clientes con más de un cobro en el mes: <strong class="text-warning">MARZO</strong><br><small>Que no están en en nuevo equema de cobros</small></h4>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Nivel</th>
                <th>Estatus de Cliente</th>
                <th>Estatus de Suscripcion</th>
                <th>Menusalidad</th>
                <th>¿Resp. Social?</th>
                <th>Deuda</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client[id_clients]}}</td>
                    <td>{{$client[name]}}</td>
                    <td>{{$client[level]}}</td>
                    <td><strong>{{$client[status]}}</strong></td>
                    <td>{{$client[subscription_status]}}</td>
                    <td>{{$client[monthly_fee] / 100}}</td>
                    <td>{{$client[social_responsability] ? 'SI':'NO'}}</td>
                    <td>{{number_format($client[debt] / 100,2)}}</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div style="padding:0 20px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" id="c_{{$client[id_clients]}}">
                                    <h3 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#collapse{{$client[id_clients]}}" aria-expanded="true" aria-controls="collapse{{$client[id_clients]}}">
                                            Págos registrados (+)
                                        </a>
                                    </h3>
                                </div>
                                <div id="collapse{{$client[id_clients]}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$client[id_clients]}}">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-3">CONEKTA ID</th>
                                                    <th class="col-xs-3">Fecha</th>
                                                    <th class="col-xs-3">Descripcion</th>
                                                    <th class="col-xs-2">Monto</th>
                                                    <th class="col-xs-4"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client[paidCharges] as $paidCharge)
                                                    <tr @if (strpos($paidCharge[description], 'Aguagente') !== false) class="warning" @endif>
                                                        <td>
                                                            @if ($paidCharge[debt])
                                                                <a href="#" class="openModal" data-info="{{json_encode($paidCharge[debt])}}">{{ $paidCharge[id_charges] }}</a>
                                                            @else
                                                                {{ $paidCharge[id_charges] }}
                                                            @endif
                                                        </td>
                                                        <td>{{ $paidCharge[paid_at] }}</td>
                                                        <td>{{ $paidCharge[description] }}</td>
                                                        <td>{{ $paidCharge[amount] / 100 }}</td>
                                                        <td>
                                                            @if (strpos($paidCharge[description], 'Aguagente') !== false)
                                                                Creado por nuevo sistema de pago
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

<script>
jQuery(function($){
    $('.openModal').on('click', function(e){
        e.preventDefault();
        var _this = $(this),
            _data = _this.data(),
            _monto = parseFloat(_data.info.amount+_data.info.collection_fees+_data.info.moratory_fees),
            _modal = {
                title: 'Cargo: '+_data.info.id_charges,
                message:'<h4>Deuda en ERP</h4>'+
                        '<table class="table">'+
                            '<tr><th>Deuda ID</th> <th>Monto</th> <th>Fecha</th></tr>'+
                            '<tr><td>'+_data.info.id_debts+'</td> <td>$ '+(_monto) / 100+'</td> <td>'+_data.info.created_at+'</td></tr>'+
                        '</table>',
                buttons: {
                    ok: {
                        label: "Cerrar",
                        className: 'btn-primary'
                    }
                }
            };

        bootbox.dialog(_modal);
    });
});
</script>
@endsection