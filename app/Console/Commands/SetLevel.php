<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Client;
use App\Level;
use Illuminate\Support\Facades\DB;

class SetLevel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:set-level';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Establece el nivel de cada usuario en el sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clients = Client::where('sales_agent', '=', '1')->get();
        $this->orderClientsTree($clients);
    }

    private function orderClientsTree($clients)
    {
        foreach ($clients as $client) {
            if (count($client->refereds) > 0) {
                $this->orderClientsTree($client->refereds);
                $this->setLevel($client);
            } else { 
                $this->setLevel($client);
            }
        }
    }

    private function setLevel($client)
    {
        $payingClients  = $this->getPayingClients($client->id_clients);
        $nPayingClients = count($payingClients);
        $nFreeWater     = 0;

        foreach ($payingClients as $payingClient) {
            if ($payingClient->level !== Level::VIAJERO) {
                $nFreeWater++;
            }
        }

        if ($nPayingClients >= 16 && $nFreeWater >= 15) {
            $client->level = Level::ALMIRANTE;
        } elseif ($nPayingClients >= 16 && $nFreeWater >= 10) {
            $client->level = Level::CAPITAN;
            $this->info($client->name . ' es Capitan');
        } elseif ($nPayingClients >= 16) {
            $client->level = Level::CABO;
            $this->info($client->name . ' es Cabo');
        } elseif ($nPayingClients >= 6) {
            $client->level = Level::MARINERO;
        } else {
            $client->level = Level::VIAJERO;
        }
        
        $client->save();
    }


    private function getPayingClients($id_clients)
    {

        return DB::table('clients')
            ->whereRaw("clients.referred_by = ? && ((clients.level <> 'VIAJERO') OR ((clients.status = 'accepted') && (clients.debt = 0)))", array($id_clients))
            ->get();
    }


}
