<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PollsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Polls index test
     *
     * @return void
     */
    public function testPollsIndex()
    {
        $this->get('/polls')
            ->assertResponseStatus(200);
    }

    public function testPollsCreate()
    {
        $params = $this->getTestData('/data/polls/data.json');
        $this->post('/polls', $params)
            ->assertResponseStatus(201);
    }

    public function testPollsUpdate()
    {
        $params = $this->getTestData('/data/polls/data.json');
        $poll = $this->getObjectRandom(\App\Poll::class);
        $this->put('/polls/' . $poll->id_polls, $params)
            ->assertResponseStatus(200);
    }

    public function testPollsShow()
    {
        $poll = $this->getObjectRandom(\App\Poll::class);
        $this->get('/polls/' . $poll->id_polls)
            ->assertResponseStatus(200);
    }

    public function testPollsDestroy()
    {
        $poll = $this->getObjectRandom(\App\Poll::class);
        $this->delete('/polls/' . $poll->id_polls)
            ->assertResponseStatus(200);
    }

    public function testAnswersIndex()
    {
        $this->get('/answers')
            ->assertResponseStatus(200);
    }

    public function testAnswers()
    {
        $this->post('/auth/login', ['email'=> 'kikixtlan2@hotmail.com', 'password' => 'hola.123']);
        $params = $this->getTestData('/data/polls/answers.json');
        $response = $this->post('/polls/5/answer', $params)
            ->assertResponseStatus(200);
    }

}
