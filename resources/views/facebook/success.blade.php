@extends('layouts.base')

@section('title', '¡Gracias por tu registro!')

@section('content')

    <div class="row" style="padding-top:150px;">
        <div class="col-md-6 col-md-offset-3">
            <img class="img-responsive center-block" alt="Aguagente" src="{{asset('/img/logo-aguagente-default.png')}}" style="max-width:200px;">
            <h1 class="text-center" style="color:#666;">AGUAGENTE</h1>
        </div>
    </div>
<script>
jQuery(function($){
    Sweetalert2({
        title: '¡MUCHAS GRACIAS!',
        html: 'Te enviamos un email, con la información de tu registro<br><br>E-mail: <strong>{{$client->email}}</strong> ',
        type: 'success',
        confirmButtonText: 'OK'
    });
});
</script>
@endsection