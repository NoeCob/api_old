<?php

namespace App\Http\Controllers\Regenerate;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Webklex\IMAP\Client as ClientImap;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Conekta_ResourceNotFoundError;
use Conekta_ProcessingError;
use Conekta_Handler;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Main;
use App\Assignation;
use App\Card;
use App\CardsVendors;
use App\Charge;
use App\Client;
use App\Contract;
use App\Debt;
use App\Element;
use App\Extras;
use App\Group;
use App\Permission;
use App\Refund;
use App\Role;
use App\Ticket;
use App\User;
use App\Vendor;

class RegenerateController extends Controller
{
    private $offset = 0;

    public function getOffset() 
    {
        return $this->offset;
    }

    public function setOffset($value) 
    {
        $this->offset = $value;
    }

    /**
     * STEP 1
     * Retrive the data from VENDOR and store in the file system.
     */
    public function grabData(Request $request)
    {
        Conekta::setApiKey('key_yhHjT5xC1Dz2y2gNSx2jJA');
        Conekta::setLocale('es');
        
        // STEP 1.0: get Offline charges
        $oxxoCharges = json_decode(Conekta_Charge::where(['payment_method.service_name' => 'OxxoPay', 'limit' => 100])->__toJSON());
        $speiCharges = json_decode(Conekta_Charge::where(['payment_method.bank' => 'STP', 'limit' => 100])->__toJSON());
        $offlineCharges = array_merge($oxxoCharges, $speiCharges);

        // STEP 1.1: get All Clients
        $clientsOffset = $this->getOffset();
        $all = [];
        $haveClients = true;
        while ($haveClients) {
            $thisOffset = [];
            $clients = json_decode(Conekta_Customer::all(['limit' => 100, 'offset' => $clientsOffset, 'sort'=>'created_at.desc'])->__toJSON());
            if (count($clients) == 0) {
                $haveClients = false;
            }

            foreach($clients as $client) {
                $client->charges = [];

                // STEP 1.2: get Client Charges
                $chargesOffset = 0;
                $haveCharges = true;
                while ($haveCharges) {
                    try {
                        $charges = json_decode(Conekta_Charge::where(['customer_id' => $client->id, 'limit' => 100, 'offset' => $chargesOffset])->__toJSON());
                        if (count($charges) == 0) {
                            $haveCharges = false;
                        }
                        $chargesOffset+=100;
                        foreach($charges as $charge) {
                            array_push($client->charges, $charge);    
                        }
                    } catch (\Exception $e) {
                        $haveCharges = false;
                        array_push($client->charges, ['no charge']);  
                    }
                    
                }

                // STEP 1.3: get Clients Offline Charges (if any)
                $findOfflineCharges = array_filter(
                    $offlineCharges,
                    function($charge) use ($client) {
                        if ($charge->details->email == $client->email) {
                            return $charge;
                        }
                    }
                );

                foreach($findOfflineCharges as $charge) {
                    array_push($client->charges, $charge);    
                }
                array_push($thisOffset, $client);
                array_push($all, $client);
            }
            Storage::disk('nosense')->put('data-'.$clientsOffset.'-'.($clientsOffset + 100).'.json', json_encode($thisOffset));
            $clientsOffset+=100;
        }
        Storage::disk('nosense')->put('all.json', json_encode($all));
    }

    /**
     * STEP 2
     * Create the users that doesnt exist in our database.
     */
    public function createUsers(Request $request, $fileName)
    {
        $clients = json_decode(Storage::disk('nosense')->get($fileName.'.json'));
        $clientsSorted = $this->sortCharges($clients);
        $i = 0;
        foreach ($clientsSorted as $jsonClient) {
            $i++;
            try {
                DB::beginTransaction();
                $haveContract = true;
                $client = Client::where('email', $jsonClient->email)->first();
                
                if (!$client || is_null($client)) {
                    $client = $this->createClient($jsonClient);
                    $haveContract = false;
                }

                $cards = [];
                $cardsVendor = [];
                if ($jsonClient->cards) {
                    foreach($jsonClient->cards as $jsonCard) {
                        $card              = new Card;
                        $card->id_clients  = $client->id_clients;
                        $card->name        = $client->name;
                        $card->address     = $client->address;
                        $card->colony      = $client->colony;
                        $card->postal_code = $client->postal_code;
                        $card->county      = ($client->county != '' ? $client->county : 'N/A');
                        $card->state       = $client->state;
                        $card->country     = 'Mexico';
                        $card->phone       = $client->phone;
                        $card->email       = $client->email;
                        $card->exp_month   = $jsonCard->exp_month;
                        $card->exp_year    = $jsonCard->exp_year;
                        $card->card_number = $jsonCard->last4;
                        $card->save();
                        array_push($cards, $card);

                        $cardVendor              = new CardsVendors;
                        $cardVendor->id_cards    = $card->id_cards;
                        $cardVendor->id_vendor   = 1;
                        $cardVendor->card_token  = $jsonCard->id;
                        $cardVendor->save();
                        array_push($cardsVendor, $cardVendor);
                    }
                }

                if (!$haveContract) {
                    $contract = $this->createContract($client, $jsonClient->charges);
                }
                print_r($i.') '.$jsonClient->email.' Insertado con exito');
                print_r("-----------<br>");
                DB::commit();
            } catch (\Exception $e) {
                print_r($i.') '.$jsonClient->email.' Error');
                print_r($e->getMessage());
                print_r("-----------<br>");
                DB::rollback();
            }
        }
    }

    private function createClient($jsonClient)
    {
        $user                 = User::where('email', '=', $jsonClient->email)->first();
        $referrerGroup        = Group::find('56');
        $password             = rand(100000,         999999);
        $address              = $this->getAddress($jsonClient);
        $firstCollectedCharge = $this->getFirstCollectedCharge($jsonClient->charges);
        $collectedAt          = $this->getCollectedDate($firstCollectedCharge);
        $groupData            = [
            'deposit'               => $this->getChargeLineItem($firstCollectedCharge, 'Depósito'),
            'installation_fee'      => $this->getChargeLineItem($firstCollectedCharge, 'Acero inoxidable'),
            'monthly_fee'           => $this->getMonthlyFee($jsonClient->charges),
            'social_responsability' => $this->getSocialResp($jsonClient->charges)
        ];

        $payday      = (!is_null($collectedAt) ? ($collectedAt->format('d') > 27 ? 27 : $collectedAt->format('d')) : null);
        $next_payday = $this->getNextPayday($jsonClient->charges, $payday);

        if ($user && !is_null($user)) {
            if (!$user->hasRole('Client')) {
                $user->attachRole(Role::where('name', '=', 'Client')->first());
            }
        } else {
            $user           = new User();
            $user->name     = $jsonClient->name;
            $user->email    = $jsonClient->email;
            $user->password = bcrypt($password);
            $user->save();
            $user->attachRole(Role::where('name', '=', 'Client')->first());
        }

        $client                        = new Client;
        $client->id_users              = $user->id;
        $client->id_groups             = $referrerGroup->id_groups;
        $client->name                  = $jsonClient->name;
        $client->email                 = $jsonClient->email;
        $client->phone                 = ($jsonClient->phone != '' ? $jsonClient->phone : 0);
        $client->address               = $address['address'];
        $client->between_streets       = ''; // $jsonClient->between_streets;
        $client->colony                = $address['colony'];
        $client->county                = $address['county'];
        $client->state                 = $address['state'];
        $client->postal_code           = $address['postal_code'];
        $client->referred_by           = 15;

        $client->deposit               = ($groupData['deposit'] ? $groupData['deposit']->unit_price : $referrerGroup->deposit);
        $client->installation_fee      = ($groupData['installation_fee'] ? $groupData['installation_fee']->unit_price : $referrerGroup->installation_fee);
        $client->monthly_fee           = ($groupData['monthly_fee'] ? $groupData['monthly_fee'] : $referrerGroup->monthly_fee);
        $client->social_responsability = $groupData['social_responsability'];

        $client->conekta_token         = $jsonClient->id;
        $client->collected_at          = (!is_null($collectedAt) ? $collectedAt->toDateString() : null);
        $client->status                = $this->alreadyCharged($jsonClient->charges) ? 'accepted':'standby';
        $client->pay_day               = $payday;
        $client->next_payday           = $next_payday;

        $client->monthly_installments  = 1;
        $client->signed_in_id          = $referrerGroup->id_groups;
        $client->signed_in             = $referrerGroup->name;
        $client->signed_in_group       = $referrerGroup;
        $client->save();

        return $client;
    }

    private function createContract($client, $charges)
    {
        $contract             = new Contract;
        $contract->id_clients = $client->id_clients;
        $contract->save();

        $elements    = Element::with('category')->where('active', 1)->get();
        $firstCharge = $this->getFirstCollectedCharge($charges);
        $extras      = [];

        if ($firstCharge) {
            $haveValidElements = false;
            foreach ($firstCharge->details->line_items as $lineItem) {
                $col = $elements->where('name', $lineItem->name);
                if ($col->count() > 0) {
                    $haveValidElements = true;
                    $selectedElem = $col->first();
                    
                    $extra                = new Extras();
                    $extra->id_contracts  = $contract->id_contracts;
                    $extra->category_name = $selectedElem->category->name;
                    $extra->element_name  = $selectedElem->name;
                    $extra->price         = $lineItem->unit_price;
                    $extra->save();
                    array_push($extras, $extra);
                }
            }

            if (!$haveValidElements) {
                $extra                = new Extras();
                $extra->id_contracts  = $contract->id_contracts;
                $extra->category_name = 'Material';
                $extra->element_name  = 'Acero inoxidable';
                $extra->price         = 0;
                $extra->save();
                array_push($extras, $extra);
            }
        } else {
            $extra                = new Extras();
            $extra->id_contracts  = $contract->id_contracts;
            $extra->category_name = 'Material';
            $extra->element_name  = 'Acero inoxidable';
            $extra->price         = 250;
            $extra->save();
            array_push($extras, $extra);
        }

        return [
            'contract' => $contract,
            'extras' => $extras
        ];
    }

    private function getAddress($client)
    {
        $address = [
            'address' => 'N/A',
            'colony' => 'N/A',
            'county' => 'N/A',
            'state' => 'N/A',
            'postal_code' => 'N/A'
        ];
        if (!is_null($client->shipping_address)) {
            $address['address'] = $client->shipping_address->street1;
            $address['colony'] = $client->shipping_address->city;
            $address['county'] = $client->shipping_address->city;
            $address['state'] = $client->shipping_address->state;
            $address['postal_code'] = $client->shipping_address->zip;
        } else {
            $firstCharge = $this->sortCharges($client->charges);
            if ($firstCharge[0]) {
                $address['address'] = $firstCharge[0]->details->shipment->address->street1;
                $address['colony'] = $firstCharge[0]->details->shipment->address->city;
                $address['county'] = $firstCharge[0]->details->shipment->address->city;
                $address['state'] = $firstCharge[0]->details->shipment->address->state;
                $address['postal_code'] = $firstCharge[0]->details->shipment->address->zip;
            }
        }

        return $address;
    }

    /**
     * Older first
     */
    private function sortCharges($charges)
    {
        usort($charges, function ($item1, $item2) {
            if ($item1->created_at == $item2->created_at) return 0;
            return $item1->created_at < $item2->created_at ? -1 : 1;
        });
        return $charges;
    }

    private function alreadyCharged($charges)
    {
        $charged = false;
        foreach ($charges as $charge) {
            if (!is_null($charge->paid_at)) {
                return true;
                break;
            }
        }
        return false;
    }

    private function getFirstCollectedCharge($charges)
    {
        $return;
        $chrgs = $this->sortCharges($charges);
        $firstCharge = array_values(
            array_filter($chrgs, function($charge) {
                return $charge->description == 'Extras' && !is_null($charge->paid_at);
            })
        );

        if (count($firstCharge) > 0) {
            $return = $firstCharge[0];
        } else {
            $return = $chrgs[0];
        }
        return $return;
    }

    private function getNextPayday($charges, $collectedAt = null)
    {
        $now = Carbon::now()->subDays(7);
        if (!is_null($collectedAt)) {
            $payday = $collectedAt;
            
            // ON TIME PAYMENT
            $latestOnTime = null;
            $onTimeChargesPaid = array_values(
                array_filter($charges, function($charge) {
                    return $charge->description != 'Pago tardío'
                        && $charge->description != 'Extras'
                        && $charge->description != 'Cargos adicionales Aguagente'
                        && !is_null($charge->paid_at);
                })
            );

            if (count($onTimeChargesPaid) > 0) {
                if ($onTimeChargesPaid[0]->description != 'Payment from order') {
                    $desc = explode('Aguagente Período: ', $onTimeChargesPaid[0]->description);
                } else {
                    $desc[1] = Carbon::createFromTimestamp($onTimeChargesPaid[0]->created_at)->format('Y-m');
                }

                $latestOnTimeDate = Carbon::createFromFormat('Y-m-d', $desc[1].'-'.$payday);
                $latestOnTime = [
                    'charge'           => $onTimeChargesPaid[0],
                    'date'             => $latestOnTimeDate,
                    'missing_payments' => $latestOnTimeDate->diffInMonths($now)
                ];
            }

            // LATE PAYMENTs PAID
            $latePaymentsPaid = array_values(
                array_filter($charges, function($charge) use ($latestOnTime) {
                    $base = $charge->description == 'Pago tardío'&& !is_null($charge->paid_at);
                    if (!is_null($latestOnTime)) {
                        $base = $base && $charge->created_at >= $latestOnTime['charge']->created_at;
                    }
                    return $base;
                })
            );

            $latePayments = [
                'already_paid_count' => count($latePaymentsPaid),
                'last_paid_charge'   => $latePaymentsPaid[0],
                'last_paid_date'     => ''
            ];

            if ($latePayments['already_paid_count'] > 0) {
                $lp = Carbon::createFromTimestamp($latePayments['last_paid_charge']->created_at)->format('Y-m');
                $latePayments['last_paid_date'] = Carbon::createFromFormat('Y-m-d', $lp.'-'.$payday);
            }
            
            // LATE PAYMENTs UNPAID
            $lastLatePaymentUnpaid = array_values(
                array_filter($charges, function($charge) use ($latestOnTime, $latePayments) {
                    $base = $charge->description == 'Pago tardío' && is_null($charge->paid_at);

                    if (!is_null($latestOnTime)) {
                        $base = $base && $charge->created_at >= $latestOnTime['charge']->created_at;
                    }

                    if (!is_null($latePayments)) {
                        $base = $base && $charge->created_at >= $latePayments['last_paid_charge']->created_at;
                    }

                    return $base;
                })
            );

            // Calculate next_payday
            if ($latestOnTime) {
                $next_payday = $latestOnTime['date']->addMonths($latestOnTime['missing_payments'] + 1);
            }

            if ($latePayments['already_paid_count'] > 0) {
                $missingMonths = $latePayments['last_paid_date']->diffInMonths($now);
                $next_payday = $latePayments['last_paid_date']->addMonths($missingMonths + 1);
            }


            // if ($latePayments['already_paid_count'] == 0 && is_null($next_payday)) {} // Creo que no hay que hacer nada
            if ($next_payday) {
                return $next_payday->format('Y-m-d');
            }
        }
        return null;
    }

    private function getCollectedDate($charge)
    {
        if ($charge) {
            return Carbon::createFromTimestamp($charge->created_at);
        }
        return null;
    }

    private function getChargeLineItem($charge, $lineItemName, $withContains = false)
    {
        foreach($charge->details->line_items as $lineItem) {
            if ($lineItem->name == $lineItemName) {
                return $lineItem;
            }
            
            if ($withContains) {
                if (strpos($lineItem->name, $lineItemName) !== false) {
                    return $lineItem;
                }
            }
        }
        
        return false;
    }

    private function getSocialResp($charges)
    {
        $chrgs = array_values(
            array_filter($charges, function($charge) {
                return $charge->description != 'Pago tardío' && !is_null($charge->paid_at);
            })
        );
        $latestCharge = $chrgs[0];
        if (!is_null($latestCharge)) {
            $price = $latestCharge->amount / 100;
            return is_float($price);
        }
        return false;
    }

    private function getMonthlyFee($charges)
    {
        $monthlyFee = 27900.00;
        $chrgs = array_values(
            array_filter($charges, function($charge) {
                return $charge->description != 'Pago tardío' && $charge->description != 'Extras';
            })
        );
        $latestCharge = $chrgs[0];
        if (!is_null($latestCharge)) {
            $netoPrice = $latestCharge->amount / 100;
            if ($netoPrice / 150) {
                if ($latestCharge->description == 'Payment from order') {
                    $netoPrice-=100;
                }
                $withoutSR = round($netoPrice / 1.007);
                $monthlyFee = $withoutSR * 100;
            }
        }
        return $monthlyFee;
    }

    /**
     * STEP 3
     * Re-create Charges or update existing ones.
     * This function will print a list of non-existent clients.
     * Grab the "cus_XXXXX" pattern, to get a full array of tokens, for the STEP 5.1
     */
    public function getCharges(Request $request, $fileName)
    {
        $clients = json_decode(Storage::disk('nosense')->get($fileName.'.json'));
        $clientsSorted = $this->sortCharges($clients);
        $nonExistents = [];
        $i = 0;
        foreach ($clientsSorted as $jsonClient) {
            $i++;
            try {
                DB::beginTransaction();
                $client = Client::where('email', $jsonClient->email)->first();

                if ($client) {
                    print_r("Usuario $client->name <br>");
                    $this->processCharges($client, $jsonClient->charges);
                    print_r($i.') '.$jsonClient->email.' Cargos insertados con exito');
                    print_r("<br>");
                    print_r("----------------------<br>");
                } else {
                    $nonExistents[] = $i.') '.$jsonClient->email.' No existe el cliente '.$jsonClient->id;
                    print_r($i.') '.$jsonClient->email.' No existe el cliente '.$jsonClient->id);
                    print_r("<br>");
                    print_r("----------------------<br>");
                }
                DB::commit();

                
            } catch (\Exception $e) {
                print_r($i.') '.$jsonClient->email.' Error');
                print_r($e->getMessage());
                print_r("<br>");
                print_r("----------------------<br>");
                DB::rollback();
            }
        }

        foreach($nonExistents as $non) {
            print_r($non);
            print_r("<br>");
        }
    }

    private function processCharges($client, $charges)
    {
        $chargesSorted = $this->sortCharges($charges);
        foreach($chargesSorted as $jsonCharge) {
            $charge = Charge::find($jsonCharge->id);

            if (!$charge) {
                print_r("----------- Creando Charge: ".$jsonCharge->id);
                print_r("<br>");
                $charge = $this->createCharge($client, $jsonCharge);
            } else {
                print_r("----------- Actualizando Charge: ".$jsonCharge->id);
                print_r("<br>");
                $this->updateCharge($charge, $jsonCharge);
            }
        }
    }

    private function createCharge($client, $jsonCharge)
    {
        $createdAt = $this->getCollectedDate($jsonCharge);
        $paidAt = ($jsonCharge->paid_at ? Carbon::createFromTimestamp($jsonCharge->paid_at) : null);

        $charge = new Charge;
        $charge->id_charges      = $jsonCharge->id;
        $charge->id_clients      = $client->id_clients;
        $charge->customer_id     = $jsonCharge->customer_id;
        $charge->description     = $jsonCharge->description;
        $charge->amount          = $jsonCharge->amount;
        $charge->fee             = $jsonCharge->fee;
        $charge->paid_at         = ($paidAt ? $paidAt->format('Y-m-d H:i:s') : $paidAt);
        $charge->failure_message = $jsonCharge->failure_message;
        $charge->created_at      = $createdAt->format('Y-m-d H:i:s');

        if ($jsonCharge->payment_method->type == 'oxxo' || $jsonCharge->payment_method->type == 'spei') {
            $extraData = [
                'line_items' => $jsonCharge->details->line_items,
                'type'       => $jsonCharge->payment_method->type,
                'id_clients' => $client->id_clients
            ];
            $charge->extra_data = json_encode($extraData);
            $charge->customer_id = $client->conekta_token;
        }
        $charge->save();

        print_r("----------- Charge Created: ".$jsonCharge->id);
        print_r("<br>");

        if ($jsonCharge->refunds) {
            foreach($jsonCharge->refunds as $refund) {
                $this->createRefund($jsonCharge->id, $refund);
            }
        }
        return $charge;
    }

    private function updateCharge($charge, $jsonCharge)
    {
        $paidAt = ($jsonCharge->paid_at ? Carbon::createFromTimestamp($jsonCharge->paid_at) : null);

        $charge->paid_at         = ($paidAt ? $paidAt->format('Y-m-d H:i:s') : $paidAt);
        $charge->failure_message = $jsonCharge->failure_message;
        $charge->save();

        print_r("----------- Charge updated: ".$jsonCharge->id);
        print_r("<br>");

        if ($jsonCharge->refunds) {
            foreach($jsonCharge->refunds as $refund) {
                if (count(Refund::where('id_charges', $charge->id_charges)->get()) < 1) {
                    $this->createRefund($charge->id_charges, $refund);
                }
            }
        }
    }

    private function createRefund($charge, $jsonRefund) 
    {
        print_r("---------------------- Creando Refund: ".$jsonRefund->id);
        print_r("<br>");
        $createdAt = $this->getCollectedDate($jsonRefund);

        $refund = new Refund;
        $refund->amount          = abs($jsonRefund->amount);
        $refund->created_at      = $createdAt->format('Y-m-d H:i:s');
        $refund->id_charges      = $charge;
        $refund->save();
    }

    /**
     * STEP 4
     * Generate Debts if not exists
     */
    public function getDebts(Request $request)
    {
        for ($f=900; $f >= 1; $f=$f-100) {
            $file = "data-".($f - 100)."-$f";
            print_r("$file<br>");

            $clients = json_decode(Storage::disk('nosense')->get($file.'.json'));
            $clientsSorted = $this->sortCharges($clients);
            $i = 0;
            foreach ($clientsSorted as $jsonClient) {
                $i++;
                try {
                    DB::beginTransaction();
                    $client = Client::where('email', $jsonClient->email)->first();

                    if ($client) {
                        print_r("Usuario $client->name <br>");
                        $this->processChargesForDebt($client, $jsonClient->charges);
                        print_r($i.') '.$jsonClient->email.' Debts insertados con exito');
                        print_r("<br>");
                        print_r("----------------------<br>");
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    print_r($i.') '.$jsonClient->email.' Error');
                    print_r($e->getMessage());
                    print_r("<br>");
                    print_r("----------------------<br>");
                    DB::rollback();
                }
            }
        }
    }

    private function processChargesForDebt($client, $charges)
    {
        $chargesSorted = $this->sortCharges($charges);
        foreach($chargesSorted as $jsonCharge) {

            if ($jsonCharge->description == 'Pago tardío' && !is_null($jsonCharge->paid_at)) {
                $debt = Debt::where('id_charges', $jsonCharge->id)->get();
                if (count($debt) < 1) {
                    print_r("----------- Creando Debt: ".$jsonCharge->id);
                    print_r("<br>");
                    $this->createDebt($client, $jsonCharge);
                } else {
                    // print_r("----------- Actualizando Debt: ".$jsonCharge->id);
                    // print_r("<br>");
                    // $this->updateDebt($debt, $jsonCharge);
                }
            }

            if ($jsonCharge->description == 'Payment from order' && !is_null($jsonCharge->paid_at)) {
                $isDebt = false;
                foreach($jsonCharge->details->line_items as $lineItem) {
                    if ($lineItem->description == 'Pago tardío') {
                        $isDebt = true;
                        break;
                    }
                }

                if ($isDebt) {
                    $debt = Debt::where('id_charges', $jsonCharge->id)->get();
                    if (count($debt) < 1) {
                        print_r("----------- Creando Debt Oxxo: ".$jsonCharge->id);
                        print_r("<br>");
                        $this->createDebt($client, $jsonCharge);
                    } else {
                        // print_r("----------- Actualizando Debt Oxxo: ".$jsonCharge->id);
                        // print_r("<br>");
                        // $this->updateDebt($debt, $jsonCharge);
                    }
                }
            }
        }
    }

    private function createDebt($client, $jsonCharge)
    {
        $createdAt = $this->getCollectedDate($jsonCharge);
        
        $debt = new Debt;
        $debt->id_clients         = $client->id_clients;
        $debt->amount             = $jsonCharge->amount - 10000.00;
        $debt->collection_fees    = 10000.00;
        $debt->moratory_fees      = 0;
        $debt->next_try_to_charge = Carbon::now();
        $debt->id_charges         = $jsonCharge->id;
        $debt->created_at         = $createdAt->format('Y-m-d H:i:s');
        $debt->save();
        print_r("---------------------- Debt Created: ".$debt->id_debts);
        print_r("<br>");
    }

    private function updateDebt($debt, $jsonCharge)
    {
        if (is_null($debt->id_charges)) {
            // $debt->id_charges = $jsonCharge->id;
            // $debt->save();
            print_r("---------------------- Debt Updated: ".$debt->id_debts);
            print_r("<br>");
        }
    }

    /**
     * STEP 5.1
     * Check for non-existentClients, the amount of payments paid and not in our database.
     * Have to paste inside $nonExistent, the list from STEP 3.
     * This function will print only:
     *    1) Customers with succesfull payment. (noExistCharge)
     *    2) Customers with payments, but the client doest exist in our database. (existChargeButNoClient)
     *          Using this list, you will have to recreatethem manually.
     */
    public function getNonExistentCharges(Request $request)
    {
        $nonExistent = [
            'cus_5E2FFpztXJZJ4cL1a',
            'cus_KxsYqCbdc7xKt6Z5L',
            'cus_yhbNwNC5fGEVpRTr6',
            'cus_daX9F7MQ4pyCu7Nx1',
            'cus_wYzC72rQcRRe1av1m',
            'cus_dGPXNhUhsX56Q1ToY',
            'cus_cohkM5qP9aQV7FZDt',
            'cus_qfb4LUkjwwuuABYDy',
            'cus_KvTTeJMW2U6vW94J9',
            'cus_8ZfxYfJhFRPMoeqrX',
            'cus_e6wsBeo8jFYVyxU6z',
            'cus_b69P9ctb2Taqkgp4r',
            'cus_vPDr5oHsJ8TMmPcn6',
            'cus_9Rn1xeMM9TWfKiT5A',
            'cus_joksCiAxsM7aCJQmp',
            'cus_XtA7auuuhamySe15J',
            'cus_25nM2WSGEzrPt21zd',
            'cus_g9HxpvbeGDjjJbbAM',
            'cus_yzZLoANNaN6AUBx2f',
            'cus_ogq7sUG6xDTQZoXqX',
            'cus_1sWyQLT6SDXLSKd1M',
            'cus_nPdj98TJfMWGj4u3s',
            'cus_14PVdVHFeXdeJnbjo',
            'cus_c9Eb4xJNKvmb2U2R7',
            'cus_FUwRNKW8EiByqWtJq',
            'cus_arUPge55KpMQ5aieT',
            'cus_2fSDEdjMqFi8GhNHi',
            'cus_2fYNEFtVVe4wA9ens',
            'cus_2fdo1x5urxTVmctTx',
            'cus_2fdxGaZi9o8ajBRZc',
            'cus_2fumxobrnTEhrDfcX',
            'cus_2g4toW8hi56ciLP9T',
            'cus_2gFMGd12b32Cidn3o',
            'cus_2gJGNepnNRgzzWh56',
            'cus_2gJyQzhwwBAzWeaYG',
            'cus_2gLFn4TfoPQ7G3sFj',
            'cus_2gMEvzbi8qkPUpbox',
            'cus_2gQYuczn6dEqE17T2',
            'cus_2gQoYnpD8Zg99qksY',
            'cus_2gQodHXhwEVEkYdXE',
            'cus_2gW1xyoGN8F3qLcAi',
            'cus_2gWRZDHq9nTKDKCbA',
            'cus_2gXvVhSKyoFQxQnsQ',
            'cus_2gaDUoaRUpxsa613A',
            'cus_2gaDiwabA3JLCYkCi',
            'cus_2gaDo6QkCJvK8QWeY',
            'cus_2geCyuMkRkv2S2pr7',
            'cus_2geSnwDZYzVPddqKL',
            'cus_2geT7fFpyFWRS8DNk',
            'cus_2gecYLur6amsALjz2',
            'cus_2geo1YKy2gGDHtNw7',
            'cus_2gfA5NMjUHvvZxGmC',
            'cus_2gfULZUwCgBNyVWSe',
            'cus_2ggYMC9PgBJU75yiw',
            'cus_2ggnp8Q65q9H4G8J4',
            'cus_2ghsR8NiPc59caWvU',
            'cus_2giUwPpBS6VopaCFk',
            'cus_2gkLGFKJ8YtjSYWhj',
            'cus_2gpiNd1n8HDeNRrEA',
            'cus_2gpinmW2rU8vNPGzE',
            'cus_2gtYqSe9e2jHcwAQS',
            'cus_2gvaM1TKVQhRaT991',
            'cus_2gwWPefRdqkzP8L8Z',
            'cus_2gypcWW4xoL7duhxP',
            'cus_2h4miYK4pncjUsH5d',
            'cus_2h9dzAoCrnHRFthhb',
            'cus_2h9e8ann2ZKpv7aMF',
            'cus_2h9eCJX5N3tyF3Rh8',
            'cus_2hCCxcVB99wzxPMrr',
            'cus_2hDdGUqDKdRe2oWrx',
            'cus_2hEakvByPjaNdDdCJ',
            'cus_2hTKHiojF9YKApSSt',
            'cus_2hTKQ9dVBpov86VNe',
            'cus_2hVBSF17tCyfB49AW',
            'cus_2hVDuceuWXXzoz5UK',
            'cus_2ha4hrSKuZogpRf8q',
            'cus_2hc4AGMpgjcQDWeeZ',
            'cus_2hc6QHnEvCa6EjU3R',
            'cus_2hc7FHtXjTZXmX1No',
            'cus_2hc9H5zAFuhZBcWAK',
            'cus_2hgLZRjmbuGBVigCM',
            'cus_2hqEddrr1sysfXhSa',
            'cus_2hsk2wZtVnEhQUYCb',
            'cus_2huhCZDxuJnb6Sx4G',
            'cus_2hxQWaqzeDUHoVeWf',
            'cus_2i5EhMxnEY7eJK489',
            'cus_2i9oepzhjG5mgndSL',
            'cus_2iRbSZr6NUGLFKtZg',
            'cus_2iXLf2yek1VJWr4nd',
            'cus_2iZNZDA37jXEr4LsA',
            'cus_2ibQvp5xkoHe6JMK7',
            'cus_2iiBgZhMPD2u7cocQ',
            'cus_2iknjuDcuYHdfMhn5',
            'cus_2irLyLSZsgjWwMJud',
            'cus_2j1rBwpa6vhmRD9QT',
            'cus_2jAdjay3iSn77QdYL',
            'cus_2jBgQCHAE9DsjUj41',
            'cus_2jCF74czKSXCRZht2',
            'cus_2jEFxaZ74eujyR2QP',
            'cus_2jXwms9ZkjJJed6mJ',
            'cus_2jZWsacWDbFv72XLG',
            'cus_2jdjyi84nkvBJqhzx',
            'cus_2jeTVy82xqoQVQrJe',
            'cus_2jeikrZXXBaQrDmnS',
            'cus_2jfSfGfqQ5T9roD6q',
            'cus_2jfSoX7oJGK3ebMUt',
            'cus_2jfT8jgsj21nATKqL',
            'cus_2jfWGEkeNyqQ3MxbD',
            'cus_2jhSsjj1FENZ8iNHv',
            'cus_2jm2gXKgrDtU1NBQG',
            'cus_2jmuCsQPcWYq24zNe',
            'cus_2joWZhw6LwMDcPnDu',
            'cus_2jqJp5J9vY1mP47w6',
            'cus_2ju7YaovdYq7idgpj',
            'cus_2kDmQ63g2ET8KpVL4',
            'cus_2kSuNouitN3bLdoDV',
            'cus_2kYpMu5pUj4tAjZGC',
            'cus_2ka4qM1BZZSmWWauX',
            'cus_2kbgsmBdB7sQ1ChHo',
            'cus_2keGTkPGa8BRxH2Qo',
            'cus_2keGiW555Caa5kW3a'
        ];
        $nonExistentsCharges = [
            'noExistCharge'          => [],
            'existChargeButNoClient' => []
        ];
        $nonExistentClientsButHaveCharges = [];
        for($f=900; $f >= 1; $f=$f-100) {
            $file = "data-".($f - 100)."-$f";
            print_r("$file<br>");
            $clients = json_decode(Storage::disk('nosense')->get($file.'.json'));
            $clientsSorted = $this->sortCharges($clients);

            $i = 0;
            foreach ($clientsSorted as $jsonClient) {
                $i++;
                try {
                    if (in_array($jsonClient->id, $nonExistent)) {
                        print_r("--------- Cliente Inexistente: ".$jsonClient->name);
                        print_r("<br>");
                        foreach ($jsonClient->charges as $jsonCharge) {
                            $charge = Charge::find($jsonCharge->id);
                            if (!$charge) {
                                $nonExistentsCharges['noExistCharge'][$jsonClient->id][] = $jsonCharge->id;
                            } else {
                                $client = Client::find($charge->id_clients);
                                
                                if (!$client && !is_null($charge->paid_at)) {
                                    $nonExistentsCharges['existChargeButNoClient'][$jsonClient->id][] = $jsonCharge->id;
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    print_r("<br>");
                    print_r("----------------------<br>");
                }
            }
        }

        dd($nonExistentsCharges);
    }

    /**
     * STEP 5.2
     * Check only Customers with success payments, and print a list of Name, Email and Creation Date.
     * Using the list from STEP 5.1, paste it inside $nonExistent.
     * This function will print the list of Customers, use that list to search in database, using 
     * the Creation Date in the clients.collected_at (+1 day in the between statement).
     * 
     * Once you have finished the updates, repeat STEP 3 & 4
     */
    public function getNonExistentClients(Request $request)
    {
        $nonExistent = [
            'cus_joksCiAxsM7aCJQmp',
            'cus_2g4toW8hi56ciLP9T',
            'cus_2irLyLSZsgjWwMJud',
            'cus_2jBgQCHAE9DsjUj41',
            'cus_2jCF74czKSXCRZht2',
            'cus_2jeTVy82xqoQVQrJe',
            'cus_2jeikrZXXBaQrDmnS',
            'cus_2jfSoX7oJGK3ebMUt',
            'cus_2kSuNouitN3bLdoDV',
            'cus_2keGiW555Caa5kW3a'
        ];
        $nonExistentsCharges = [];
        for($f=900; $f >= 1; $f=$f-100) {
            $file = "data-".($f - 100)."-$f";
            print_r("$file<br>");
            $clients = json_decode(Storage::disk('nosense')->get($file.'.json'));
            $clientsSorted = $this->sortCharges($clients);

            $i = 0;
            foreach ($clientsSorted as $jsonClient) {
                $i++;
                try {
                    if (in_array($jsonClient->id, $nonExistent)) {
                        $created = Carbon::createFromTimestamp($jsonClient->created_at)->format('Y-m-d H:i:s');
                        print_r("- Cliente Inexistente: $jsonClient->name | $jsonClient->id | $jsonClient->email | $created");
                        print_r("<br>");
                    }
                } catch (\Exception $e) {
                    
                }
            }
        }

        dd($nonExistentsCharges);
    }

    /**
     * STEP 6
     * Update the client data with the last charge.
     */
    public function updateClientInfoWithLastCharge(Request $request)
    {
        for($f=900; $f >= 1; $f=$f-100) {
            $file = "data-".($f - 100)."-$f";
            print_r("$file<br>");
            $clients = json_decode(Storage::disk('nosense')->get($file.'.json'));
            $clientsSorted = $this->sortCharges($clients);

            $i = 0;
            foreach ($clientsSorted as $jsonClient) {
                $i++;
                $client = Client::where('email', $jsonClient->email)->first();
                if ($client) {
                    $this->processClients($client, $jsonClient);
                }
                try {
                } catch (\Exception $e) {
                    print_r($e->getMessage().'<br>');
                }
            }
        }
    }

    private function processClients($client, $jsonClient)
    {
        $charges = $this->sortCharges($jsonClient->charges);
        $chrgs = array_values(
            array_filter($charges, function($charge) {
                return $charge->description != 'Pago tardío' 
                    && !is_null($charge->paid_at);
            })
        );

        $client->created_at = Carbon::createFromTimestamp($jsonClient->created_at)->format('Y-m-d H:i:s');
        if(count($chrgs) > 0) {

            $latestCharge = $chrgs[0];
            $desc = [];
            $nextPayDay = null;
            if (strpos($latestCharge->description, 'Payment From Order') === false) {
                $desc = explode('Aguagente Período: ', $latestCharge->description);
                $client->name = $latestCharge->details->name;
                $client->phone = $latestCharge->details->phone;
                $client->email = $latestCharge->details->email;
                $client->address = $latestCharge->details->shipment->address->street1;
                $client->county = $latestCharge->details->shipment->address->city;
                $client->state = $latestCharge->details->shipment->address->state;
                $client->postal_code = $latestCharge->details->shipment->address->zip;
            } elseif(strpos($latestCharge->description, 'Payment from order') !== false) {
                $desc[1] = Carbon::createFromTimestamp($latestCharge->created_at)->format('Y-m');
                $client->name = $latestCharge->details->name;
                $client->phone = $latestCharge->details->phone;
                $client->email = $latestCharge->details->email;
                $client->address = $jsonClient->shipping_address->street1;
                $client->county = $jsonClient->shipping_address->city;
                $client->state = $jsonClient->shipping_address->state;
                $client->postal_code = $jsonClient->shipping_address->zip;
            }
            if ($desc[1] && $client->payday) {
                $nextPayDay = Carbon::createFromFormat('Y-m-d', $desc[1].'-'.$client->payday)->addMonths(1);
                $client->next_payday = $nextPayDay->format('Y-m-d');
            }
        }
        $client->save();

        if ($jsonClient->cards) {
            foreach($jsonClient->cards as $jsonCard) {
                $card = CardsVendors::where('card_token', $jsonCard->id)->first();
                if (!$card) {
                    $this->createCard($client, $jsonCard);
                }
            }
        }
    }

    private function createCard($client, $jsonCard)
    {
        $card              = new Card;
        $card->id_clients  = $client->id_clients;
        $card->name        = $client->name;
        $card->address     = $client->address != '' ? $client->address: '';
        $card->colony      = $client->colony != '' ? $client->colony: '';
        $card->postal_code = $client->postal_code != '' ? $client->postal_code: '';
        $card->county      = ($client->county != '' ? $client->county : '');
        $card->state       = $client->state != '' ? $client->state: '';
        $card->country     = 'Mexico';
        $card->phone       = $client->phone;
        $card->email       = $client->email;
        $card->exp_month   = $jsonCard->exp_month;
        $card->exp_year    = $jsonCard->exp_year;
        $card->card_number = $jsonCard->last4;
        $card->save();

        $cardVendor              = new CardsVendors;
        $cardVendor->id_cards    = $card->id_cards;
        $cardVendor->id_vendor   = 1;
        $cardVendor->card_token  = $jsonCard->id;
        $cardVendor->save();
    }

    /**
     * STEP 7
     * Calculate the next_payday for all the clients, and debts
     */
    public function getClientsLastChargeAndLastLatePayment(Request $request)
    {
        $finalList = [];
        $clients = Client::all(['id_clients', 'name']);
        foreach ($clients as $client) {
            $recentOnTime = Charge::where('id_clients', $client->id_clients)
                ->whereNotNull('paid_at')
                ->orderBy('paid_at', 'desc')
                ->first();

            $recentPaidDebt = Debt::where('id_clients', $client->id_clients)
                                    ->whereNotNull('id_charges');
            if ($recentOnTime) {
                $recentPaidDebt = $recentPaidDebt->where('created_at', '>=', $recentOnTime->paid_at);
            }
            $recentPaidDebt = $recentPaidDebt->orderBy('created_at', 'desc')->first();

            $debtsAfter = Debt::where('id_clients', $client->id_clients)
                                ->whereNull('id_charges');
            if ($recentOnTime && !$recentPaidDebt) {
                $debtsAfter = $debtsAfter->where('created_at', '>=', $recentOnTime->paid_at);
            }
            if ($recentPaidDebt) {
                $debtsAfter = $debtsAfter->where('created_at', '>=', $recentPaidDebt->paid_at);
            }
            $debtsAfter = $debtsAfter->orderBy('created_at', 'desc')->get();
            
            dd($recentOnTime, $recentPaidDebt, $debtsAfter);
        }
    }
}