<?php

namespace App\Http\Controllers;

use App\Withdrawal;
use App\Http\Controllers\Main;
use Request;
use Validator;

class WithdrawalsController extends Main {

    /**
     * index
     * Un retiro es un monto monetario retirado de cierto fondo.
     * Devuelve todas los retiros (\App\Withdrawal) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $withdrawals = Withdrawal::query();

            foreach(Request::query() as $name => $value) {

                $withdrawals = $withdrawals->where($name, $value);

            }

            return Main::response(true, 'OK', $withdrawals->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * create
     * Crea un una retiro.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Withdrawal
     * 
     * @return response     Created|Bad Request|Internal Server Error
     */
    public function create() {

        try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'amount' => 'required|numeric',
                    'date'   => 'required|date',
                    'reason' => 'required|string'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            $withdrawal = new Withdrawal;
            $withdrawal->amount = $input['amount'];
            $withdrawal->date = $input['date'];
            $withdrawal->reason = $input['reason'];
            $withdrawal->save();

            return Main::response(true, 'Created', $withdrawal, 201);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * show
     * Muestra un retiro (\App\Withdrawal) por medio del ID
     *
     * @\App\Withdrawal
     * 
     * @param  int      $id ID del retiro
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($withdrawal = Withdrawal::find($id)) {

            return Main::response(true, 'OK', $withdrawal, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }


}

