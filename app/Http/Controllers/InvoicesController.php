<?php

namespace App\Http\Controllers;

use http\Env\Response;
use App\Invoice;
use App\Client;
use App\Traits\NotifyTrait;
use Request;
use DB;
use ZipArchive;

class InvoicesController extends Main
{

    use NotifyTrait;

        /**
         * __construct
         * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
         */
        public function __construct()
        {

            parent::__construct(['index']);

        }


    public function index()
    {
        $params = Request::all();

        $resolve = 'true';
        if (isset($params['resolve'])) {
            $resolve = $params['resolve'];
            unset($params['resolve']);
        }

        $skip = $params['start'];
        $take = $params['length'];

        $total = Invoice::get()->count();
        $totalFilters = $total;
        $invoices = new Invoice;
        $invoices = $invoices->join('clients', 'invoices.id_clients', '=', 'clients.id_clients');
        if ($params['search']['value'] != '') {
            $invoices = $invoices->where('clients.name', 'like', '%' . $params['search']['value'] . '%')
                ->orWhere('invoices.id_invoices', 'LIKE', '%' . $params['search']['value'] . '%')
                ->orWhere('clients.rfc', 'LIKE', '%' . $params['search']['value'] . '%');
            $totalFilters = $invoices->count();
        }

        $columnsFilter = ['id_invoices', 'clients.name', 'clients.rfc', 'date_stamp'];

        if ($params['order'] != null) {
            foreach ($params['order'] as $order) {
                $columnFilter = $columnsFilter[$order['column']];
                $invoices = $invoices->orderBy($columnFilter, $order['dir']);
            }
        }
        $invoices = $invoices->with('client');
        $invoices = $invoices->skip($skip)->take($take)->get();

        $return = [
            "draw" => (int)$params['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $totalFilters,
            "data" => $invoices
        ];

        return response()->json($return);
    }

    public function sendInvoice()
    {
        $params = Request::all();
        $invoice = Invoice::with(['client'])->find($params['id_invoices']);

        if ($invoice != null) {
            $this->sendInvoiceToClient($invoice, $params['message']);
        }
        return response()->json($invoice);
    }

    public function downloadFiles($id)
    {
        $params = Request::all();
        $invoice = Invoice::find($id);
        
        $directory = public_path('invoices_files/');
        $zipDirectory = $directory . $invoice->id_clients . '.zip';
        $zip = new ZipArchive();

        if ($zip->open($zipDirectory, ZipArchive::CREATE) === TRUE) {
            $xmlFile = $directory . $invoice->filename . '.xml';
            $pdfFile = $directory . $invoice->filename . '.pdf';
            // Add File in ZipArchive
            $zip->addFile($xmlFile, $invoice->filename . '.xml');
            $zip->addFile($pdfFile, $invoice->filename . '.pdf');
            // Close ZipArchive     
            $zip->close();
        }
        
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=\"" . basename($zipDirectory) . "\"");
        header("Content-Length: " . filesize($zipDirectory));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile($zipDirectory);
        header("Connection: close");
    }
}