<?php namespace App;

use App\StandardType;

class Level extends StandardType {
	
	const VIAJERO = 'VIAJERO';
	const MARINERO = 'MARINERO';
	const CABO = 'CABO';
	const CAPITAN = 'CAPITAN';
	const ALMIRANTE = 'ALMIRANTE';

}
