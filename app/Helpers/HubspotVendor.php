<?php

namespace App\Helpers;

use SevenShores\Hubspot\Factory;

class HubspotVendor
{
    /**
     * @var $hubspot
     */
    private $hubspot;

    public function __construct()
    {
        $this->hubspot = new Factory([
            'key' => env('HUBSPOT_HAPIKEY'),
        ],
            null,
            [
                'http_errors' => false // pass any Guzzle related option to any request, e.g. throw no exceptions
            ],
            true // return Guzzle Response object for any ->request(*) call
        );
    }

    /**
     * @param $email
     * @return array
     */
    public function getContact($email)
    {
        $success = false;
        $contact = $this->hubspot->contacts()->getByEmail($email);
        $response = $contact->getData();

        if ($contact->getStatusCode() === 200) {
            $success = true;
        }

        return ['success' => $success, 'data' => $response];
    }

    /**
     * @param $data data to create a contact in hubspot
     * @return array
     */
    public function createContact($data)
    {
        $success = false;
        $contact = $this->hubspot->contacts()->create($data);
        $response = $contact->getData();

        if ($contact->getStatusCode() === 200) {
            $success = true;
        }

        return ['success' => $success, 'data' => $response];
    }

    /**
     * @param $id
     * @param $data
     * @return array
     */
    public function updateContact($id, $data)
    {
        $success = false;
        $contact = $this->hubspot->contacts()->update($id, $data);
        $response = $contact->getData();

        if ($contact->getStatusCode() === 200) {
            $success = true;
        }

        return ['success' => $success, 'data' => $response];

    }

    /**
     * @param $data engagement, associations and metadata
     * @return array
     */
    public function createEngagement($data)
    {
        $success = false;
        $response = $this->hubspot->engagements()->create($data['engagement'], $data['associations'], $data['metadata']);

        if ($response->getStatusCode() === 200) {
            $success = true;
        }

        return ['success' => $success, 'data' => $response->getData()];
    }

    /**
     * @param $data
     * @return array
     */
    public function createDeal($data)
    {
        $success = false;
        $response = $this->hubspot->deals()->create($data);

        if ($response->getStatusCode() === 200) {
            $success = true;
        }

        return ['success' => $success, 'data' => $response->getData()];
    }
}
