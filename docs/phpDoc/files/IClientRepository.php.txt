<?php namespace App;

use App\Client;

interface IClientRepository {
	
	/**
	 * @param  integer
	 * @return Client[]
	 */
	public function getById($id_clients);

	/**
	 * @param  integer $id_clients
	 * @return Client[]
	 */
	public function getPayingClients($id_clients);

	/**
	 * @param  integer $id_clients
	 * @return array
	 */
	public function getSignedClientsTree($id_clients);

	/**
	 * @param  integer $id_clients
	 * @param  integer $months
	 * @return Client[]
	 */
	public function getSignedClientsFromThePastMonths($id_clients, $months);

	/**
	 * @param  integer $id_clients
	 * @return float
	 */
	public function getCommissionsFromPastMonth($id_clients);

	/**
	 * @param  Client $client
	 */
	public function save(Client $client);

}

