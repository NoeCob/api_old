@extends('layouts.email')

@section('name')
{{ $nombre }}
@endsection

@section('content')
Tu pago se ha realizado con éxitio. Gracias!  El cargo se reflejará en tu estado de cuenta como <br>
<strong>Aguagente: {{$periodo}}</strong>
Gracias y disfruta de Aguagente!
@endsection