<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     *
     * @param string $dir
     * @return mixed
     *
     */
    protected function getTestData($dir)
    {
        $path = __DIR__ . $dir;
        $file = fopen($path, "r") or die("Unable to open file!");
        $json = fread($file, filesize($path));
        fclose($file);
        return json_decode($json, true);
    }

    /**
     * @param $model Model
     * @return mixed
     */
    protected function getObjectRandom($model)
    {
        $objects = $model::all();
        $maxLength = sizeof($objects);
        // Getting number random with the max length of the clients existing
        $index = rand(0, $maxLength - 1);
        $response = $objects[$index];

        return $response;
    }
}
