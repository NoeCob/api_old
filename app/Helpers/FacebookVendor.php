<?php

namespace App\Helpers;

use Facebook\Facebook;
use FacebookAds\Api;
use FacebookAds\Http\Exception\AuthorizationException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\Lead;
use FacebookAds\Logger\CurlLogger;

class FacebookVendor
{
    /**
     * @var $hubspot
     */
    private $fb;
    private $accessToken;
    private $appSecret;
    private $appId;

    public function __construct()
    {
        $access_token = env('ACCESS_TOKEN');
        $app_secret = env('APP_SECRET');
        $app_id = env('APP_ID');
        Api::init($app_id, $app_secret, $access_token);
        $this->fb = new Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.10',
            'default_access_token' => $access_token
        ]);
    }

    public function sendWelcomeMessage($recipientId)
    {
        try {
            // Returns a `Facebook\FacebookResponse` object

            $response = $this->fb->post(
                '/me/messages',
                [
                    'messaging_type' => 'RESPONSE',
                    'recipient' => [
                        'id' => $recipientId
                    ],
                    'message' => [
                        'text' => 'Hola, en un momento uno de nuestros asesores se pondrá en contacto contigo, gracias por la espera'
                    ]
                ]
            );
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        return $graphNode;
    }

    public function getUser($userId)
    {
        /* make the API call */
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $this->fb->get(
                '/' . $userId
            );
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        /* handle the result */
        return $response->getGraphNode();
    }

    public function getLeadInformationById($leadId)
    {
        try {
            $data = [];
            $error = '';
            $fields = array();
            $params = array();
            $data = (new Lead($leadId))->getSelf(
                $fields,
                $params
            )->exportAllData();
        } catch (RequestException $e) {
            $error = $e->getMessage();
        }

        return empty($error) ? $data : $error;

    }

    /**
     * @return mixed
     */
    public function getFb()
    {
        return $this->fb;
    }

    /**
     * @param mixed $fb
     */
    public function setFb($fb)
    {
        $this->fb = $fb;
    }
}


