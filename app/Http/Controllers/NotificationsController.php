<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Http\Controllers\Main;
use App\Traits\NotifyTrait;
use DB;
use Request;
use Illuminate\Support\Facades\Mail;

class NotificationsController extends Main
{
    use NotifyTrait;

    protected $subjects = [
        array('name' => ''),
        array('name' => 'Problema con la App'),
        array('name' => 'Soporte'),
        array('name' => 'Comentarios'),
        array('name' => 'General')
    ];

    public function __construct()
    {
        parent::__construct(['report']);
    }

    /**
     * index
     * Devuelve todas las notificaciones (\App\Notification) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Notification
     *
     * @return response NULL|Error(404)
     */
    public function index()
    {
        try {

            $notifications = Notification::query();

            foreach (Request::query() as $name => $value) {

                $notifications = $notifications->where($name, $value)->orderBy('created_at', 'desc');

            }

            return Main::response(true, 'OK', $notifications->get());

        } catch (\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    public function report()
    {
        $input = Request::all();
        $data = array(
            'nombre' => $input['nombre'],
            'email' => $input['email'],
            'asunto' => $this->subjects[$input['asunto']]['name'],
            'msg' => $input['msg'],
        );
        Mail::send('email.report', $data, function ($message) use ($input) {
            $message->subject('Reporte de problema de App');
            if ($input['asunto'] == 1) {
                $message->to('it@aguagente.com');
            } else {
                $message->to('contacto@aguagente.com');
            }
            //$message->to('gus.knul@gmail.com');
        });
        return Main::response(true, 'OK', 'Ok');
    }

}
