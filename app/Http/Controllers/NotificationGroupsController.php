<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use App\NotificationGroup;
use Illuminate\Support\Facades\DB;

class NotificationGroupsController extends Main
{


    /**
     * __construct
     * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct()
    {

        parent::__construct(['index']);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificationGroups = NotificationGroup::all();
        return Main::response(true, 'OK', $notificationGroups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $notificationGroup = new NotificationGroup($params['notification_group']);
        try {
            DB::beginTransaction();
            $notificationGroup->save();
            $notificationGroup->users()->attach($params['notification_group']['users']);
            DB::commit();
            return Main::response(true, 'OK', $notificationGroup);
        } catch (\Exception $e) {
            DB::rollback();
            return Main::response(false, 'Internal Server Error', null, 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notificationGroup = NotificationGroup::with('users')->find($id);
        if ($notificationGroup) {
            return Main::response(true, 'OK', $notificationGroup);
        } else {
            return Main::response(false, null, null, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notificationGroup = NotificationGroup::with('users')->find($id);
        $params = $request->all();
        //dd($params);
        try {
            DB::beginTransaction();
            $notificationGroup->update($params['notification_group']);
            $notificationGroup->users()->detach();
            $notificationGroup->users()->attach($params['notification_group']['users']);
            DB::commit();
            return Main::response(true, 'OK', $notificationGroup);
        } catch (\Exception $e) {
            DB::rollback();
            return Main::response(false, 'Internal Server Error', null, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notificationGroup = NotificationGroup::find($id);
        if ($notificationGroup && $notificationGroup->delete()) {
            return Main::response(true, 'OK', null);
        } else {
            return Main::response(false, null, null, 404);
        }
    }
}
