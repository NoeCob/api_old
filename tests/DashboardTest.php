<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;

class DashboardTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Dashboard test.
     *
     * @return void
     */
    public function testDashboard()
    {
        $this->json('GET', '/dashboard')
            ->assertResponseStatus(200);
    }
}
