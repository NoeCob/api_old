<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClientService;
use App\Client;
use App\Charge;
use App\Commission;
use App\CommissionService;
use App\Debt;
use App\MySQLClientRepository;
use App\Traits\DebtTrait;
use App\Traits\NotifyTrait;
use Carbon\Carbon;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Conekta_ResourceNotFoundError;
use Conekta_ProcessingError;
use Conekta_Handler;

class DailyCharge extends Command
{
    use DebtTrait, NotifyTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charges:run {day?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job for create charges per Client
    						{day : Simulate the day of the actual month}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 1) Obtiene todos los clientes que tengan que pagar.
     * 2) Obtiene las deudas y cargos de cada cliente.
     * 3) Verifica fecha del ultimo pago, para ver tiene que realizar cargo o no.
     * 4) Antes de cobrar el mes, intenta cobrar las deudas que tenga antes.
     * 5) Verifica si tiene o no aguagratis.
     * 5.1) Si no, intenta realizar el cargo
     *
     * @return void
     */
    public function handle()
    {   
		$today        = date('Y-m-d');
        $simulatedDay = $this->argument('day');
        $exceptionList = [];
        $onlyRun = [142,231,304,305,307,379];
		
        if ($simulatedDay != '') {
            $today = date('Y-m-').str_pad($simulatedDay, 2, '0', STR_PAD_LEFT);
        }

        $this->info('Fecha: '.$today);
        $clients = Client::where('status', '=', 'accepted')
                            ->whereNotNull('next_payday')
                            ->where('next_payday', '<=', $today)
                            ->orWhere('pay_tries', '>', 0)
                            //->whereNotIn('id_clients', $exceptionList)
                            //->whereIn('id_clients', $onlyRun)
                            ->get();
        
        foreach ($clients as $client) {
            if ($client->status == 'accepted') {
                $debts      = Debt::where('id_clients', '=', $client->id_clients)
                                        ->whereNull('id_charges')
                                        ->where('next_try_to_charge', '<=', $today)
                                        ->get();
                                        
                $charges    = Charge::where('id_clients', '=', $client->id_clients)
                                        ->whereNotIn('description', ['Extras','Pago Tardío', 'Depósito', 'Cargos adicionales Aguagente'])
                                        ->whereNotNull('paid_at')
                                        ->orderBy('created_at')
                                        ->get();

                $lastCharge = $charges->last();
                $dateToDiff = $lastCharge->created_at;

                $DateLastCharge = explode('Aguagente Período: ', $lastCharge->description);
                if (count($DateLastCharge) > 1) {
                    $realDateLastCharge = $DateLastCharge[1].'-'.str_pad($client->pay_day, 2, '0', STR_PAD_LEFT);
                    $dateToDiff         = $realDateLastCharge;
                }

                $arrayDiff  = $this->generateDateDiff($dateToDiff);

                //intenta cobrar las deudas que tenga el cliente primero.
                $debtsCharged = [];
                foreach ($debts as $debt) {
                    $debtCharged = $this->chargeDebt($debt->id_debts);
                    if ($debtCharged == '500' || $debtCharged == '402') {
                        // skip next debt charge and dailyCharge
                        // $this->info('skipping this client for today');
                    }
                    $debtsCharged[] = $debtCharged;
                }
                
                //Si la diferencia de dias entre el ultimo pago y hoy es mayor a 27, genera el cargo
                if ($arrayDiff[1] > 27 || is_null($lastCharge)) {
                    $validClients = [];
                    $allClients   = Client::getHierarchicalTree($client->id_clients)[0]['clients'];
                    
                    foreach ($allClients as $clientClient) {
                        if ($clientClient[debt] == '0.00' && $clientClient[status]=='accepted') {
                            array_push($validClients, $clientClient);
                        }
                    }

                    Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                    Conekta::setLocale('es');
                    
                    //Aguagratis: clientes activos y al dia > 6
                    if (count($validClients) < 6) {

                        $dateDiffPaytries = $this->generateDateDiff($client->next_payday);
                        if (   ($client->pay_tries == 3 && $dateDiffPaytries[1] == 3)
                            || ($client->pay_tries == 4 && $dateDiffPaytries[1] == 5)
                        ) {
                            //nothing to do
                        } else {

                            try {
                                $customer = Conekta_Customer::find($client->conekta_token);
                                $cards    = json_decode($customer->cards->__toJSON());
                                $periodo  = Carbon::parse($client->next_payday)->format('Y-m');

                                foreach ($cards as $i => $card) {
                                    if($card->id == $customer->default_card_id) {
                                        unset($cards[$i]);
                                        array_unshift($cards, $card);
                                        break;
                                    }
                                }

                                $amount = $client->monthly_fee;
                                if ($client->social_responsability == 1) {
                                    $srAmount = ($amount/1.16)*.007;
                                    $amount  += $srAmount;
                                }

                                $charged = [];
                                foreach($cards as $card) {
                                    $now = Carbon::now();
                                    $expDate = Carbon::create('20'.$card->exp_year, $card->exp_month);
                                    if ($expDate->greaterThanOrEqualTo($now)) {
                                        $params = [
                                            'description' => 'Aguagente Período: '.$periodo,
                                            'amount'      => $amount,
                                            'currency'    => 'MXN',
                                            'card'        => $card->id,
                                            'details'     => [
                                                'name'  => $client->name,
                                                'phone' => $client->phone,
                                                'email' => $client->email,
                                                'line_items' => [
                                                    [
                                                        'name'        => 'Aguagente Período: '.$periodo,
                                                        'description' => 'Aguagente Período: '.$periodo,
                                                        'unit_price'  => $client->monthly_fee,
                                                        'quantity'    => 1
                                                    ]
                                                ],
                                                "shipment" => [
                                                    "carrier" => "aguagente",
                                                    "service" => "next_day",
                                                    "price"   => 0,
                                                    "address" => [
                                                        "street1" => $client->address,
                                                        "street2" => $client->address,
                                                        "street3" => $client->address,
                                                        "city"    => $client->county,
                                                        "state"   => $client->state,
                                                        "zip"     => $client->postal_code,
                                                        "country" => "Mexico"
                                                    ]
                                                ]
                                            ]
                                        ];
                                        try {
                                            $order     = Conekta_Charge::create($params);
                                            $charged[] = true;
                                            $this->info('Cliente: ('.$client->id_clients.') '.$client->name.' Aguagente Período: '.$periodo);
                                            break;
                                        } catch(Conekta_ProcessingError $e){
                                            $charged[] = false;
                                            $this->error('Cliente: ('.$client->id_clients.') '.$client->name.' Aguagente Período: '.$periodo.' | Tries: ('.$client->pay_tries.') - DIFF:('.$dateDiffPaytries[1].')');
                                        } catch(\Exception $e){
                                            $charged[] = false;
                                            $this->error('Cliente: ('.$client->id_clients.') '.$client->name.' Error: '.$e->getMessage());
                                            $this->notifyAdmin();
                                        }
                                    } else {
                                        $charged[] = false;
                                        $msg = 'Cliente: ('.$client->id_clients.') '.$client->name.' | Error: Tarjeta Expirada (**** **** **** '.$card->last4.'). Total de tarjetas registradas: '.count($cards);
                                        $this->error($msg);
                                        $this->notifyAdmin($msg);
                                    }
                                }
                                
                                $charged = collect($charged);
                                if (!$charged->contains(true)) {
                                    switch ($client->pay_tries) { 
                                        case 4:
                                            $debt                = $this->createDebt($client);
                                            $newNextPayday       = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));

											$client->subscription_status = 'past_due';
											$client->next_payday         = $newNextPayday;
											$client->debt                += $debt;
											$client->pay_tries           = 0;
                                            $this->notifyClientErrorCharges($client, $periodo);
                                            break;
                                        
                                        default:
                                            $client->pay_tries++;
                                            // $this->notifyClientErrorCharges($client);
                                            break;
                                    }
                                    
                                    $client->save();

                                } else {
                                    $newNextPayday       = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));
                            
                                    $client->subscription_status = 'active';
                                    $client->next_payday         = $newNextPayday;
                                    $client->pay_tries           = 0;
                                    $client->save();
                                    $this->notifyClientSuccessCharges($client, $periodo);
                                }
                                

                            } catch(\Conekta\ResourceNotFoundError $e) {
                                echo $e->getMessage();
                            }
                        }
                    
                    //si sus clientes están al dia y tiene más de 6, solo cambia su siguiente día de pago.
                    } else {
                        $newNextPayday               = date("Y-m-d", strtotime("+1 month", strtotime($client->next_payday)));
                        $client->subscription_status = 'active';
                        $client->next_payday         = $newNextPayday;
                        $client->save();
                    }
                }
            }
        }
    }

    /**
     * generateDateDiff 
     * Genera un array para la diferencia entre fechas, en cantidad de días.
     * 
     * @param date $next_payday Fecha del siguiente pago
     * @return array            Array de la diferencia en días.
     */
    private function generateDateDiff($next_payday)
    {
        $diffNowVsLastCharge = date_diff(date_create(), date_create($next_payday));

        if (!$diffNowVsLastCharge) {
        	 $arrayDiff  = [0,0];
        } else {
        	$r = $diffNowVsLastCharge->format('%R');
        	$a = $diffNowVsLastCharge->format('%a'); 

        	$arrayDiff  = [
	            $r,
	            $a
	        ];
        }

        return $arrayDiff;
    }
}
