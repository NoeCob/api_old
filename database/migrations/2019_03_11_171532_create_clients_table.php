<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('clients')) {
            return;
        }

        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id_clients');
            $table->string('name', 50)->nullable(false);
            $table->string('rfc', 13)->default(NULL);
            $table->text('address')->nullable(false);
            $table->string('outdoor_number', 15)->default(NULL);
            $table->string('inside_number', 15)->default(NULL);
            $table->string('phone', 20)->nullable(false);
            $table->string('email', 100)->nullable(false);
            $table->text('between_streets');
            $table->string('colony', 150)->nullable(false);
            $table->string('postal_code', 10)->nullable(false);
            $table->string('state', 150)->nullable(false);
            $table->string('county', 50)->default(NULL);
            $table->enum('level', ['VIAJERO', 'MARINERO', 'CABO', 'CAPITAN', 'ALMIRANTE'])->nullable(false);
            $table->string('device_id', 100)->default(NULL);
            $table->string('conekta_token', 100)->default(NULL);
            $table->integer('referred_by')->default(NULL);
            $table->enum('status', ['invalid','rejected', 'accepted', 'canceled', 'standby'])
                ->nullable(false)
                ->default('standby');
            $table->date('collected_at')->default(NULL);
            $table->unsignedInteger('id_users')->nullable(false);
            $table->unsignedInteger('id_groups')->nullable(false);
            $table->decimal('installation_fee', 10, 2)->nullable(false);
            $table->decimal('monthly_fee', 10, 2)->nullable(false);
            $table->decimal('deposit', 10, 2)->nullable(false);
            $table->boolean('social_responsability')->default(NULL);
            $table->string('signed_in', 50)->nullable(false);
            $table->integer('signed_in_id')->default(NULL);
            $table->integer('coupon')->default(NULL);
            $table->string('serial_number', 128)->default(NULL);
            $table->boolean('require_invoice')->default(false);
            $table->integer('monthly_installments')->nullable(false)->default(1);
            $table->decimal('charge_fee', 10, 2)->nullable(false)->default(0.00);
            $table->decimal('subscription_fee', 10, 2)->nullable(false)->default(0.00);
            $table->decimal('debt', 10, 2)->nullable(false)->default(0.00)->unsigned();
            $table->enum('subscription_status', ['in_trial','active','paused','canceled','past_due'])
                ->default(NULL);
            $table->dateTime('subscription_status_date')->default(NULL);
            $table->integer('pay_day')->default(NULL);
            $table->integer('pay_tries')->nullable(false)->default(0);
            $table->date('next_payday')->default(NULL);
            $table->text('extra_data');
            $table->unsignedSmallInteger('id_leadsources')->default(NULL);
            $table->string('super_agent', 20)->default(NULL);
            $table->longText('invoice_data');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
