<?php
namespace App\Traits;

use App\Debt;
use App\Contract;
use App\Client;
use App\MySQLClientRepository;
use App\LevelService;
use Carbon\Carbon;
use Request;
use Conekta;
use Conekta_Charge;
use Conekta_Customer;
use Conekta_Plan;
use Conekta_ProcessingError;
use Conekta_ParameterValidationError;

trait DebtTrait
{
    /**
     * createDebt
     * Crea una deuda a un cliente, se usa cuando no se logra realizar un cargo despues del 5to intento.
     * 
     * @param Client $client
     * @return float            Monto total de la deuda generada.
     */
    public function createDebt(Client $client)
    {
        $accumulated = Debt::where('id_clients', '=', $client->id_clients)
                            ->whereNull('id_charges')
                            ->sum('amount');

        $accumulated += $client->monthly_fee;
        $amount = $client->monthly_fee;

        if ($client->social_responsability == 1) {
            $sr = ($amount / 1.16) * .007;
            $amount += $sr;
        }

        $debt = new Debt();
        $debt->id_clients         = $client->id_clients;
        $debt->amount             = $amount;
        $debt->collection_fees    = 10000;
        $debt->moratory_fees      = (integer) $accumulated * .04;
        $debt->next_try_to_charge = date('Y-m-d', strtotime('+6 days'));
        $debt->save();

        $totalDebt = $debt->amount + $debt->collection_fees + $debt->moratory_fees;
        return $totalDebt;
    }

    /**
     * chargeDebt
     * Se realiza el intento de cargo de la deuda.
     * 
     * @param int $id ID de la deuda
     * @return int    ID de error.
     */
    public function chargeDebt($id)
    {
        try {

            if($debt = Debt::find($id)) {
    
                if($debt->id_charges) {
    
                    return '403';
    
                }
    
                $client = Client::find($debt->id_clients);
    
                Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                Conekta::setLocale('es');
    
                $customer = Conekta_Customer::find($client->conekta_token);
    
                $cards = json_decode($customer->cards->__toJSON());
    
                foreach($cards as $i => $card)
                    if($card->id == $customer->default_card_id) {
                        unset($cards[$i]);
                        array_unshift($cards, $card);
                        break;
                    }
    
                foreach($cards as $card) {
                    $now = Carbon::now();
                    $expDate = Carbon::create('20'.$card->exp_year, $card->exp_month);
                    if ($expDate->greaterThanOrEqualTo($now)) {
                        try {
                            $latePaymentTime = strtotime($debt->next_try_to_charge);
                            $latePaymentdate = date('Y-m', $latePaymentTime);
                            $latePaymentName = 'Aguagente Período: '.$latePaymentdate;

                            $chargeName = 'Pago tardío';
                            $total      = $debt->amount;
                            $isDebt     = true;
                            $lineItems  = [
                                [
                                    'name'        => $latePaymentName,
                                    'description' => $latePaymentName,
                                    'unit_price'  => $debt->amount,
                                    'quantity'    => 1,
                                    'type'        => 'digital'
                                ]
                            ];

                            if ($debt->collection_fees != '0.00') {
                                $total += $debt->collection_fees;
                                $lineItems[] = [
                                    'name'        => 'Gastos de cobranza',
                                    'description' => 'Gastos de cobranza',
                                    'unit_price'  => $debt->collection_fees,
                                    'quantity'    => 1,
                                    'type'        => 'digital'
                                ];
                            }

                            if ($debt->moratory_fees != '0.00') {
                                $total += $debt->moratory_fees;
                                $lineItems[] = [
                                    'name'        => 'Interés moratorio',
                                    'description' => 'Interés moratorio',
                                    'unit_price'  => $debt->moratory_fees,
                                    'quantity'    => 1,
                                    'type'        => 'digital'
                                ];
                            }
                            
                            if ($debt->collection_fees == '0.00' && $debt->moratory_fees == '0.00') {
                                $timestamp  = strtotime($debt->created_at);
                                $date       = date('Y-m', $timestamp);
                                $chargeName = 'Aguagente Período: '.$date;
                                $isDebt     = false;
                            }
                            
                            $chargeInfo = [
                                'description' => $chargeName,
                                'amount'      => $total,
                                'currency'    => 'MXN',
                                'card'        => $card->id,
                                'details'     => array(
                                    'name'       => $client->name,
                                    'phone'      => $client->phone,
                                    'email'      => $client->email,
                                    'line_items' => $lineItems,
                                    'shipment'   => array(
                                        'carrier' => 'aguagente',
                                        'service' => 'next_day',
                                        'price'   => 0,
                                        'address' => array(
                                            'street1' => $client->address,
                                            'street2' => $client->address,
                                            'street3' => $client->address,
                                            'city'    => $client->county,
                                            'state'   => $client->state,
                                            'zip'     => $client->postal_code,
                                            'country' => 'Mexico'
                                        )
                                    )
                                )
                            ];

                            try {
                                $charge = Conekta_Charge::create($chargeInfo);
                                
                                $debt->id_charges = $charge->id;
                                $debt->save();
                                
                                if ($isDebt) {
                                    $client->debt -= $charge->amount;
                                    $client->save();
                                }
            
                                $levelService = new LevelService(new MySQLClientRepository());
                                $levelService->setLevels($client);
            
                                return '200';
                            } catch(Conekta_ProcessingError $e){
                                return '402';
                            }                        
        
                        } catch(\Exception $e) {
        
                            return '402';
        
                        }
                    } else {
                        return '402';
                    }
                }
                
                return '402';
    
            } else {
                return '404';
            }
    
        } catch(\Exception $e) {
            return '500';
        }
    }
}