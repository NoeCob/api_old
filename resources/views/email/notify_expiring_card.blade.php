
@extends('layouts.email')

@section('name')
{{ $nombre }}
@endsection

@section('content')
Tu  tarjeta está próxima a expirar.  Por favor entra a nuestra App y actualiza los datos de pago para que no pierdas tu servicio y no vayas a tener cargos adicionales por pagos tardíos.
<br/>
Si no tienes la App, bajala de:
<br/>
AppStore: https://itunes.apple.com/mx/app/aguagente/id1089456395?l=en&mt=8
<br/>
GooglePlay: https://play.google.com/store/apps/details?id=com.Arizaga.AguagenteClient&hl=es
<br/>
Abre la App. Una vez dentro de la App primero busca el icono de tres rayitas horizontales blancas en la parte superior izquierda y dale click.
<br/>
Selecciona abrir sesión y has tu login con:
[Usuario] y contraseña.
<br/>
Si no recuerdas tu contraseña, enviar un correo a contacto@aguagente.com y te enviaremos una provisional que podrás cambiar en la App una vez que inicies sesión.
<br/>
Dale click a ‘Iniciar sesión’.
<br/>
Regresa al icono de las 3 rayitas blancas horizontales en la parte superior izquierda. Dale click y selecciona ‘Metodos de pago’.
<br/>
Selecciona ‘Agregar tarjeta’ y dale click.
<br/>
Captura los datos de tu tarjeta como solicitados y dale ‘Guardar’. Asegúrate de marcar la nueva tarjeta como principal si tienes otra tarjeta registrada. También puedes borrar la(s) otra(s).
<br/>
Cerrar sesión.
<br/>
Enviar un correo a <a href="mailto:contacto@aguagente.com">contacto@aguagente.com</a>  avisando que los datos de tarjeta se han puesto al dia.  
<br/>
Gracias y disfruta de Aguagente!
@endsection
