<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Charge;
use App\Client;
use App\Refund;
use App\Debt;
use App\Http\Controllers\Main;
use App\Traits\ChargeTrait;
use DB;
use Request;
use Validator;
use Mail;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Error;
use DateTime;
use DateInterval;
use DateTimeZone;

class ChargesController extends Main {

    use ChargeTrait;

    /**
     * index
     * Devuelve todos los cargos realizados a los clientes.
     *  Hay 2 tipos de informaciÃ³n devuelta:
     *   1) CONEKTA
     *   2) Base de datos:
     *      Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *  
     * @Conekta
     * @Conekta_Charge
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Charge
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $params = Request::query();

            if(isset($params['conekta'])) {
                unset($params['conekta']);
                if(isset($params['id_clients'])) {
                    $client = Client::find($params['id_clients']);
                    $params['customer_id'] = $client->conekta_token;
                    unset($params['id_clients']);
                }
                Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                Conekta::setLocale('es');

                $charges = json_decode(Conekta_Charge::where($params)->__toJSON());
                return Main::response(true, 'OK', $charges, 200);
                //return response(Conekta_Charge::where($params)->__toJSON(), 200)->header('Content-Type', 'application/json');
            }

            $charges = Charge::query();

            if(!isset($params['resolved']))
                $params['resolved'] = 'true';

            foreach($params as $name => $value) {

                 switch($name) {

                    case 'resolved': break;
                    case 'facturable':

                        $charges = $charges
                            ->whereNull('id_invoices')
                            ->whereRaw(
                                "YEAR(paid_at) =  YEAR(CURDATE() - INTERVAL 1 MONTH) AND
                                MONTH(paid_at) = MONTH(CURDATE() - INTERVAL 1 MONTH)"
                            );

                    break;

                    default:

                        $charges = $charges->where($name, $value);

                    break;

                }

            }

            $charges = $charges->orderBy('created_at', 'desc')->get();

            if($params['resolved'] == 'true') {

                foreach($charges as &$charge) {

                    $charge = $this->resolveRelations($charge);

                }

            }

            return Main::response(true, 'OK', $charges, 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    
    /**
     * createInvoice
     * Crea el array de datos para generar un INVOICE
     * 
     * @param  string      $type                  tipo de invoice (ingreso|egreso)
     * @param  Client|null $client                Objeto Eloquent del Cliente
     * @param  string      $description           DescripciÃ³n del invoice
     * @param  float       $amount                Monto de la transacciÃ³n
     * @param  float       $socialResponsability  Monto a donar.
     * @param  date        $date                  Fecha a realizar el pago.
     * @param  int         $ids                   ID del cargo.
     * @return aray                               
     */
    private function createInvoice($type, Client $client = null, $description, $amount, $socialResponsability, $date, $ids)
    {

        /*$invoice['client'] = $client ?
        [
			'address' => $client->address,
			'city'    => $client->county,
			'colony'  => $client->colony,
			'state'   => $client->state,
			'cp'      => $client->postal_code,
			'email'   => $client->email,
			'name'    => $client->name,
			'rfc'     => $client->rfc
        ] : [
			'address' => 'NA',
			'city'    => 'NA',
			'colony'  => 'NA',
			'state'   => 'NA',
			'cp'      => 'NA',
			'email'   => 'it@aguagente.com',
			'name'    => 'Aguagente',
			'rfc'     => 'XAXX010101000'
        ];

        $invoice['items'][] = [
			'description' => $description . " ($date)",
			'quantity'    => 1,
			'unit_price'  => round(round($amount / 1.16, 0), 2),
			'iva'         => round(($amount - round($amount / 1.16, 0)), 2)
        ];*/

        $clientData = [
			'address' => 'NA',
			'city'    => 'NA',
			'colony'  => 'NA',
			'state'   => 'NA',
			'cp'      => 'NA',
			'email'   => 'it@aguagente.com',
			'name'    => 'Aguagente',
			'rfc'     => 'XAXX010101000'
        ];

        if ($client) {
            $clientData = [
                'address' => $client->address,
                'city'    => $client->county,
                'colony'  => $client->colony,
                'state'   => $client->state,
                'cp'      => $client->postal_code,
                'email'   => 'facturacion_api@aguagente.com', // $client->email.',servicio@aguagente.com',
                'name'    => $client->name,
                'rfc'     => $client->rfc
            ];

            if (!is_null($client->invoice_data)) {
                $clientData = [
                    'address' => $client->invoice_data->address,
                    'city'    => $client->invoice_data->county,
                    'colony'  => $client->invoice_data->colony,
                    'state'   => $client->invoice_data->state,
                    'cp'      => $client->invoice_data->postal_code,
                    'email'   => 'facturacion_api@aguagente.com', // $client->invoice_data->email.',servicio@aguagente.com',
                    'name'    => $client->invoice_data->name,
                    'rfc'     => $client->invoice_data->rfc
                ];
            }
        }

        $invoice['client'] = $clientData;
        $invoice['items']  = [];

        if($socialResponsability != 0) {
            $amountSinIva    = $amount / 1.16;
            $sr              = $amountSinIva * .007;
            $srIva           = $sr * .16;

            $neto            = $amount + $sr;
            $realAmount      = $neto / 1.16;
            $realAmountSinSR = $realAmount - $sr;
            $realAmountIva   = $realAmountSinSR * .16;
            
             $invoice['items'][] = [
                'description' => $description . " ($date) con Resp. Social",
                'quantity'    => 1,
                'unit_price'  => round($realAmountSinSR + $sr, 2),
                'iva'         => round($realAmountIva + $srIva, 2),
            ];

            /*
            Working with SR display
            $invoice['items'][] = [
                'description' => $description . " ($date)",
                'quantity'    => 1,
                'unit_price'  => round($realAmountSinSR, 2),
                'iva'         => round($realAmountIva, 2),
            ];

            $invoice['items'][] = [
                'description' => 'Responsabilidad social',
                'quantity'    => 1,
                'unit_price'  => round($socialResponsability, 2),
                'iva'         => round($socialResponsability - ($socialResponsability / 1.16), 2)//0.00
            ];

            OLD
            $invoice['items'][] = [
				'description' => 'Responsabilidad social',
				'quantity'    => 1,
				'unit_price'  => round($sr - $srIva,2),
				'iva'         => round($srIva,2)
            ];
            */

        } else {
            $invoice['items'][] = [
                'description' => $description . " ($date)",
                'quantity'    => 1,
                'unit_price'  => round($amount / 1.16, 2),
                'iva'         => round($amount - ($amount / 1.16), 2)
            ];
        }

        $invoice['tipo']     = $type;
        $invoice['subtotal'] = array_reduce($invoice['items'], function($amount, $item) { return $amount += $item['unit_price']; }, 0);
        $invoice['iva']      = array_reduce($invoice['items'], function($amount, $item) { return $amount += $item['iva']; }, 0);
        $invoice['total']    = $invoice['subtotal'] + $invoice['iva'];
        $invoice['fecha']    = $date;
        $invoice['ids']      = $ids;
        return $invoice;

    }

    /**
     * extractSocialResponsability
     * Genera el monto real de la donaciÃ³n. No se genera en los depositos o cargos adicionales.
     *
     * @App\Debt
     * 
     * @param  Charge $charge Objeto Eloquent del Cargo
     * @param  Client $client Objeto Eloquent del Cliente
     * @return float
     */
    private function extractSocialResponsability($charge, $client) {

        $socialResponsability = 0;
        if($client->social_responsability) {
            switch($charge->description) {
                case 'Depósito':                     $amountWithSocial = 0; break;
                case 'Cargos adicionales Aguagente': $amountWithSocial = 0; break;
                case 'Pago tardí­o':                  $amountWithSocial = Debt::where('id_charges', '=', $charge->id_charges)->sum('amount'); break;
                case 'Extras':                       $amountWithSocial = $charge->amount - $client->deposit; break;
                default:                             $amountWithSocial = $charge->amount; break;
            }
            $socialResponsability = $amountWithSocial - round($amountWithSocial / (1 + (1 / 1.16 * .007)), 0);
        }
        return $socialResponsability;

    }

    /**
     * generateInvoices
     * Genera el array de invoices por cliente, por fecha.
     *
     * @App\Client
     * 
     * @param  Charge $charges Objeto Eloquent de Cargos
     * @param  date   $date    Fecha
     * @return array
     */
    private function generateInvoices($charges, $date) {

        $invoices = [];

        foreach($charges as $charge) {

            $client = Client::find($charge->id_clients);

            $socialResponsability = $this->extractSocialResponsability($charge, $client);

            $invoices[] = $this->createInvoice(
                'ingreso',
                $client,
                $charge->description,
                $charge->amount - $socialResponsability,
                $socialResponsability,
                $date,
                [$charge->id_charges]
            );

        }

        return $invoices;

    }

    /**
     * getChargesFromConekta
     * Obtiene todos los cargos de CONEKTA en el rango de 1 mes.
     *
     * @param Carbon $today Objeto CARBON con la fecha
     * @return array        Array de cargos
     */
    private function getChargesFromConekta($today)
    {
        Conekta::setApiKey(env('CONEKTA_API_KEY'));
        Conekta::setLocale('es');

        $init     = $today->startOfMonth()->startOfDay()->timestamp;
        $end      = $today->lastOfMonth()->endOfDay()->timestamp;
        
        $conektaCharges = [];
        $iteraciones    = 20;
        $limit          = 50;

        try {
            for ($i=0; $i < $iteraciones; $i++) {
                
                $offset = $limit * $i;

                $conektaCharges[] = json_decode(
                    Conekta_Charge::where([
                        'created_at.gte' => $init,
                        'created_at.lte' => $end,
                        'limit'          => $limit,
                        'offset'         => $offset
                    ])->__toJSON()
                );
            }
        } catch(Conekta_Error $e) {
            //dd($e, $conektaCharges);
        }
        
        $charges = [];
        foreach ($conektaCharges as $chargesArray) {
            foreach($chargesArray as $charge) {
                if (!is_null($charge->paid_at)) {
                    $charges[$charge->id] = $charge;
                    $subtotal += $charge->amount;
                    $refunded += $charge->amount_refunded;
                }
            }
        }

        //dd($charges);
        return $charges;
    }

    /**
     * invoice
     * Genera las facturas para una fecha dada.
     *  - Factura individual para clientes con RFC
     *  - Factura global para clientes sin RFC
     * DescripciÃ³n del proceso:
     *  1) Se obtienen los clientes CON y SIN RFC.
     *  2) Obtiene los cargos PAGADOS y no FACTURADOS de los clientes CON y SIN RFC.
     *  3) Obtiene las devoluciones generadas en el rango de fechas y que NO ESTÃ‰N FACTURADAS.
     *  4) Valida que los cargos realmente existan en CONEKTA
     *  5) Genera el array de invoices para Clientes con RFC y el Global
     *  6) POR CADA INVOICE:
     *     - FORMATEA EL PRECIO PARA QUITARLE EL FORMATO DE CONEKTA
     *     - FORMATEA LA FECHA, CON LA ZONA HORARIA DE MEXICO
     *     - ENVIA A WEBSERVICE PARA FACTURAR EL INVOICE
     *     - ACTUALIZA LOS CARGOS, Y LES ASIGNA EL ID DE FACTURA GENERADO.
     * 
     * @return array Array de invoices.
     */
    public function invoice(Request $request)
    {
        //dd($request::input('month'));
        $today = Carbon::now();
        $y     = $request::input('y');
        $m     = $request::input('m');
        if ( $m != '') {
            $today = Carbon::createFromDate(($y != '' ? $y:date('Y')), str_pad($m, 2, '0', STR_PAD_LEFT), '01');
        }
        $year  = $today->year;
        $month = $today->month;
        $date  = $today->lastOfMonth()->endOfDay()->toDateString();

        
        $clientsWithRFC = array_map(
            function($element) { return $element['id_clients']; },
            Client::whereNotNull('rfc')
                  ->where('rfc', '!=', 'null')
                  ->where('rfc', '!=', '')
                  ->select('id_clients')
                  ->get()
                  ->toArray()
        );

        $clientsWithoutRFC = array_map(
            function($element) { return $element['id_clients']; },
            Client::whereNull('rfc')
                  ->orWhere('rfc', '=', 'null')
                  ->orWhere('rfc', '=', '')
                  ->select('id_clients')
                  ->get()
                  ->toArray()
        );

        $condition    =  "     paid_at IS NOT NULL
                            && invoice_status != 'invoiced'
                            && YEAR(paid_at) = ?
                            && MONTH(paid_at) = ?";

        $chargesRfc   = Charge::whereIn('id_clients', $clientsWithRFC)
                                ->whereRaw($condition, [$year, $month])
                                ->get();

        $chargesNoRfc = Charge::whereIn('id_clients', $clientsWithoutRFC)
                                ->whereRaw($condition, [$year, $month])
                                ->get();
        
        $refunds      = Refund::whereRaw('id_credit_notes IS NULL && YEAR(created_at) = ? && MONTH(created_at) BETWEEN ? AND 12', [$year, $month])->get();
        
        $charges              = array_merge($chargesRfc->toArray(), $chargesNoRfc->toArray());
        $conektaCharges       = [];
        //$conektaCharges       = $this->getChargesFromConekta($today);
        $invoices             = [];
        $total                = 0;
        $totalConekta         = 0;
        $totalRefunded        = 0;
        $totalRefundedConekta = 0;
        $totalInvaildRefunds  = 0;
        $nonExistent          = [];
        $invalid              = [];
        $invalidRefunds       = [];

        foreach ($charges as $charge) {
            if (!array_key_exists($charge['id_charges'], $conektaCharges)) {
                $nonExistent[] = $charge;
            } else {
                $thisConektaCharge = $conektaCharges[$charge['id_charges']];
                $conektaAmount     = number_format($thisConektaCharge->amount, 2, '.', '');
                $conektaRefund     = number_format($thisConektaCharge->amount_refunded, 2, '.', '');
                
                if ($charge['amount'] != $conektaAmount) {
                    $invalid[] = $charge;
                } else {
                    $total                += $charge['amount'];
                    $totalConekta         += $conektaAmount;
                    $totalRefundedConekta += $conektaRefund;
                }
            }
        }

        /*
        foreach ($refunds as $refund) {
            $totalRefunded += $refund->amount;
            if (!array_key_exists($refund->id_charges, $conektaCharges)) {
                $invalidRefunds[] = $refund;
            }
        }

        if (count($invalidRefunds) > 0) {
            foreach ($invalidRefunds as $invalidRefund) {
                $charge                    = Charge::where('id_charges', '=', $invalidRefund->id_charges)->first();
                $totalInvaildRefunds      += $invalidRefund->amount;
                $invalidRefund->created_at = $charge->created_at;
                //$invalidRefund->save();
            }
            //Re-run the get refunds;
            //$refunds = Refund::whereRaw('id_credit_notes IS NULL && YEAR(created_at) = ? && MONTH(created_at) = ?', [$year, $month])->get();
        }
        */

        //Genera los invoices para los clientes que requieran factura
        $invoices = $this->generateInvoices($chargesRfc, $date);

        //Genera el invoice general
        if (!empty($charges)) {

            $totals = array_reduce(
                $this->generateInvoices($chargesNoRfc, $date),
                function($totals, $invoice) {
                    $totals['amount'] += $invoice['items'][0]['unit_price'];
                    $totals['iva']    += $invoice['items'][0]['iva'];
                    $totals['social'] += isset($invoice['items'][1]) ? $invoice['items'][1]['unit_price'] : 0.00;
                    //$totals['social'] += isset($invoice['items'][1]) ? $invoice['items'][1]['unit_price'] : 1;
                    $totals['ids']     = array_merge($totals['ids'], $invoice['ids']);
                    return $totals;
                },
                [
                    'amount' => 0.00,
                    'iva'    => 0.00,
                    'social' => 0.00,
                    'ids'    => []
                ]
            );

            $invoices[] = $this->createInvoice(
                'ingreso',
                null,
                'Servicios de agua purificada',
                $totals['amount'] + $totals['iva'],
                $totals['social'],
                $date,
                $totals['ids']
            );

        }

        $credit_notes = [];
        foreach ($refunds as $refund) {
            $charge = Charge::find($refund->id_charges);
            $client = Client::find($charge->id_clients);
            $social = $this->extractSocialResponsability($charge, $client) * round($refund->amount / $charge->amount, 2);
            if($client->rfc) {
                $invoices[] = $this->createInvoice(
                    'egreso',
                    $client,
                    $charge->description,
                    $refund->amount - $social,
                    $social,
                    $date,
                    [$refund->id_refunds]
                );
            } else {
                $credit_notes[] = $this->createInvoice(
                    'egreso',
                    $client,
                    $charge->description,
                    $refund->amount - $social,
                    $social,
                    $date,
                    [$refund->id_refunds]
                );
            }
        }

        if (!empty($credit_notes)) {
        	$totals = array_reduce(
                $credit_notes,
                function($totals, $invoice) {
                    $totals['amount'] += $invoice['items'][0]['unit_price'];
                    $totals['iva']    += $invoice['items'][0]['iva'];
                    $totals['social'] += isset($invoice['items'][1]) ? $invoice['items'][1]['unit_price'] : 0.00;
                    $totals['ids']     = array_merge($totals['ids'], $invoice['ids']);
                    return $totals;
                },
                [
                    'amount' => 0.00,
                    'iva' => 0.00,
                    'social' => 0.00,
                    'ids' => []
                ]
            );

            $invoices[] = $this->createInvoice(
                'egreso',
                null,
                'Servicios de agua purificada',
                $totals['amount'] + $totals['iva'],
                $totals['social'],
                $date,
                $totals['ids']
            );
        }

        $finalT = 0;
        $ivaT   = 0;
        foreach ($invoices as &$invoice) {
            $sumaUnit  = 0;
            $sumaIva   = 0;
            $sumaTotal = 0;
        	foreach ($invoice['items'] as &$item) {
                $item['unit_price'] = round($item['unit_price'] / 100, 2, PHP_ROUND_HALF_UP);
                $item['iva']        = round($item['iva'] / 100, 2, PHP_ROUND_HALF_UP);

                $sumaUnit += $item['unit_price'];
                $sumaIva  += $item['iva'];
            }
            $sumaTotal = $sumaUnit+$sumaIva;

            //DEBUG
            if ($invoice['tipo'] == 'ingreso') {
                $finalT += $sumaUnit;
                $ivaT   += $sumaIva;
            }
            

            /*
            $invoice['subtotal'] = round($invoice['subtotal'] / 100, 1);
            $invoice['iva']      = round($invoice['iva'] / 100, 1);
            $invoice['total']    = round($invoice['total'] / 100, 1);
            */
            $invoice['subtotal'] = $sumaUnit;
            $invoice['iva']      = $sumaIva;
            $invoice['total']    = $sumaTotal;

            if ((new DateTime())->diff(new DateTime($invoice['fecha']))->format('%a') >= 3) {
                $invoice['fecha'] = (new DateTime('now', new DateTimeZone('America/Mexico_City')))->format('Y-m-d');
            }

            
            try {
                $toSend = $invoice;
                unset($toSend[ids]);
                
                $response = (new \GuzzleHttp\Client())
                    ->request(
                        'POST',
                        'http://cfdipro.info/api/webservice/v3/aguagente_cfdi',
                        [
                            'json' => $toSend,
                            'headers' => [
                                'Authorization' => 'Basic:QUdVMTUwMjEzREI2OkFHVTE1MDIxM0RCNg=='
                            ]
                        ]
                    );

                $body = json_decode($response->getBody()->__toString());
                
                $ids = implode(',', array_fill(0, count($invoice['ids']), '?'));

                
                if($invoice['tipo'] == 'ingreso') {
                    DB::update("UPDATE charges SET invoice_status = 'invoiced', id_invoices = ? WHERE id_charges IN($ids)", array_merge([$body->id], $invoice['ids']));
                } else {
                    DB::update("UPDATE refunds SET id_credit_notes = ? WHERE id_refunds IN($ids)", array_merge([$body->id], $invoice['ids']));
                }

            } catch(\Exception $e) {
                echo 'Client: '.$invoice[client][name].' | Data: '.json_encode($invoice).' | Error: '.$e->getMessage().'<br>';
                //file_put_contents('invoice', date('Y-m-d H:i:s') . " " . $e->getMessage() . "\n", FILE_APPEND);
            }
            
        }

        //dd($invoices, $finalT, $ivaT, $finalT+$ivaT);
        return $invoices;
    }

    /**
     * invoiceOld
     * @deprecated version 2
     * Genera el Invoice o cargo para una fecha dada a los clientes.
     *  - Invoice para clientes con RFC
     *  - Charge para clientes sin RFC
     * DescripciÃ³n del proceso:
     *  1) Se obtienen los clientes CON y SIN RFC
     *  2) OBTIENE TODOS LOS CARGOS REALIZADOS Y PAGADOS A LOS CLIENTES SIN RFC
     *  3) SI HAY CARGOS, OBTIENE LOS TOTALES DE LO YA COBRADO Y PAGADO, Y GENERA LOS INVOICES PARA ESOS CARGOS.
     *  4) REVISA SI HAY DEVOLUCIONES PARA ESE MES/AÃ‘O
     *      SI EL CLIENTE TIENE RFC, GENERA EL INVOICE, SINO, LO MANDA A NOTA DE CREDITO
     *  5) SI HAY NOTAS DE CREDITOS, OBTIENE EL TOTAL DE LAS NOTAS DE CREDITO Y GENERA UN UNICO INVOICE POR TODAS
     *     LAS NOTAS
     *  6) POR CADA INVOICE:
     *     - FORMATEA EL PRECIO PARA QUITARLE EL FORMATO DE CONEKTA
     *     - FORMATEA LA FECHA, CON LA ZONA HORARIA DE MEXICO
     *     - ENVIA A WEBSERVICE PARA FACTURAR EL INVOICE
     *     - ACTUALIZA LOS CARGOS, Y LES ASIGNA EL ID DE FACTURA GENERADO.
     * 
     * @return array Array de invoices.
     */
    public function invoiceOld() {

        $params = Request::query();
        $year  = isset($params['year'])  ? $params['year']  : date('Y');
        $month = isset($params['month']) ? $params['month'] : date('m');

        $date = new DateTime(sprintf('%02s-%02s-%02s', $year, $month, 1));
        $date->sub(new DateInterval('P1D'));
        $year  = $date->format('Y');
        $month = $date->format('m');
        $date  = $date->format('Y-m-d');

        $clientsWithRFC = array_map(
            function($element) { return $element['id_clients']; },
            Client::whereNotNull('rfc')
                  ->select('id_clients')
                  ->get()
                  ->toArray()
        );

        $clientsWithoutRFC = array_map(
            function($element) { return $element['id_clients']; },
            Client::whereNull('rfc')
                  ->select('id_clients')
                  ->get()
                  ->toArray()
        );

        $condition =  "paid_at IS NOT NULL &&
              invoice_status != 'invoiced' &&
                         YEAR(paid_at) = ? &&
                        MONTH(paid_at) = ?";

        //GENERA EL ARRAY DE INVOCIES PARA LOS CLIENTES CON RFC
        $invoices = $this->generateInvoices(
            Charge::whereIn('id_clients', $clientsWithRFC)
                  ->whereRaw($condition, [$year, $month])
                  ->get(),
            $date
        );

        //OBTIENE TODOS LOS CARGOS REALIZADOS Y PAGADOS A LOS CLIENTES SIN RFC
        $charges = Charge::whereIn('id_clients', $clientsWithoutRFC)
                         ->whereRaw($condition, [$year, $month])
                         ->get();

        //SI HAY CARGOS, OBTIENE LOS TOTALES DE LO YA COBRADO Y PAGADO, Y GENERA LOS INVOICES PARA ESOS CARGOS.
        if(!$charges->isEmpty()) {

            $totals = array_reduce(
                $this->generateInvoices($charges, $date),
                function($totals, $invoice) {
                    $totals['amount'] += $invoice['items'][0]['unit_price'];
                    $totals['iva']    += $invoice['items'][0]['iva'];
                    $totals['social'] += isset($invoice['items'][1]) ? $invoice['items'][1]['unit_price'] : 0.00;
                    $totals['ids']     = array_merge($totals['ids'], $invoice['ids']);
                    return $totals;
                },
                [
                    'amount' => 0.00,
                    'iva' => 0.00,
                    'social' => 0.00,
                    'ids' => []
                ]
            );

            $invoices[] = $this->createInvoice(
                'ingreso',
                null,
                'Servicios de agua purificada',
                $totals['amount'] + $totals['iva'],
                $totals['social'],
                $date,
                $totals['ids']
            );

        }

        $credit_notes = [];

        //REVISA SI HAY DEVOLUCIONES PARA ESE MES/AÃ‘O
        //SI EL CLIENTE TIENE RFC, GENERA EL INVOICE, SINO, LO MANDA A NOTA DE CREDITO
        $refunds = Refund::whereRaw('id_credit_notes IS NULL && YEAR(created_at) = ? && MONTH(created_at) = ?', [$year, $month])->get();

        foreach($refunds as $refund) {
            $charge = Charge::find($refund->id_charges);
            $client = Client::find($charge->id_clients);
            $social = $this->extractSocialResponsability($charge, $client) * round($refund->amount / $charge->amount, 2);
            if($client->rfc) {
                $invoices[] = $this->createInvoice(
                    'egreso',
                    $client,
                    $charge->description,
                    $refund->amount - $social,
                    $social,
                    $date,
                    [$refund->id_refunds]
                );
            } else {
                $credit_notes[] = $this->createInvoice(
                    'egreso',
                    $client,
                    $charge->description,
                    $refund->amount - $social,
                    $social,
                    $date,
                    [$refund->id_refunds]
                );
            }
        }

        //SI HAY NOTAS DE CREDITOS, OBTIENE EL TOTAL DE LAS NOTAS DE CREDITO Y GENERA UN UNICO INVOICE
        //POR TODAS LAS NOTAS
        if($credit_notes) {

            $totals = array_reduce(
                $credit_notes,
                function($totals, $invoice) {
                    $totals['amount'] += $invoice['items'][0]['unit_price'];
                    $totals['iva']    += $invoice['items'][0]['iva'];
                    $totals['social'] += isset($invoice['items'][1]) ? $invoice['items'][1]['unit_price'] : 0.00;
                    $totals['ids']     = array_merge($totals['ids'], $invoice['ids']);
                    return $totals;
                },
                [
                    'amount' => 0.00,
                    'iva' => 0.00,
                    'social' => 0.00,
                    'ids' => []
                ]
            );

            $invoices[] = $this->createInvoice(
                'egreso',
                null,
                'Servicios de agua purificada',
                $totals['amount'] + $totals['iva'],
                $totals['social'],
                $date,
                $totals['ids']
            );

        }

        //POR CADA INVOICE:
        // FORMATEA EL PRECIO PARA QUITARLE EL FORMATO DE CONEKTA
        // FORMATEA LA FECHA, CON LA ZONA HORARIA DE MEXICO
        // ENVIA A WEBSERVICE PARA FACTURAR EL INVOICE
        // ACTUALIZA LOS CARGOS, Y LES ASIGNA EL ID DE FACTURA GENERADO.
        foreach($invoices as $invoice) {

            $invoice['items'][0]['unit_price'] = $invoice['items'][0]['unit_price'] / 100;
            $invoice['items'][0]['iva'] = $invoice['items'][0]['iva'] / 100;
            if(isset($invoice['items'][1])) {
                $invoice['items'][1]['unit_price'] = $invoice['items'][1]['unit_price'] / 100;
                $invoice['items'][1]['iva'] = $invoice['items'][1]['iva'] / 100;
            }
            $invoice['subtotal'] = $invoice['subtotal'] / 100;
            $invoice['iva'] = $invoice['iva'] / 100;
            $invoice['total'] = $invoice['total'] / 100;

            if((new DateTime())->diff(new DateTime($invoice['fecha']))->format('%a') >= 3)
                $invoice['fecha'] = (new DateTime('now', new DateTimeZone('America/Mexico_City')))->format('Y-m-d');

            try {

                $response = (new \GuzzleHttp\Client())
                    ->request(
                        'POST',
                        'http://webcreation.com.mx/factura/ajax_garry.php',
                        [
                            'json' => $invoice,
                            'headers' => [
                                'Authorization' => 'Basic:QUdVMTUwMjEzREI2OkFHVTE1MDIxM0RCNg=='
                            ]
                        ]
                    );

                $body = json_decode($response->getBody()->__toString());

                $ids = implode(',', array_fill(0, count($invoice['ids']), '?'));

                if($invoice['tipo'] == 'ingreso') {
                    DB::update("UPDATE charges SET invoice_status = 'invoiced', id_invoices = ? WHERE id_charges IN($ids)", array_merge([$body->id], $invoice['ids']));
                } else {
                    DB::update("UPDATE refunds SET id_credit_notes = ? WHERE id_refunds IN($ids)", array_merge([$body->id], $invoice['ids']));
                }

            } catch(\Exception $e) {

                file_put_contents('invoice', date('Y-m-d H:i:s') . " " . $e->getMessage() . "\n", FILE_APPEND);

            }


        }

        return $invoices;

    }

    /**
     * show
     * Muestra una cargo (\App\Charge) por medio del ID
     *
     * @\App\Charge
     * 
     * @param  int      $id ID del cargo
     * @return response     OK|Not Found
     */
    public function show($id) {

        if($charge = Charge::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($charge), 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * refund
     * Genera las devoluciones en CONEKTA y las almacena en la base de datos.
     *
     * @Conekta
     * @App\Charge
     * @App\Refund
     * 
     * @param  int $id ID del cargo
     * @return reponse     OK|Bad Request|Forbidden
     */
    public function refund($id) {

        if ($charge = Charge::find($id)) {

            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');

            try {

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'amount'  => 'numeric'
                    ]
                );

                if ($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                $amount = isset($input['amount']) ? $input['amount'] : $charge->amount;

                $conektaCharge = Conekta_Charge::find($charge->id_charges);
                $conektaCharge->refund($amount);

                $refund = new Refund();
                $refund->id_charges = $charge->id_charges;
                $refund->amount = $amount;
                $refund->save();

                return Main::response(true, 'OK', $this->resolveRelations($charge), 200);

            } catch(\Exception $e) {

                return Main::response(false, 'Forbidden', ['errors'=>['refund'=>[$e->getMessage()]]], 403);

            }

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * create
     * Crea un cargo en CONEKTA de manera manual para pagos OFFLINE (OXXO/SPEI)
     * 
     * @return response     OK|ERROR
     */
    public function create() 
    {   
        Conekta::setApiKey(env('CONEKTA_API_KEY'));
        Conekta::setLocale('es');

        $inputs   = Request::all();
        if ($client = Client::find($inputs['id_clients'])) {
            
            $customer = Conekta_Customer::find($client->conekta_token);

            if (!isset($inputs['months_ahead'])) {
                $amount      = $client->debt;
                $description = 'Pago tardío';
                
            } else {
                $monthly_fee = $client->monthly_fee;
                $sr          = ($client->social_responsability == 1 ? ($client->monthly_fee/1.16)*.007: 0);
                $amount      = ($monthly_fee + $sr) * $inputs['months_ahead'];
                $description = 'Aguagente Período: Pago adelantado ( '.$inputs['months_ahead'].' )';

            }

            $lineItems = [
                [
                    'name'        => $description,
                    'description' => $description,
                    'unit_price'  => $amount,
                    'quantity'    => 1
                ]
            ];

            $params = [
                'client'      => $client,
                'description' => $description,
                'line_items'  => $lineItems,
                'type'        => $inputs['type']
            ];

            if (isset($inputs['months_ahead'])) {
                $params['months_ahead'] = $inputs['months_ahead'];
            }

            //dd($params);

            $order = $this->createCharge($params);

            switch($order['error']){
                case 0:
                    return Main::response(true, 'OK', $order, 200);
                    break;

                case 1:
                    return Main::response(true, 'ERROR', $order, 500);
                    break;
            }


            
        }
        
    }

    /**
     * resolveRelations
     * Devuelve todos el cargo con los clientes y devoluciones.
     *
     * @App\Charge
     * @App\Cliente
     * @App\Refund
     * 
     * @param  Charge $category Objeto Eloquent Charge
     * @return Charge           Objeto Eloquent Charge
     */
    private function resolveRelations(Charge $charge) {

        $charge->client = Client::where('conekta_token', '=', $charge->customer_id)->first();
        $charge->refunds = Refund::where('id_charges', '=', $charge->id_charges)->get();

        return $charge;

    }

}
